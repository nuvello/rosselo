import {ApplicationRef, enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';
import {bootloader, hmrModule} from '@angularclass/hmr';
import {enableDebugTools} from '@angular/platform-browser';
import {setAutoFreeze} from 'immer';

if (environment.production) {
  enableProdMode();
}

bootloader(() => {
  setAutoFreeze(!environment.production);

  const promise = platformBrowserDynamic().bootstrapModule(AppModule);
  if (!environment.production) {
    promise.then((module) => {
      enableDebugTools(module.injector.get(ApplicationRef).components[0]);
    });

    if (environment.hmr) {
      (<any>module).hot.accept();

      promise.then((mod) => {
        return hmrModule(mod, <any>module);
      });
    }
  }
});
