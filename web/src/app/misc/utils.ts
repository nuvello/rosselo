export function isTouchDevice() {
  const prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  const mq = function (query) {
    return window.matchMedia(query).matches;
  };

  if (('ontouchstart' in window) || window['DocumentTouch'] && document instanceof window['DocumentTouch']) {
    return true;
  }

  // include the 'heartz' as a way to have a non matching MQ to help terminate the join
  // https://git.io/vznFH
  return mq(['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join(''));
}


export function isInputFocused() {
  const el = document.activeElement;

  return el && (el.tagName.toLowerCase() === 'input' && (<HTMLInputElement>el).type === 'text' ||
    el.tagName.toLowerCase() === 'textarea');
}

export function isUrl(text: string): boolean {
  return /^(https?:\/\/(?:www\.|(?!www)))?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)$/.test(text);
}
