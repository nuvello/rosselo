import {isUrl} from './utils';
import {CreateSourceType} from '../dto/create-source-type';

export function parseCreateSourceTypeFromRawString(
  sourceInput: string,
  defaultChannel: 'url' | 'youtube' | 'twitter'
): {
  type: CreateSourceType,
  parsedValue: string
} {
  sourceInput = sourceInput.trim();

  if (sourceInput.indexOf('twitter.com') !== -1) {
    const match = /twitter\.com\/(\w+)/.exec(sourceInput);

    if (match.length > 1 && match[1].length > 0) {
      return {
        type: CreateSourceType.TWITTER_ACCOUNT_NAME,
        parsedValue: match[1].trim()
      };
    }
  } else if (sourceInput.indexOf('youtube.com') !== -1) {
    if (/channel\/([a-zA-Z0-9-_]+)/.test(sourceInput)) {
      return {
        type: CreateSourceType.YOUTUBE_CHANNEL_ID,
        parsedValue: /channel\/([a-zA-Z0-9-_]+)/.exec(sourceInput)[1]
      };
    } else if (/list=([a-zA-Z0-9-_]+)/.test(sourceInput)) {
      return {
        type: CreateSourceType.YOUTUBE_PLAYLIST_ID,
        parsedValue: /list=([a-zA-Z0-9-_]+)/.exec(sourceInput)[1]
      };
    } else if (/user\/([a-zA-Z0-9-_]+)/.test(sourceInput)) {
      return {
        type: CreateSourceType.YOUTUBE_CHANNEL_NAME,
        parsedValue: /user\/([a-zA-Z0-9-_]+)/.exec(sourceInput)[1]
      };
    }
  }

  if (isUrl(sourceInput)) {
    return {type: CreateSourceType.RSS_URL, parsedValue: sourceInput};
  } else if (defaultChannel === 'twitter' || sourceInput[0] === '@') {
    if (sourceInput[0] === '@') {
      sourceInput = sourceInput.substring(1);
    }

    return {
      type: CreateSourceType.TWITTER_ACCOUNT_NAME,
      parsedValue: sourceInput
    };
  } else if (defaultChannel === 'youtube') {
    return {
      type: CreateSourceType.YOUTUBE_CHANNEL_NAME,
      parsedValue: sourceInput
    };
  } else {
    // the default channel is URL
    throw new Error('Invalid URL');
  }
}
