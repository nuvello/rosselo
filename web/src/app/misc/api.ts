export const API_URI_PLACEHOLDER = 'ROSSELO-API';
export const LOCAL_STORAGE_AUTH_TOKEN_KEY = 'auth-token';
export const HIDE_TRIAL_NOTIFICATION_TOKEN_KEY = 'hide-trial-notification';

export function parseDateFromApi(date): Date | null {
  if (date === null) {
    return null;
  }

  return new Date(date + 'Z');
}

export function convertDateForApi(date): Date {
  if (date === null ) {
    return null;
  }

  return new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(),
    date.getUTCHours(), date.getUTCMinutes(), date.getUTCSeconds()));
}

export function parseJwtToken(token: string): {exp: number, sub: any} {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
}
