import {Router, RoutesRecognized} from '@angular/router';
import {Injectable} from '@angular/core';
import {filter, pairwise} from 'rxjs/operators';
import {isGaEnabled} from './ga';

@Injectable()
export class RouterExtService {

  private previousUrl: string = null;
  private currentUrl: string = null;

  constructor(private router: Router) {
    router.events.pipe(
      filter(e => e instanceof RoutesRecognized),
      pairwise()
    ).subscribe((event: any[]) => {
      this.previousUrl = event[0].urlAfterRedirects;

      if (isGaEnabled()) {
        ga('set', 'page', event[1].urlAfterRedirects);
        ga('send', 'pageview');
      }
    });
  }

  public init() {
    this.currentUrl = this.router.url;
  }

  public getPreviousUrl(): string | null {
    return this.previousUrl;
  }
}
