import {fromEvent, Observable, of, throwError, timer} from 'rxjs';
import {HttpErrorResponse, HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {catchError, concatMap, finalize, retryWhen, switchMap, tap} from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from '../shared/services/auth.service';
import {API_URI_PLACEHOLDER, LOCAL_STORAGE_AUTH_TOKEN_KEY} from './api';
import {Store} from '@ngrx/store';
import {RootState} from '../root-store';
import * as RootActions from '../root-store/root.actions';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  public constructor(
    private router: Router,
    private authService: AuthService,
    private store: Store<RootState.State>
  ) {
  }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.url.startsWith(API_URI_PLACEHOLDER)) {
      return next.handle(req);
    }

    const headers = {};
    if (localStorage.getItem(LOCAL_STORAGE_AUTH_TOKEN_KEY) != null) {
      headers['Authorization'] = localStorage.getItem(LOCAL_STORAGE_AUTH_TOKEN_KEY);
    }
    const newUrl = req.url.replace(API_URI_PLACEHOLDER,
      environment.production ? location.origin + '/api' : 'http://localhost:6382');

    const apiReq = req.clone({
      url: newUrl,
      setHeaders: headers
    });

    let causedError = false;

    // of null so that after subscribing we can immediately register beforeunload
    // event handler
    return of(null).pipe(
      switchMap(() => {
        const observable = next.handle(apiReq).pipe(
          // retry if connection / server troubles
          retryWhen((errors) => {
            return errors.pipe(
              concatMap((error, i) => {
                  if (error.status > 399 && error.status < 500) {
                    return throwError(error);
                  }

                  // allow 2 attempts for 5xx errors
                  if (error.status > 499 && i > 1 && [502, 503, 504, 521].indexOf(error.status) === -1) {
                    return throwError(error);
                  }

                  // after 2 attempt, show error message
                  if (!causedError && i > 1) {
                    causedError = true;
                    this.store.dispatch(new RootActions.IncreaseConnectionProblemCountAction());
                  }
                  // exponentially increase delay up to 2^7 seconds
                  return timer(Math.pow(2, Math.min(i, 7)) * 1000);
                },
              )
            );
          }),
          tap((event) => {
            // if caused error and was resolved
            if (event.type === HttpEventType.Response && causedError) {
              this.store.dispatch(new RootActions.DecreaseConnectionProblemCountAction());
            }
          }),
          catchError((err: HttpErrorResponse) => {
            // if 403, log out
            if (err.status === 401 || err.status === 403) {
              this.authService.logout();
            }

            return throwError(err);
          }),
        );

        if (apiReq.method !== 'GET') {
          // cannot use piping, would cause the pipeline to be asynchronous and
          // this event handler must be synchronous to work
          const subscription = fromEvent(window, 'beforeunload').subscribe((e: any) => {
            // Cancel the event
            e.preventDefault();
            // Chrome requires returnValue to be set
            e.returnValue = '';
          });

          return observable.pipe(
            finalize(() => {
              subscription.unsubscribe(); // if errored or succeeded, unsubscribe from before unload event
            })
          );
        } else {
          return observable;
        }
      })
    );
  }
}
