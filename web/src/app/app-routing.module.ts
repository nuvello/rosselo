import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {loadExternalModule} from './misc/other';

const routes: Routes = [
  {
    path: 'app', loadChildren: loadExternalModule(() => import('./news/news.module'), (data) => data.NewsModule)
  },
  {
    path: '', loadChildren: loadExternalModule(() => import('./presentation/presentation.module'), (data) => data.PresentationModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
