import {SourceView} from './source.view';

/**
 * DTO version of {@link Feed} sent by server.
 */
export interface FeedView {
  id: string;
  name: string;
  source: SourceView;
  categoryId: string;
  createdAt: Date;
  column: number;
  position: number;
  hideEntriesAfterRead: boolean;
  maxAmountOfItems: number | null;
  noOlderThan: number | null;
}
