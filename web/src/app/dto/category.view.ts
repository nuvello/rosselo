/**
 * DTO version of {@link Category} sent by server.
 */
export interface CategoryView {
  id: string;
  name: string;
  slug: string;
  position: number;
}
