import {SourceType} from "./source-type";

export interface CreateSourceResult {
  sourceId: string;
  sourceName: string;
  sourceData: string;
  sourceType: SourceType;
}
