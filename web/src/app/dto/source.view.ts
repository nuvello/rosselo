import {SourceType} from './source-type';

/**
 * DTO version of {@link Source} sent by server.
 */
export interface SourceView {
  id: string;
  type: SourceType;
  source: string;
  title: string;
  link: string;
  description: string | null;
  subscribers: number;
  error: boolean;
  lastSuccessfulFetchAt: Date;
  lastExtraScrapAt: Date;
  smallIconUrl: string | null;
  bigIconUrl: string | null;
}
