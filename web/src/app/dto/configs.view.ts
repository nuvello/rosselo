import {Big} from 'big.js';

export interface ConfigsView {
  trialPeriodInDays: number;
  premiumPriceYearWithoutVat: Big;
  maxNumberOfFeedsPerAccount: number;
  maxNumberOfCategoriesPerAccount: number;
  maxNumberOfEntriesPerFeed: number;
  defaultFeedHideEntriesAfterReadPreference: boolean;
  defaultFeedMaxAmountOfItemsPreference: number | null;
  defaultFeedNoOlderThanPreference: number | null;
  country: string | null;
  continent: string | null;
  currency: string;
}
