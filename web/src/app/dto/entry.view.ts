export interface EntryView {
  title: string;
  link: string;
}
