import {SourceType} from './source-type';

export interface SourceSearchView {
  id: string;
  type: SourceType;
  source: string;
  title: string;
  link: string;
  description: string | null;
  smallIconUrl: string | null;
  bigIconUrl: string | null;
  feedId: string | null;
  categoryName: string | null;
}
