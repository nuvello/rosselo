import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild
} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {RootState} from '../../../root-store';
import {Store} from '@ngrx/store';
import {Meta, Title} from '@angular/platform-browser';
import {AuthService} from '../../../shared/services/auth.service';
import {ConfigsView} from '../../../dto/configs.view';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'rs-sign-up-page',
    templateUrl: './sign-up-page.component.html',
    styleUrls: ['./sign-up-page.component.scss']
})
export class SignUpPageComponent implements OnInit, AfterViewInit, OnDestroy {
    public signUpForm: FormGroup;
    public buttonState = 1;
    public error: string;
    public errorClearTimeoutId: any;
    public configs: ConfigsView;
    public onDestroy$: Subject<null> = new Subject();

    @ViewChild('emailInput', {read: ElementRef, static: true})
    public emailInput: ElementRef;

    public get email(): AbstractControl {
        return this.signUpForm.get('email')!;
    }

    public get password(): AbstractControl {
        return this.signUpForm.get('password')!;
    }

    public get consent(): AbstractControl {
        return this.signUpForm.get('consent')!;
    }

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private store: Store<RootState.State>,
        private authService: AuthService,
        private title: Title,
        private meta: Meta
    ) {
    }

    public ngOnInit() {
        this.title.setTitle('Rosselo - Sign up for Rosselo account');
        this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
        this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

        this.signUpForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required, Validators.minLength(8)]],
            consent: [false, [Validators.required, Validators.requiredTrue]]
        });

        this.store.select(state => state.config).pipe(
            takeUntil(this.onDestroy$)
        ).subscribe((configs) => {
            this.configs = configs;
        });
    }

    public ngAfterViewInit(): void {
        setTimeout(() => {
            this.emailInput.nativeElement.focus();
        });
    }

    public ngOnDestroy(): void {
        this.onDestroy$.next(null);
        this.onDestroy$.complete();
    }

    public submit(): void {
        if (this.signUpForm.valid) {
            if (this.buttonState !== 2) {
                this.buttonState = 2;
                clearTimeout(this.errorClearTimeoutId);

                const formValue = this.signUpForm.value;
                this.authService.register(formValue.email, formValue.password)
                    .subscribe(() => {
                        this.buttonState = 3;
                        this.authService.login(formValue.email, formValue.password)
                            .subscribe(() => {
                                this.router.navigateByUrl('/app');
                            });
                        this.signUpForm.reset();
                    }, (error) => {
                        this.error = error;
                        this.buttonState = 4;
                        this.errorClearTimeoutId = setTimeout(() => {
                            this.buttonState = 1;
                        }, 2000);
                    });
            }
        } else {
            this.signUpForm.markAsTouched();
            this.signUpForm.markAsDirty();

            for (const controlsKey in this.signUpForm.controls) {
                if (this.signUpForm.controls.hasOwnProperty(controlsKey)) {
                    this.signUpForm.get(controlsKey).markAsTouched();
                    this.signUpForm.get(controlsKey).markAsDirty();
                }
            }

            let error = 'Invalid';

            if (this.email.errors?.required) {
                error = 'No e-mail entered';
            } else if (this.email.errors?.email) {
                error = 'Invalid e-mail';
            } else if (this.password.errors?.required) {
                error = 'No password entered';
            } else if (this.password.errors?.minlength) {
                error = 'Password is too weak';
            } else if (this.consent.errors?.required) {
                error = 'Unchecked terms';
            }

            this.error = error;
            this.buttonState = 4;
            clearTimeout(this.errorClearTimeoutId);
            this.errorClearTimeoutId = setTimeout(() => {
                this.buttonState = 1;
            }, 2000);
        }
    }
}
