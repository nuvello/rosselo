import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ViewportScroller} from '@angular/common';

@Component({
  selector: 'rs-terms-page',
  templateUrl: './terms-page.component.html',
  styleUrls: ['./terms-page.component.scss']
})
export class TermsPageComponent implements OnInit, AfterViewInit, OnDestroy {
  private onDestroy$: Subject<null> = new Subject();

  public constructor(
    private title: Title,
    private meta: Meta,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller
  ) {
  }

  public ngOnInit() {
    this.title.setTitle('Rosselo - Terms of Use');
    this.meta.updateTag({name: 'description', content: 'Learn about Rosselo Terms of Use and Privacy policy.'});
    this.meta.updateTag({name: 'tags', content: 'terms, privacy, policy, news, rss, feeds, dashboard, app, newsfeed'});
  }

  public ngAfterViewInit(): void {
    try {
      this.route.fragment.pipe(
        takeUntil(this.onDestroy$)
      ).subscribe(fragment => {
        if (fragment !== null) {
          this.scrollToAnchor(fragment);
        }
      });
    } catch (e) {
    }
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

  public scrollToAnchor(anchor: string, $event?: MouseEvent) {
    if ($event != null) {
      $event.preventDefault();
    }

    this.viewportScroller.scrollToAnchor(anchor);
    const scrolledY = window.scrollY;

    if (scrolledY) {
      window.scroll(0, scrolledY - document.querySelector('rs-layout header').clientHeight - 40);
    }
  }
}
