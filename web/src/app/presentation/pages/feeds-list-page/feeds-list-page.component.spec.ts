import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedsListPageComponent } from './feeds-list-page.component';

describe('FeedsListPageComponent', () => {
  let component: FeedsListPageComponent;
  let fixture: ComponentFixture<FeedsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
