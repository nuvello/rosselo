import { Component, OnInit } from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'rs-feeds-list-page',
  templateUrl: './feeds-list-page.component.html',
  styleUrls: ['./feeds-list-page.component.scss']
})
export class FeedsListPageComponent implements OnInit {

  constructor(private title: Title, private meta: Meta) {
  }

  ngOnInit() {
    this.title.setTitle('Rosselo - List of popular RSS feeds');
    this.meta.updateTag({name: 'description', content: 'Discover most popular RSS feeds to add to Rosselo'});
    this.meta.updateTag({name: 'tags', content: 'news, rss, feeds, newsfeed'});
  }
}
