import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../../root-store';
import {merge, of, Subject} from 'rxjs';
import {distinctUntilChanged, filter, switchMap, take, takeUntil} from 'rxjs/operators';
import {Meta, Title} from '@angular/platform-browser';
import {AuthService} from '../../../shared/services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthSelectors} from '../../../root-store/auth';

enum MetaState {
    DEFAULT = 0,
    WORKING = 1,
    SUCCESS = 2,
    ERROR = 3
}

@Component({
    selector: 'rs-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit, AfterViewInit, OnDestroy {
    public form: FormGroup;
    public mode: 'login' | 'reset-password' | 'set-password';

    public meta = {
        working: false,
        state: MetaState.DEFAULT,
        error: <string>null,
        resetTimeoutId: <any>null
    };
    private onDestroy$: Subject<void> = new Subject();

    @ViewChild('emailInput', {read: ElementRef, static: false})
    public emailInput: ElementRef;

    public get email(): AbstractControl {
        return this.form.get('email')!;
    }

    public get password(): AbstractControl {
        return this.form.get('password')!;
    }

    constructor(
        private formBuilder: FormBuilder,
        private authService: AuthService,
        private store: Store<RootState.State>,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private title: Title,
        private metaTags: Meta
    ) {
    }

    private success(): void {
        this.resetMeta();
        this.meta.state = MetaState.SUCCESS;
        this.meta.resetTimeoutId = setTimeout(() => this.resetMeta(), 2000);
    }

    private error(message: string): void {
        this.resetMeta();
        this.meta.error = message;
        this.meta.state = MetaState.ERROR;
        this.meta.resetTimeoutId = setTimeout(() => this.resetMeta(), 3000);
    }

    private resetMeta(): void {
        if (this.meta.state !== MetaState.DEFAULT) {
            clearTimeout(this.meta.resetTimeoutId);
            this.meta.state = MetaState.DEFAULT;
        }
    }

    public ngOnInit() {
        this.title.setTitle('Rosselo - Login');
        this.metaTags.updateTag({name: 'description', content: 'Login page for Rosselo app.'});
        this.metaTags.updateTag({name: 'tags', content: 'login, app, news, rss, feeds, dashboard, newsfeed'});

        this.store.pipe(
            select(AuthSelectors.getAuthState),
            take(1),
            filter((auth) => auth.loggedIn),
        ).subscribe(() => {
            setTimeout(() => this.router.navigateByUrl('/app', {replaceUrl: true}));
        });

        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });

        merge(this.router.events, of(null))
            .pipe(
                takeUntil(this.onDestroy$),
                switchMap(() => this.activatedRoute.children[0].url),
                distinctUntilChanged()
            )
            .subscribe((url) => {
                if (url.length > 0) {
                    this.setMode(<any>url[0].path);
                }
            });
    }

    public ngAfterViewInit(): void {
        setTimeout(() => {
            this.emailInput.nativeElement.focus();
        });
    }

    public ngOnDestroy(): void {
        this.onDestroy$.next();
        this.onDestroy$.complete();
    }

    public submit(): void {
        if (this.form.valid) {
            if (this.meta.state !== MetaState.WORKING) {
                if (this.mode === 'login') {
                    this.login();
                } else if (this.mode === 'reset-password') {
                    this.resetPassword();
                } else {
                    this.setPassword();
                }
            }
        } else {
            this.form.markAsTouched();
            this.form.markAsDirty();

            for (const controlsKey in this.form.controls) {
                if (this.form.controls.hasOwnProperty(controlsKey)) {
                    this.form.get(controlsKey).markAsTouched();
                    this.form.get(controlsKey).markAsDirty();
                }
            }

            if (this.email.errors?.required) {
                this.error('No e-mail entered');
            } else if (this.email.errors?.email) {
                this.error('Invalid e-mail');
            } else if (this.password.errors) {
                this.error('No password entered');
            }
        }
    }

    private resetPassword() {
        this.resetMeta();
        this.meta.state = MetaState.WORKING;
        this.authService.requestPasswordReset(this.form.value.email)
            .subscribe(() => {
                this.meta.state = MetaState.SUCCESS;
                this.success();
                this.password.reset();
            }, (error) => {
                this.error(error);
            });
    }

    private setPassword() {
        this.resetMeta();
        this.meta.state = MetaState.WORKING;
        this.activatedRoute.children[0].params.subscribe((params) => {
            const email = params.email;
            const token = params.token;

            this.authService.resetPassword(email, token, this.form.value.password)
                .pipe(
                    switchMap(() => {
                        return this.authService.login(email, this.form.value.password);
                    })
                )
                .subscribe(() => {
                    this.success();
                    this.afterLoggedIn();
                }, (error) => {
                    this.error(error);
                });
        });
    }

    private login() {
        this.resetMeta();
        this.meta.state = MetaState.WORKING;
        this.authService.login(this.form.value.email, this.form.value.password)
            .subscribe(() => {
                this.success();
                this.afterLoggedIn();
            }, (error) => {
                this.error(error);
            });
    }

    public setMode(value: 'login' | 'reset-password' | 'set-password') {
        this.mode = value;
        switch (value) {
            case 'login':
                this.email.enable();
                this.password.enable();
                this.router.navigateByUrl('/login');
                return;
            case  'set-password':
                this.email.disable();
                this.password.enable();
                return;
            case  'reset-password':
                this.email.enable();
                this.password.disable();
                this.router.navigateByUrl('/reset-password');
                return;
        }
    }

    private afterLoggedIn() {
        this.router.navigateByUrl('/app');
    }
}
