import {Component, OnDestroy, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {fromEvent, Subject} from 'rxjs';
import {startWith, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'rs-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit, OnDestroy {
  public mobileLayout: boolean;
  private onDestroy$: Subject<void> = new Subject();

  constructor(private title: Title, private meta: Meta) {
  }

  ngOnInit() {
    this.title.setTitle('Rosselo - Simple dashboard for all your RSS news feeds');
    this.meta.updateTag({
      name: 'description',
      content: 'Gather all your favorite RSS news feeds into one simple place. Following news has never been easier.'
    });
    this.meta.updateTag({name: 'tags', content: 'news, rss, feeds, dashboard, app, newsfeed'});

    fromEvent(window, 'resize').pipe(
      startWith(null),
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      this.mobileLayout = window.innerWidth <= 768;
    });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }
}
