import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';
import {ViewportScroller} from '@angular/common';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {ConfigsView} from '../../../dto/configs.view';
import {Store} from '@ngrx/store';
import {RootState} from '../../../root-store';

@Component({
  selector: 'rs-questions-page',
  templateUrl: './questions-page.component.html',
  styleUrls: ['./questions-page.component.scss']
})
export class QuestionsPageComponent implements OnInit, AfterViewInit, OnDestroy {
  public configs$: Observable<ConfigsView>;

  private onDestroy$: Subject<null> = new Subject();

  constructor(
    private title: Title,
    private meta: Meta,
    private route: ActivatedRoute,
    private store: Store<RootState.State>,
    private viewportScroller: ViewportScroller
  ) {
  }

  public ngOnInit() {
    this.title.setTitle('Rosselo - Frequently Asked Questions');
    this.meta.updateTag({name: 'description', content: 'The most common question and answers about Rosselo app.'});
    this.meta.updateTag({name: 'tags', content: 'news, rss, feeds, dashboard, app, newsfeed'});

    this.configs$ = this.store.select((data) => data.config);
  }

  public ngAfterViewInit(): void {
    try {
      this.route.fragment.pipe(
        takeUntil(this.onDestroy$)
      ).subscribe(fragment => {
        if (fragment !== null) {
          this.scrollToAnchor(fragment);
        }
      });
    } catch (e) {
    }
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

  public scrollToAnchor(anchor: string, $event?: MouseEvent) {
    if ($event != null) {
      $event.preventDefault();
    }

    this.viewportScroller.scrollToAnchor(anchor);
    const scrolledY = window.scrollY;

    if (scrolledY) {
      window.scroll(0, scrolledY - document.querySelector('rs-layout header').clientHeight - 40);
    }
  }
}
