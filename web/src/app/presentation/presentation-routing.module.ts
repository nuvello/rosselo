import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexPageComponent} from './pages/index-page/index-page.component';
import {LayoutComponent} from './components/layout/layout.component';
import {QuestionsPageComponent} from './pages/questions-page/questions-page.component';
import {FeedsListPageComponent} from './pages/feeds-list-page/feeds-list-page.component';
import {TermsPageComponent} from './pages/terms-page/terms-page.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {SignUpPageComponent} from './pages/sign-up-page/sign-up-page.component';
import {NotFoundPageComponent} from './pages/not-found-page/not-found-page.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: IndexPageComponent
      },
      {
        path: 'questions',
        component: QuestionsPageComponent
      },
      {
        path: 'feeds-list',
        component: FeedsListPageComponent
      },
      {
        path: 'terms',
        component: TermsPageComponent
      },
      {
        path: 'signup',
        redirectTo: 'sign-up'
      },
      {
        path: 'sign-up',
        component: SignUpPageComponent
      },
      {
        path: '',
        component: LoginPageComponent,
        children: [
          {
            path: 'login'
          },
          {
            path: 'reset-password'
          },
          {
            path: 'set-password/:email/:token',
          }
        ]
      },
      {
        path: '**',
        component: NotFoundPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PresentationRoutingModule {
}
