import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {fromEvent, merge, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'rs-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
  animations: [
    trigger('slide', [
      state('open', style({
        height: '*',
      })),
      state('closed', style({
        height: 0,
        display: 'none'
      })),
      transition('open <=> closed', animate('0.2s ease')),
    ])
  ]
})
export class LayoutComponent implements OnInit, OnDestroy {
  public showMenu = false;
  public mobile = false;
  public currentYear = new Date().getFullYear();

  private onDestroy$ = new Subject();


  public constructor(
    public router: Router
  ) {
  }

  public ngOnInit() {
    if (window.innerWidth <= 768) {
      this.mobile = true;
    }

    merge(
      fromEvent(window, 'click'),
      fromEvent(window, 'contextmenu'),
      fromEvent(window, 'keyup').pipe(
        filter((event: KeyboardEvent) => event.key === 'Escape')
      )
    ).pipe(
      takeUntil(this.onDestroy$)
    )
      .subscribe(() => {
        if (this.showMenu) {
          this.showMenu = false;
        }
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

  @HostListener('window:resize')
  public onWindowResize() {
    const mobile = window.innerWidth <= 768;
    if (mobile && !this.mobile) {
      this.showMenu = false;
    }
    this.mobile = mobile;
  }

  public scrollToTop() {
    window.scrollTo({top: 0});
  }
}
