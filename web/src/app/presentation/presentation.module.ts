import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PresentationRoutingModule } from './presentation-routing.module';
import { IndexPageComponent } from './pages/index-page/index-page.component';
import {SharedModule} from '../shared/shared.module';
import { LayoutComponent } from './components/layout/layout.component';
import { QuestionsPageComponent } from './pages/questions-page/questions-page.component';
import { FeedsListPageComponent } from './pages/feeds-list-page/feeds-list-page.component';
import { TermsPageComponent } from './pages/terms-page/terms-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { SignUpPageComponent } from './pages/sign-up-page/sign-up-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import { NotFoundPageComponent } from './pages/not-found-page/not-found-page.component';

@NgModule({
  declarations: [IndexPageComponent, LayoutComponent, QuestionsPageComponent, FeedsListPageComponent,  TermsPageComponent, LoginPageComponent, SignUpPageComponent, NotFoundPageComponent],
  imports: [
    CommonModule,
    SharedModule,
    PresentationRoutingModule,
    ReactiveFormsModule
  ]
})
export class PresentationModule { }
