import {Component, Injector, OnInit} from '@angular/core';
import {RouteConfigLoadEnd, RouteConfigLoadStart, Router} from '@angular/router';
import {delay, filter, map, switchMap} from 'rxjs/operators';
import {interval, merge, Observable, of, race} from 'rxjs';
import {spinnerFadeAnimation} from './misc/animations';
import {RouterExtService} from './misc/router.extension';
import {Store} from '@ngrx/store';
import {RootState} from './root-store';
import {AuthService} from './shared/services/auth.service';

@Component({
  selector: 'rs-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    spinnerFadeAnimation
  ]
})
export class AppComponent implements OnInit {
  // todo ugly
  static injector: Injector;
  public loadingRouteConfig: boolean;
  public connectionProblemsCount$: Observable<number>;
  public configLoaded: boolean;

  public constructor(
    private router: Router,
    private routerExtService: RouterExtService,
    private store: Store<RootState.State>,
    private authService: AuthService,
    injector: Injector
  ) {
    AppComponent.injector = injector;
  }

  public ngOnInit() {
    this.connectionProblemsCount$ = this.store.select((state) => state.connectionProblemsCount);
    this.store.select((state) => state.config).pipe(
      map((config) => config !== null)
    ).subscribe((loaded) => {
      this.configLoaded = loaded;
    });
    this.routerExtService.init();

    const end$ = this.router.events.pipe(
      filter((event) => event instanceof RouteConfigLoadEnd),
      map(() => false),
    );
    const start$ = this.router.events.pipe(
      filter((event) => event instanceof RouteConfigLoadStart),
      map(() => true),
    );

    merge(
      end$,
      start$.pipe(
        switchMap(() => {
          return race(end$, of(true).pipe(delay(300)));
        }),
        filter((value) => value === true)
      )
    ).subscribe((value) => {
      this.loadingRouteConfig = value;
    });

    this.authService.refreshTokenIfNeeded();
    interval(60 * 1000 * 10).subscribe(() => this.authService.refreshTokenIfNeeded());
  }
}
