import {BrowserModule} from '@angular/platform-browser';
import {ApplicationRef, NgModule} from '@angular/core';
import {createInputTransfer, createNewHosts, removeNgStyles} from '@angularclass/hmr';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {Store, StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {rootMetaReducers, rootReducers, RootState} from './root-store';
import {EffectsModule} from '@ngrx/effects';
import {AuthEffects} from './root-store/auth/auth.effects';
import {SharedModule} from './shared/shared.module';
import {take} from 'rxjs/operators';
import {RouterExtService} from './misc/router.extension';
import {RootEffects} from './root-store/root.effects';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'rosselo-app'}),
    BrowserAnimationsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(rootReducers, {metaReducers: rootMetaReducers}),
    EffectsModule.forRoot([RootEffects, AuthEffects]),
    !environment.production ? StoreDevtoolsModule.instrument({
      maxAge: 25
    }) : [],
    SharedModule.forRoot(),
    AppRoutingModule,
  ],
  providers: [
    RouterExtService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    public appRef: ApplicationRef,
    public store: Store<RootState.State>
  ) {
  }

  hmrOnInit(store) {
    if (!store || !store.state) {
      return;
    }
    console.log('HMR store', store);
    console.log('store.state.data:', store.state.data);
    // inject AppStore here and update it
    // this.AppStore.update(store.state)

    this.store.dispatch({
      type: 'SET_ROOT_STATE',
      payload: store.state
    });

    if ('restoreInputValues' in store) {
      const restore = store.restoreInputValues;
      setTimeout(() => restore(), 500);
    }
    // change detection
    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // inject your AppStore and grab state then set it on store
    this.store.pipe(take(1)).subscribe(s => store.state = s);
    store.state = Object.assign({}, store.state);
    // save input values
    store.restoreInputValues = createInputTransfer();
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
    // anything you need done the component is removed
  }
}
