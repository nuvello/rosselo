import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ButtonComponent} from './components/button/button.component';
import {HamburgerComponent} from './components/hamburger/hamburger.component';
import {IconComponent} from './components/icon/icon.component';
import {SkewContainerComponent} from './components/skew-container/skew-container.component';
import {ContainerComponent} from './components/container/container.component';
import {StateButtonComponent} from './components/state-button/state-button.component';
import {AuthService} from './services/auth.service';
import {SpinnerComponent} from './components/spinner/spinner.component';
import {SwitchComponent} from './components/switch/switch.component';
import {HTTP_INTERCEPTORS, HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {ApiInterceptor} from '../misc/api.interceptor';
import {TimeAgoPipe} from './pipes/time-ago.pipe';
import {HotkeysTableComponent} from './components/hotkeys-table/hotkeys-table.component';
import { AdvancedSelectComponent } from './components/advanced-select/advanced-select.component';
import {FormsModule} from '@angular/forms';
import {ActionStateButtonComponent} from './components/action-state-button/action-state-button.component';

@NgModule({
  declarations: [
    ButtonComponent,
    HamburgerComponent,
    IconComponent,
    SkewContainerComponent,
    ContainerComponent,
    StateButtonComponent,
    SpinnerComponent,
    SwitchComponent,
    TimeAgoPipe,
    HotkeysTableComponent,
    HotkeysTableComponent,
    AdvancedSelectComponent,
    ActionStateButtonComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    ButtonComponent,
    HamburgerComponent,
    IconComponent,
    SkewContainerComponent,
    ContainerComponent,
    StateButtonComponent,
    SpinnerComponent,
    SwitchComponent,
    TimeAgoPipe,
    HotkeysTableComponent,
    AdvancedSelectComponent,
    ActionStateButtonComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        AuthService,
      ]
    };
  }
}
