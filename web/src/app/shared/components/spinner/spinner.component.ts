import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'rs-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @HostBinding('class')
  @Input()
  public mode: 'rosselo' | 'basic' = 'rosselo';

  constructor() {
  }

  ngOnInit() {
  }

}
