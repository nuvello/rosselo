import {ChangeDetectionStrategy, Component, HostBinding, Input} from '@angular/core';

export type IconType =
    'search'
    | 'options'
    | 'person'
    | 'glasses'
    | 'indicator-full'
    | 'tick'
    | 'cross'
    | 'arrow-small'
    | 'plus'
    | 'glasses-small'
    | 'menu-delete'
    | 'menu-more'
    | 'add'
    | 'logo'
    | 'heart'
    | 'hotkeys'
    | 'logout'
    | 'menu'
    | 'notello-logo'
    | 'menu-2'
    | 'sort'
    | 'rss'
    | 'plus-thinner'
    | 'folder'
    | 'indicator'
    | 'import'
    | 'hash'
    | 'youtube'
    | 'twitter'
    | 'news'
    | 'education'
    | 'entertainment'
    | 'politics'
    | 'science'
    | 'skills'
    | 'society'
    | 'lifestyle'
    | 'media'
    | 'sport'
    | 'tech'
    | 'business';

@Component({
    selector: 'rs-icon',
    templateUrl: './icon.component.html',
    styleUrls: ['./icon.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IconComponent {
    @Input()
    @HostBinding('class')
    public type: IconType;

    @HostBinding('attr.area-hidden')
    public areaHidden = true;
}
