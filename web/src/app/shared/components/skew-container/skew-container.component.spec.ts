import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkewContainerComponent } from './skew-container.component';

describe('SkewContainerComponent', () => {
  let component: SkewContainerComponent;
  let fixture: ComponentFixture<SkewContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkewContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkewContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
