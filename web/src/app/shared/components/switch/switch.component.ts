import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'rs-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {

  @HostBinding('class.checked')
  @Input()
  public checked = false;

  @HostBinding('class.disabled')
  @Input()
  public disabled = false;

  @Output()
  public checkedChange = new EventEmitter<boolean>();

  @Output()
  public enter = new EventEmitter<void>();

  ngOnInit() {
  }

  public toggle(): void {
    if (this.disabled) {
      return;
    }
    this.checked = !this.checked;
    this.checkedChange.emit(this.checked);
  }

  public onKeyDown($event: KeyboardEvent) {
    if ($event.code === 'Space') {
      $event.preventDefault();

      if (!this.disabled) {
        this.toggle();
      }
    } else if ($event.code === 'Enter') {
      this.enter.emit(void 0);
    }
  }
}
