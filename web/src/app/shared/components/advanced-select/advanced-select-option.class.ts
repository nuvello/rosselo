import {IconType} from '../icon/icon.component';

export class AdvancedSelectOption<T> {
    public constructor(
        public name: string,
        public icon: IconType | null,
        public data: T
    ) {
    }
}
