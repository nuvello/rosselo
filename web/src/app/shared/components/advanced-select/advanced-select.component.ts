import {
    Component,
    ElementRef,
    EventEmitter,
    HostBinding,
    HostListener,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {AdvancedSelectOption} from './advanced-select-option.class';
import {animate, style, transition, trigger} from '@angular/animations';
import {isMobile} from '../../../misc/other';

@Component({
    selector: 'rs-advanced-select',
    templateUrl: './advanced-select.component.html',
    styleUrls: ['./advanced-select.component.scss'],
    animations: [
        trigger('slide', [
            transition(':enter', [
                style({
                    height: 0,
                    opacity: 0
                }),
                animate('0.1s ease', style({
                    height: '*',
                    opacity: 1
                }))
            ]),
            transition(':leave', [
                animate('0.1s ease', style({
                    height: 0,
                    display: 'none',
                    opacity: 0
                }))
            ])
        ])
    ]
})
export class AdvancedSelectComponent<T> implements OnInit, OnChanges {
    @Input()
    public options: AdvancedSelectOption<T>[];
    @Input()
    public value: AdvancedSelectOption<T> | AdvancedSelectOption<T>[] | null;
    @Input()
    public anyOption: AdvancedSelectOption<T> | null = new AdvancedSelectOption<any>('any', null, null);
    @Input()
    @HostBinding('class.highlight-selected-by-background')
    public highlightSelectedByBackground = false;
    @Input()
    @HostBinding('class.robust-layout')
    public robustLayout = false;
    @Input()
    @HostBinding('class.disabled')
    public disabled = false;
    @Input()
    public multiple = false;
    @Input()
    public placeholder = 'Pick a value';
    @Input()
    public enableSearch = false;

    @Output()
    public valueChange: EventEmitter<AdvancedSelectOption<T> | AdvancedSelectOption<T>[]> = new EventEmitter();
    @ViewChild('nativeSelect', {read: ElementRef, static: true})
    public nativeSelect: ElementRef;
    public toggle = false;
    public focused = null;
    public isMobile = false;
    public searchTerm = '';
    public visibleOptions: AdvancedSelectOption<T>[];
    public allSelected = false;

    public ngOnInit() {
        this.isMobile = isMobile();
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (this.value === undefined) {
            this.value = this.multiple ? [] : null;
        }

        if (changes.value || changes.multiple) {
            if (this.multiple) {
                if (!Array.isArray(this.value)) {
                    throw new Error('Value has to be array if multiple');
                }

                this.allSelected = this.options.length === this.value.length;
            } else {
                if (this.value !== null && this.options.indexOf(<any>this.value) === -1) {
                    throw new Error('Value has to be one of the options');
                }
            }
        }

        if (changes.options) {
            this.visibleOptions = this.options;
            this.focused = 0;
            if (this.enableSearch && this.searchTerm.length > 0) {
                this.search(this.searchTerm);
            }
        }

        if (changes.anyOption || changes.multiple) {
            if (this.anyOption !== null && !this.multiple) {
                throw new Error('Any option not supported for single option select.');
            }
        }
    }

    public select(option: AdvancedSelectOption<T>) {
        if (this.disabled) {
            return;
        }

        if (this.multiple) {
            if (this.anyOption === option) {
                this.setActive(option, !(this.options.length === (<[]>this.value).length));
            } else {
                const array: AdvancedSelectOption<T>[] = <any>this.value;

                const index = array.indexOf(option);

                this.setActive(option, index === -1);
            }
        } else {
            this.value = option;
            this.searchTerm = '';

            if (this.toggle) {
                this._toggle();
            }
            this.valueChange.emit(this.value);
        }
    }

    public setActive(option: AdvancedSelectOption<T>, active: boolean) {
        if (option === this.anyOption) {
            if (active) {
                this.value = this.options.slice();
            } else {
                this.value = [];
            }
            this.allSelected = active;
        } else {
            const array: AdvancedSelectOption<T>[] = <any>this.value;

            const index = array.indexOf(option);
            if (active) {
                if (index === -1) {
                    array.push(option);
                }
            } else {
                if (index !== -1) {
                    array.splice(index, 1);
                }
            }

            this.allSelected = array.length === this.options.length;
        }

        this.valueChange.emit(this.value);
    }

    public onKeydown($event: KeyboardEvent) {
        if (this.disabled) {
            return;
        }

        if (['Enter', 'Space', 'ArrowDown', 'ArrowUp'].indexOf($event.code) && !this.toggle) {
            $event.preventDefault();
            this._toggle();

            if (this.multiple || this.value === null) {
                this.focused = 0;
            } else {
                this.focused = this.visibleOptions.indexOf(<any>this.value) + this.getOptionsOffset();
            }
        } else if ($event.code === 'Enter' || $event.code === 'Space') {
            $event.preventDefault();

            if (this.focused !== null && this.visibleOptions.length > this.focused - this.getOptionsOffset()) {
                if (this.anyOption !== null && this.focused === 0) {
                    this.select(this.anyOption);
                } else {
                    this.select(this.visibleOptions[this.focused - this.getOptionsOffset()]);
                }
            }
        } else if ($event.key === 'ArrowDown') {
            $event.preventDefault();

            if (this.focused === null) {
                this.focused = 0;
            } else {
                this.focused = Math.min(this.visibleOptions.length - 1 + this.getOptionsOffset(), this.focused + 1);
            }
        } else if ($event.key === 'ArrowUp') {
            $event.preventDefault();
            if (this.focused === null) {
                this.focused = 0;
            } else {
                this.focused = Math.max(0, this.focused - 1);
            }
        } else if ($event.key === 'Home') {
            $event.preventDefault();
            this.focused = 0;
        } else if ($event.key === 'End') {
            $event.preventDefault();
            this.focused = this.visibleOptions.length - 1 + this.getOptionsOffset();
        } else if ($event.key === 'Escape') {
            if (this.toggle) {
                $event.stopPropagation();
            }
        }
    }

    private getOptionsOffset() {
        return this.anyOption === null ? 0 : 1;
    }

    public onKeyup($event: KeyboardEvent) {
        if (this.disabled) {
            return;
        }

        if ($event.key === 'Escape') {
            if (this.toggle) {
                this._toggle();
                $event.preventDefault();
                $event.stopPropagation();
            }
        }
    }

    public _toggle() {
        if (this.disabled) {
            return;
        }

        if (this.toggle) {
            this.focused = null;
            this.toggle = !this.toggle;
        } else {
            this.toggle = !this.toggle;
        }
    }

    @HostListener('click', ['$event'])
    public onClick(event: KeyboardEvent): void {
        event.stopPropagation();
    }

    @HostListener('window:click', ['$event'])
    public onWindowClick(event: KeyboardEvent): void {
        if (this.toggle) {
            this._toggle();
        }
    }

    public getValueRepresentation(): string {
        if (this.multiple) {
            const arr = <AdvancedSelectOption<any>[]>this.value;

            if (arr.length === 0) {
                return null;
            }

            return arr.map(val => val.name).join(', ');
        } else {
            if (this.value === null) {
                return null;
            }

            return (<AdvancedSelectOption<any>>this.value).name;
        }
    }

    public search(term: string) {
        this.searchTerm = term;

        let focusedItem = null;

        if (this.anyOption !== null && this.focused === 0) {
            focusedItem = this.anyOption;
        } else if (this.focused !== null && this.focused < this.visibleOptions.length + this.getOptionsOffset()) {
            focusedItem = this.visibleOptions[this.focused - this.getOptionsOffset()];
        }

        if (term.length > 0) {
            this.visibleOptions = this.options.filter(val => val.name.toLowerCase().indexOf(term.toLowerCase()) !== -1);
        } else {
            this.visibleOptions = this.options;
        }

        if (this.anyOption !== focusedItem) {
            const index = this.visibleOptions.indexOf(focusedItem);

            this.focused = index !== -1 ? index + this.getOptionsOffset() : 0;
        }
    }
}

