import {Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';

/**
 * Special component for buttons with several states (max 7). Each state has its
 * own html element with CSS class state-{state number}. To tell the component
 * which state should be shown, use property {@link _state}.
 *
 * Width of the button must be set manually by parent component.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Component({
    selector: 'rs-state-button',
    templateUrl: './state-button.component.html',
    styleUrls: ['./state-button.component.scss'],
    animations: [
        trigger('slide', [
            state('hidden', style({
                position: 'absolute',
                top: '-40px'
            })),
            transition('hidden => visible', [
                style({
                    position: 'relative',
                }),
                animate('0.2s ease', style({
                    top: 0,
                }))
            ]),
            transition('visible => hidden', [
                style({
                    position: 'absolute',
                }),
                animate('0.2s ease', style({
                    marginTop: '40px',
                }))
            ])
        ])
    ]
})

export class StateButtonComponent {
    @HostBinding('class')
    get class() {
        let clazz = 'align-' + this.align + ' bg-' + this.color + ' ' + this.type;

        if (this.working) {
            clazz += ' working';
        }

        if (this.disabled) {
            clazz += ' disabled';
        }

        return clazz;
    }

    private _state = 1;

    @Input()
    public align: 'left' | 'center' = 'left';

    @Input()
    public color: 'default' | 'success' | 'error' = 'default';

    @Input()
    public type: 'normal' | 'bordered' = 'normal';

    @Input()
    public working = false;

    @Input()
    public disabled = false;

    @Output()
    public rsClick = new EventEmitter<void>();

    @Output()
    public rsSubmit = new EventEmitter<void>();

    // HOW DOES THIS WORK?
    // we keep count of animations happening
    // when state is set and no animation is happening, switch to new state is done immediately
    // if not, it is done after all animations are done
    get state(): number {
        return this._state;
    }

    @Input()
    set state(value: number) {
        this._state = value;
        this.updateVisible();
    }

    public currentState = 1;
    public animationsCount = 0;

    public animationStart() {
        this.animationsCount++;
    }

    public animationDone() {
        this.animationsCount--;

        this.updateVisible();
    }

    private updateVisible() {
        setTimeout(() => {
            if (this.animationsCount === 0) {
                this.currentState = this._state;
            }
        });
    }

    @HostListener('click')
    public onClick() {
        this.rsClick.emit();
        if (!this.disabled) {
            this.rsSubmit.emit();
        }
    }
}
