import {Component, HostBinding, HostListener, Input} from '@angular/core';

@Component({
  selector: 'rs-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @HostBinding('class')
  get class() {
    const _class = this.type + ' ' + this.align + ' ' + this.weight + ' ' + this.clazz + ' ';

    return this.active ? _class + ' active' : _class;
  }

  @Input()
  public active = false;

  @Input()
  public type: 'normal' | 'filled' | 'bordered' = 'normal';

  @Input()
  public align: 'left' | 'center' | 'right' = 'center';

  @Input()
  public weight: 'bold' | 'normal' = 'bold';

  // component overwrites class
  @Input()
  public clazz = '';

  @HostListener('mousedown', ['$event'])
  private onHostMousedown(event) {
    event.preventDefault(); // to disable focus after click
  }
}
