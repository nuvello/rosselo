import {Component, HostBinding, Input, OnInit} from '@angular/core';

@Component({
  selector: 'rs-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit {
  @Input()
  @HostBinding('class.narrow')
  public narrow = false;

  constructor() { }

  ngOnInit() {
  }

}
