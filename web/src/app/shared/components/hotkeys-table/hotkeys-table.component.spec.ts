import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotkeysTableComponent } from './hotkeys-table.component';

describe('HotkeysTableComponent', () => {
  let component: HotkeysTableComponent;
  let fixture: ComponentFixture<HotkeysTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotkeysTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotkeysTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
