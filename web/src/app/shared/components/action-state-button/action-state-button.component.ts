import {ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output} from '@angular/core';

@Component({
    selector: 'rs-action-state-button',
    templateUrl: './action-state-button.component.html',
    styleUrls: ['./action-state-button.component.scss']
})

export class ActionStateButtonComponent {
    @Input()
    public align: 'left' | 'center' = 'left';

    @Input()
    public disabled = false;

    @Output()
    public rsClick = new EventEmitter<void>();

    @Output()
    public rsSubmit = new EventEmitter<void>();

    @HostBinding('class.icon-only')
    @Input()
    public iconOnly = false;

    @Input()
    public defaultIcon = null;

    @Input()
    public type: 'normal' | 'bordered' = 'normal';

    @Input()
    @HostBinding('class')
    public state: 'default' | 'working' | 'done' | 'error' = 'default';

    @Input()
    public defaultText = 'Do';
    @Input()
    public workingText = 'Working';
    @Input()
    public doneText = 'Done';
    @Input()
    public errorText = 'Error';


    public calculateState() {
        switch (this.state) {
            case 'default':
                return 1;
            case 'working':
                return 2;
            case 'done':
                return 3;
            case 'error':
                return 4;
        }
    }

    public calculateColor() {
        switch (this.state) {
            case 'default':
                return 'default';
            case 'working':
                return 'default';
            case 'done':
                return 'success';
            case 'error':
                return 'error';
        }
    }
}
