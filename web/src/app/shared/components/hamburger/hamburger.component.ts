import {Component, EventEmitter, HostBinding, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'rs-hamburger',
  templateUrl: './hamburger.component.html',
  styleUrls: ['./hamburger.component.scss']
})
export class HamburgerComponent {
  @Input()
  public active = false;

  @Output()
  public activeChange = new EventEmitter<boolean>();

  @HostListener('click', ['$event'])
  public onHostClick(event: MouseEvent): void {
   this.active = !this.active;
   this.activeChange.emit(this.active);
   event.preventDefault();
   event.stopPropagation();
  }
}
