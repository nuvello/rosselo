import {Injectable} from '@angular/core';
import {forkJoin, Observable, throwError} from 'rxjs';
import {catchError, map, switchMap, take, tap} from 'rxjs/operators';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../root-store';
import {AuthActions, AuthSelectors} from '../../root-store/auth';
import {ChangeEmailAction, TerminateAccountAction, UpdatePreferencesAction} from '../../root-store/auth/auth.actions';
import {HttpClient} from '@angular/common/http';
import {
  API_URI_PLACEHOLDER,
  HIDE_TRIAL_NOTIFICATION_TOKEN_KEY,
  LOCAL_STORAGE_AUTH_TOKEN_KEY,
  parseDateFromApi,
  parseJwtToken
} from '../../misc/api';
import {ActivePlan, Preferences, User} from '../../root-store/auth/auth.state';
import {ConfigsView} from '../../dto/configs.view';
import {Big} from 'big.js';
import {isGaEnabled} from '../../misc/ga';

@Injectable()
export class AuthService {

  public constructor(
    private store: Store<RootState.State>,
    private http: HttpClient
  ) {
  }

  public isLoggedIn(): boolean {
    return localStorage.getItem(LOCAL_STORAGE_AUTH_TOKEN_KEY) !== null;
  }

  public refreshTokenIfNeeded(): void {
    if (!this.isLoggedIn()) {
      return;
    }

    const token = parseJwtToken(localStorage.getItem(LOCAL_STORAGE_AUTH_TOKEN_KEY));

    if (token.exp <= new Date().getTime() / 1000 + 60 * 60 * 24 * 7) {
      this.http.get(API_URI_PLACEHOLDER + '/accounts/refresh-token', {
        observe: 'response'
      }).subscribe(response => {
        localStorage.setItem(LOCAL_STORAGE_AUTH_TOKEN_KEY, response.headers.get('Authorization'));
      });
    }
  }

  public requestPasswordReset(email: string): Observable<void> {
    return this.http.post(API_URI_PLACEHOLDER + '/accounts/request-password-reset', email).pipe(
      map(() => void 0)
    );
  }

  public resetPassword(email: string, token: string, newPassword: string): Observable<void> {
    return this.http.post(API_URI_PLACEHOLDER + '/accounts/password-reset', {
      email: email,
      confirmToken: token,
      newPassword: newPassword
    }).pipe(
      map(() => void 0),
      catchError(() => throwError('Invalid token'))
    );
  }

  public logout(): void {
    localStorage.removeItem(LOCAL_STORAGE_AUTH_TOKEN_KEY);
    this.store.dispatch(new AuthActions.LoggedOutAction());
  }

  public login(email: string, password: string): Observable<void> {
    return this.http.post(API_URI_PLACEHOLDER + '/accounts/login', {
      email: email,
      password: password
    }, {
      observe: 'response'
    }).pipe(
      tap((response) => {
        localStorage.setItem(LOCAL_STORAGE_AUTH_TOKEN_KEY, response.headers.get('Authorization'));
        this.store.dispatch(new AuthActions.LoggedInAction());
      }),
      map(() => null),
      catchError((error) => {
        if (error.error && error.error.message === 'Auth limit exceeded.') {
          return throwError('Auth limit exceeded');
        }
        return throwError('Wrong e-mail or password');
      })
    );
  }

  public register(email: string, password: string): Observable<void> {
    return this.http.post(API_URI_PLACEHOLDER + '/accounts', {
      email: email,
      password: password
    }).pipe(
      map(() => null),
      tap(() => {
        if (isGaEnabled()) {
          ga('send', 'event', 'conversion', 'signup', 'form', 1);
        }
      }),
      catchError((error) => {
        if (error.error.details === 'E-mail already used.') {
          return throwError('E-mail already used');
        }
        return throwError('Unexpected error');
      })
    );
  }

  public terminateAccount(): Observable<void> {
    return this.http.delete(API_URI_PLACEHOLDER + '/accounts/current').pipe(
      tap(() => {
        localStorage.removeItem(LOCAL_STORAGE_AUTH_TOKEN_KEY);
        this.store.dispatch(new TerminateAccountAction());
      }),
      map(() => void 0)
    );
  }

  public getUserData(): Observable<{
    user: User,
    preferences: Preferences,
    activePlan: ActivePlan,
  }> {
    return this.http.get<any>(API_URI_PLACEHOLDER + '/accounts/current').pipe(
      map((data) => {
        return {
          user: {
            id: data.id,
            name: null,
            email: data.email,
            registered: parseDateFromApi(data.createdAt),
          },
          preferences: {
            emailTipsFreeStuff: data.preferences['EMAIL_TIPS_FREE_STUFF'] === 'true',
            emailServiceUpdates: data.preferences['EMAIL_SERVICE_UPDATES'] === 'true',
            hideEmptyFeeds: data.preferences['GLOBAL_HIDE_EMPTY_FEEDS'] === 'true',
            columnCount: parseInt(data.preferences['GLOBAL_COLUMN_COUNT'], 10),
            hideEntriesAfterRead: data.preferences['HIDE_ENTRIES_AFTER_READ'] === 'RESPECT_FEED_SETTINGS'
              ? 'respect-feed-settings' : (data.preferences['HIDE_ENTRIES_AFTER_READ'] === 'TRUE') as true | false | 'respect-feed-settings'
          },
          activePlan: {
            type: data.activePlan.type,
            until: parseDateFromApi(data.activePlan.validTo)
          }
        };
      }),
      tap((data) => {
        if (data.activePlan.type !== 'EXPIRED') {
          // todo don't forget this after renewing subscription
          localStorage.removeItem(HIDE_TRIAL_NOTIFICATION_TOKEN_KEY);
        }
      })
    );
  }

  public getConfig(): Observable<ConfigsView> {
    return this.http.get<ConfigsView>(API_URI_PLACEHOLDER + '/configs').pipe(
      map((configs) => {
        configs.premiumPriceYearWithoutVat = new Big(configs.premiumPriceYearWithoutVat);

        return configs;
      })
    );
  }

  public updatePreference(
    preferences: Preferences
  ): Observable<void> {
    return this.store.pipe(
      select(AuthSelectors.getAuthState),
      map(value => value.preferences),
      take(1),
      tap(() => {
        this.store.dispatch(
          new UpdatePreferencesAction(preferences)
        );
      }),
      switchMap((oldPreferences) => {
        const requests = [];

        // todo watafak juraj

        if (oldPreferences.emailTipsFreeStuff !== preferences.emailTipsFreeStuff) {
          requests.push(this.http.put<void>(
            API_URI_PLACEHOLDER + '/accounts/current/preferences/EMAIL_TIPS_FREE_STUFF',
            String(preferences.emailTipsFreeStuff)
          ));
        }

        if (oldPreferences.emailServiceUpdates !== preferences.emailServiceUpdates) {
          requests.push(this.http.put<void>(
            API_URI_PLACEHOLDER + '/accounts/current/preferences/EMAIL_SERVICE_UPDATES',
            String(preferences.emailServiceUpdates)
          ));
        }

        if (oldPreferences.hideEmptyFeeds !== preferences.hideEmptyFeeds) {
          requests.push(this.http.put<void>(
            API_URI_PLACEHOLDER + '/accounts/current/preferences/GLOBAL_HIDE_EMPTY_FEEDS',
            String(preferences.hideEmptyFeeds)
          ));
        }

        if (oldPreferences.columnCount !== preferences.columnCount) {
          requests.push(this.http.put<void>(
            API_URI_PLACEHOLDER + '/accounts/current/preferences/GLOBAL_COLUMN_COUNT',
            String(preferences.columnCount)
          ));
        }

        if (oldPreferences.hideEntriesAfterRead !== preferences.hideEntriesAfterRead) {
          requests.push(this.http.put<void>(
            API_URI_PLACEHOLDER + '/accounts/current/preferences/HIDE_ENTRIES_AFTER_READ',
            String(preferences.hideEntriesAfterRead === 'respect-feed-settings' ?
              'respect_feed_settings' : preferences.hideEntriesAfterRead).toUpperCase()
          ));
        }

        return forkJoin(requests);
      }),
      map(() => void 0),
    );
  }

  public changeEmail(email: string): Observable<void> {
    return this.http.put<void>(API_URI_PLACEHOLDER + '/accounts/current/email', email).pipe(
      catchError((error) => {
        if (error.error.details === 'E-mail already used.') {
          return throwError('E-mail used');
        }
        return throwError('Unexpected error');
      }),
      tap(() => this.store.dispatch(new ChangeEmailAction(email))),
    );
  }

  public changePassword(oldPassword: string, newPassword: string): Observable<void> {
    return this.http.put<void>(API_URI_PLACEHOLDER + '/accounts/current/password', {
      oldPassword: oldPassword,
      newPassword: newPassword
    });
  }
}
