import {AuthActionUnion, Types} from './auth.actions';
import {defaultState, State} from './auth.state';
import produce from 'immer';

export function authReducer(state: State = defaultState, action: AuthActionUnion): State {
  'use-strict';

  return produce(state, draft => {
    switch (action.type) {
      case Types.INIT:
        draft.loggedIn = action.loggedIn;
        break;
      case Types.LOGGED_IN:
        draft.loggedIn = true;
        break;
      case Types.LOGGED_OUT:
        draft.loggedIn = false;
        draft.plan = null;
        draft.user = null;
        draft.preferences = null;
        break;
      case Types.LOADED:
        draft.plan = action.activePlan;
        draft.user = action.user;
        draft.preferences = action.preferences;
        break;
      case Types.CHANGE_EMAIL:
        draft.user.email = action.newEmail;
        break;
      case Types.UPDATE_PREFERENCES:
        draft.preferences = action.preferences;
        break;
    }
  });
}


