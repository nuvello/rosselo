import {createSelector} from '@ngrx/store';
import {AuthState} from './index';
import {RootState} from '../index';

export enum AuthStatus {
  LOADED,
  LOADING,
}

export const getAuthState = createSelector(
  (state: RootState.State): AuthState.State => state.auth,
  (authState) => authState
);

export const getAuthStatus = createSelector(
  getAuthState,
  (state): AuthStatus => {
    if (!state.loggedIn) {
      return AuthStatus.LOADED;
    }

    if (state.preferences === null || state.plan === null || state.user === null) {
      return AuthStatus.LOADING;
    }

    return AuthStatus.LOADED;
  }
);
