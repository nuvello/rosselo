export interface User {
  id: string;
  name: string;
  email: string;
  registered: Date;
}

export interface ActivePlan {
  type: 'EXPIRED' | 'TRIAL' | 'PREMIUM';
  until: Date;
}

export interface Preferences {
  columnCount: number;
  hideEmptyFeeds: boolean;
  hideEntriesAfterRead: true | false | 'respect-feed-settings';
  emailServiceUpdates: boolean;
  emailTipsFreeStuff: boolean;
}

export interface State {
  loggedIn: boolean;
  user: User | null;
  preferences: Preferences | null;
  plan: ActivePlan | null;
}

export const defaultState: State = {
  loggedIn: false,
  plan: null,
  preferences: null,
  user: null
};

