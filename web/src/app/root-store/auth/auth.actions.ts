import {Action} from '@ngrx/store';
import {ActivePlan, Preferences, User} from './auth.state';

export enum Types {
  INIT = '[Auth] init',
  LOGGED_IN = '[Auth] logged in',
  LOGGED_OUT = '[Auth] logged out',
  LOAD = '[Auth] load user\'s data',
  LOADED = '[Auth] loaded user\'s data',
  TERMINATE_ACCOUNT = '[News] Terminate account',
  CHANGE_EMAIL = '[Auth] Change  email',
  UPDATE_PREFERENCES = '[Auth] Update settings'
}

export class InitAction implements Action {
  public readonly type = Types.INIT;

  public constructor(
    public readonly loggedIn: boolean
  ) {
  }
}

export class LoggedInAction implements Action {
  public readonly type = Types.LOGGED_IN;
}

export class LoggedOutAction implements Action {
  public readonly type = Types.LOGGED_OUT;
}

export class LoadAction implements Action {
  public readonly type = Types.LOAD;
}

export class LoadedAction implements Action {
  public readonly type = Types.LOADED;


  public constructor(
    public readonly user: User,
    public readonly preferences: Preferences,
    public readonly activePlan: ActivePlan,
  ) {
  }
}

export class TerminateAccountAction implements Action {
  public readonly type = Types.TERMINATE_ACCOUNT;
}

export class ChangeEmailAction implements Action {
  public readonly type = Types.CHANGE_EMAIL;

  public constructor(
    public newEmail: string
  ) {
  }
}

export class UpdatePreferencesAction implements Action {
  public readonly type = Types.UPDATE_PREFERENCES;

  public constructor(
    public readonly preferences: Preferences
  ) {
  }
}

export type AuthActionUnion =
  InitAction |
  LoadAction |
  LoadedAction |
  LoggedInAction |
  LoggedOutAction |
  TerminateAccountAction |
  ChangeEmailAction |
  UpdatePreferencesAction;
