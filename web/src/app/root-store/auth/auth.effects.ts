import {Actions, createEffect, ofType, ROOT_EFFECTS_INIT} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {filter, map, switchMap} from 'rxjs/operators';
import {AuthActions} from './index';
import {AuthService} from '../../shared/services/auth.service';
import {InitAction} from './auth.actions';
import {merge} from 'rxjs';

@Injectable()
export class AuthEffects {
  
  public init = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    map(() => new AuthActions.InitAction(
      this.authService.isLoggedIn()
    ))
  ));

  
  public triggerLoadData = createEffect(() => merge(
    this.actions$.pipe(
      ofType(AuthActions.Types.INIT),
      filter((action: InitAction) => action.loggedIn)
    ),
    this.actions$.pipe(
      ofType(AuthActions.Types.LOGGED_IN)
    )
  ).pipe(
    map(() => new AuthActions.LoadAction())
  ));

  
  public loadData = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.Types.LOAD),
    switchMap(() => this.authService.getUserData()),
    map((data) => new AuthActions.LoadedAction(data.user, data.preferences, data.activePlan))
  ));

  
  public terminateAccount = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.Types.TERMINATE_ACCOUNT),
    map(() => new AuthActions.LoggedOutAction()),
  ));

  constructor(
    private actions$: Actions,
    private authService: AuthService,
  ) {
  }
}
