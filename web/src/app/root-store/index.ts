import * as RootState from './root.state';
import * as RootActions from './root.actions';

export {rootReducers, rootMetaReducers} from './root.reducer';
export {
  RootState,
  RootActions
};

