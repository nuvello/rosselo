import {Action} from '@ngrx/store';
import {ConfigsView} from '../dto/configs.view';

export enum Types {
  LOADED_CONFIGS = '[Root] Loaded configs',
  INCREASE_CONNECTION_PROBLEM_COUNT = '[Root] Increase connection problem count',
  DECREASE_CONNECTION_PROBLEM_COUNT = '[Root] Decrease connection problem count'
}

export class LoadedConfigsAction implements Action {
  public readonly type = Types.LOADED_CONFIGS;

  public constructor(
    public readonly configs: ConfigsView
  ) {
  }
}

export class IncreaseConnectionProblemCountAction implements Action {
  public readonly type = Types.INCREASE_CONNECTION_PROBLEM_COUNT;
}

export class DecreaseConnectionProblemCountAction implements Action {
  public readonly type = Types.DECREASE_CONNECTION_PROBLEM_COUNT;
}


export type RootActionUnion =
  LoadedConfigsAction |
  IncreaseConnectionProblemCountAction |
  DecreaseConnectionProblemCountAction;
