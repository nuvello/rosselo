import {AuthState} from './auth';
import {ConfigsView} from '../dto/configs.view';

export interface State {
  auth: AuthState.State;
  config: ConfigsView | null;
  connectionProblemsCount: number;
}
