import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType, ROOT_EFFECTS_INIT} from '@ngrx/effects';
import {map, switchMap} from 'rxjs/operators';
import {AuthService} from '../shared/services/auth.service';
import {LoadedConfigsAction} from './root.actions';

@Injectable()
export class RootEffects {
  
  init$ = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    switchMap(() => this.authService.getConfig()),
    map((configs) => new LoadedConfigsAction(configs))
  ));

  public constructor(
    private actions$: Actions,
    private authService: AuthService
  ) {
  }
}
