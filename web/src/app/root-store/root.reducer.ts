import {ActionReducer, ActionReducerMap, MetaReducer} from '@ngrx/store';
import {State} from './root.state';
import {authReducer} from './auth';
import {environment} from '../../environments/environment';
import {ConfigsView} from '../dto/configs.view';
import {DecreaseConnectionProblemCountAction, IncreaseConnectionProblemCountAction, LoadedConfigsAction} from './root.actions';

const defaultConfig: ConfigsView = null;

export function configReducer(state = defaultConfig, action) {
  if (action instanceof LoadedConfigsAction) {
    return action.configs;
  }

  return state;
}

export function connectionProblemsCountReducer(state = 0, action) {
  if (action instanceof IncreaseConnectionProblemCountAction) {
    return state + 1;
  } else if (action instanceof DecreaseConnectionProblemCountAction) {
    return state - 1;
  }

  return state;
}


export const rootReducers: ActionReducerMap<State> = {
  auth: authReducer,
  config: configReducer,
  connectionProblemsCount: connectionProblemsCountReducer
};

export const rootMetaReducers: MetaReducer<State>[] = [];

if (environment.hmr) {
  rootMetaReducers.push((reducer: ActionReducer<any>): ActionReducer<any> => {
    return function (state: any, action: any) {
      if (action.type === 'SET_ROOT_STATE') {
        return action.payload;
      }
      return reducer(state, action);
    };
  });
}
