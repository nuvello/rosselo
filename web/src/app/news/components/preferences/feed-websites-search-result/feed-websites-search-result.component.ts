import {ChangeDetectionStrategy, Component, HostBinding, Input, OnInit} from '@angular/core';
import {SourceSearchView} from '../../../../dto/source.search-view';
import {FeedService} from '../../../service/feed.service';
import {Category} from '../../../news-store/data/category';
import {delay, map, switchMap, take, takeUntil} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {RootState} from '../../../../root-store';
import {SourceService} from '../../../service/source.service';
import {EntryView} from '../../../../dto/entry.view';

@Component({
    selector: 'rs-feed-websites-search-result',
    templateUrl: './feed-websites-search-result.component.html',
    styleUrls: ['./feed-websites-search-result.component.scss']
})
export class FeedWebsitesSearchResultComponent implements OnInit {
    @Input()
    public source: SourceSearchView;

    @Input()
    public category: Category;

    @HostBinding('class')
    public get clazz() {
        let result = '';

        if (this.source.feedId !== null) {
            result += ' already-added';
        }

        if (this.meta.working) {
            result += ' working';
        }

        if (this.meta.error !== null) {
            result += ' error';
        }

        return result;
    }

    public meta = {
        working: false,
        error: <string>null,
        resetTimeoutId: <any>null
    };

    public previewStatus: 'hidden' | 'loading' | 'shown' = 'hidden';
    public items: EntryView[] | null = null;

    constructor(
        private feedService: FeedService,
        private sourceService: SourceService,
        private store: Store<RootState.State>
    ) {

    }

    ngOnInit(): void {
    }

    getHost(): string {
        return (new URL(this.source.link)).host.replace('www.', '');
    }

    calculateButtonState() {
        if (this.meta.working) {
            return 'working';
        }

        if (this.source.feedId !== null) {
            return 'done';
        }

        if (this.meta.error !== null) {
            return 'error';
        }

        return 'default';
    }

    add() {
        if (this.source.feedId === null && !this.meta.working) {
            this.meta.working = true;
            this.store.select((state) => state).pipe(
                take(1),
                map((state) => state.config),
                switchMap((config) => {
                    return this.feedService.createFeed({
                        name: this.source.title,
                        sourceId: this.source.id,
                        categoryId: this.category.id,
                        hideEntriesAfterRead: config.defaultFeedHideEntriesAfterReadPreference,
                        maxAmountOfItems: config.defaultFeedMaxAmountOfItemsPreference,
                        noOlderThan: config.defaultFeedNoOlderThanPreference
                    });
                })
            ).subscribe((createdFeed) => {
                this.source.feedId = createdFeed.id;
                this.source.categoryName = this.category.name;
                this.meta.working = false;
            });
        }
    }

    previewItems() {
        if (this.previewStatus === 'hidden') {
            if (this.items === null) {
                this.previewStatus = 'loading';
                this.sourceService.previewItems(this.source.id).pipe(
                    map((data) => data.slice(0, 5))
                ).subscribe((data) => {
                    this.previewStatus = 'shown';
                    this.items = data;
                });
            } else {
                this.previewStatus = 'shown';
            }
        } else if (this.previewStatus === 'shown') {
            this.previewStatus = 'hidden';
        }
    }
}
