import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedWebsitesComponent } from './feed-websites.component';

describe('FeedWebsitesComponent', () => {
  let component: FeedWebsitesComponent;
  let fixture: ComponentFixture<FeedWebsitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedWebsitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedWebsitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
