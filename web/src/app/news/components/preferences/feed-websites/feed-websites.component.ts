import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {SourceService} from '../../../service/source.service';
import {EMPTY, fromEvent, merge, of, Subject, timer} from 'rxjs';
import {catchError, debounce, distinctUntilChanged, map, startWith, switchMap, takeUntil, tap} from 'rxjs/operators';
import {SourceSearchView} from '../../../../dto/source.search-view';
import {isUrl} from '../../../../misc/utils';
import {Category} from '../../../news-store/data/category';
import {parseCreateSourceTypeFromRawString} from '../../../../misc/parse-create-source-type-from-raw-string';
import {CreateSourceType} from '../../../../dto/create-source-type';

@Component({
    selector: 'rs-feed-websites',
    templateUrl: './feed-websites.component.html',
    styleUrls: ['./feed-websites.component.scss']
})
export class FeedWebsitesComponent implements OnInit, OnDestroy {
    @Input()
    public category: Category;

    public term: string;
    public results: SourceSearchView[] = [];
    public mobileLayout = false;

    public queryTooShort = true;
    public filterAllowed = true;
    public searching = false;
    public filterOpened = false;
    public error = false;

    public creatingNewSource = false;
    public editFeedId: string | null = null;

    private onDestroy$ = new Subject();
    private termChanged$ = new Subject<string>();
    private page = 1;

    public constructor(
        private sourceService: SourceService
    ) {
    }

    public ngOnInit() {
        // todo filters here
        const query$ = this.termChanged$.pipe(
            takeUntil(this.onDestroy$),
            map((query) => {
                query = query.trim();

                return {
                    term: query,
                    isTooShort: query.length < 3,
                    isUrl: isUrl(query)
                };
            }),
            distinctUntilChanged()
        );

        const result$ = query$.pipe(
            debounce((query) => query.isTooShort ? EMPTY : timer(400)),
            switchMap((query) => {
                if (query.isTooShort) {
                    return of([]);
                } else {
                    return this.sourceService.search(query.term, 1, [], []).pipe(
                        switchMap((result) => {
                            if (result.length === 0 && query) {
                                try {
                                    const {type, parsedValue} = parseCreateSourceTypeFromRawString(
                                        query.term,
                                        'url'
                                    );

                                    if (type !== CreateSourceType.RSS_URL) {
                                        return of([]);
                                    }

                                    // todo don't like this side effect here
                                    this.creatingNewSource = true;

                                    return this.sourceService.create(type, parsedValue)
                                        .pipe(
                                            switchMap((value) => {
                                                return this.sourceService.search(value.sourceName, 1, [], []);
                                            }),
                                            catchError(() => {
                                                return of([]);
                                            })
                                        );
                                } catch (error) {
                                    return of([]);
                                }
                            }
                            return of(result);
                        }),
                    );
                }
            }),
            takeUntil(this.onDestroy$),
        );

        query$.subscribe((query) => {
            this.queryTooShort = query.isTooShort;
            this.searching = true;
            this.error = false;
            this.page = 1;
            this.results = [];
            this.creatingNewSource = false;

            if (query.isUrl) {
                this.filterAllowed = false;
                this.filterOpened = false;
            }
        });

        result$.subscribe((data) => {
            this.creatingNewSource = false;
            this.results = data;
            this.searching = false;
        }, () => {
            this.error = true;
        });

        fromEvent(window, 'resize').pipe(
            startWith(<Object>null),
            takeUntil(this.onDestroy$)
        ).subscribe(() => {
            this.mobileLayout = window.innerWidth <= 768;
        });
    }

    ngOnDestroy() {
        this.termChanged$.complete();
        this.onDestroy$.next(null);
        this.onDestroy$.complete();
    }

    search($event: any) {
        this.term = $event;
        this.termChanged$.next($event);
    }

    calculateButtonState() {
        if (this.searching) {
            return 'working';
        }

        if (!this.queryTooShort) {
            return 'done';
        }

        return 'default';
    }

    loadMore() {
        if (this.results.length === this.page * 10 && !this.searching) {
            this.page++;
            this.searching = true;

            this.sourceService.search(this.term.trim(), this.page, [], []).pipe(
                takeUntil(merge(this.termChanged$.pipe(distinctUntilChanged()), this.onDestroy$)),
            ).subscribe((data) => {
                this.results.push.apply(this.results, data);
            }, () => void 0, () => {
                this.searching = false;
            });
        }
    }
}
