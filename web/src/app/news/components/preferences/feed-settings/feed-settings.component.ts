import {
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnInit,
    Output,
    SimpleChanges,
    ViewChild
} from '@angular/core';
import {Observable, of, Subject, Subscription, throwError} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Category} from '../../../news-store/data/category';
import {select, Store} from '@ngrx/store';
import {NewsSelector} from '../../../news-store';
import {Feed} from '../../../news-store/data/feed';
import {FeedService} from '../../../service/feed.service';
import {RootState} from '../../../../root-store';
import {catchError, finalize, switchMap, take, tap} from 'rxjs/operators';
import {SourceService} from '../../../service/source.service';
import {AdvancedSelectOption} from '../../../../shared/components/advanced-select/advanced-select-option.class';
import {HttpErrorResponse} from '@angular/common/http';
import {Source} from '../../../news-store/data/source';
import {SourceType} from '../../../../dto/source-type';
import {parseCreateSourceTypeFromRawString} from '../../../../misc/parse-create-source-type-from-raw-string';
import {CreateSourceType} from '../../../../dto/create-source-type';
import {CreateSourceResult} from '../../../../dto/create-source.result';
import {isUrl} from '../../../../misc/utils';


enum MetaState {
    DEFAULT = 0,
    WORKING = 1,
    SUCCESS = 2,
    ERROR = 3
}

type ComponentContentType = 'url' | 'channel';

interface ChannelOption {
    type: 'youtube' | 'twitter';
    label: string;
}

@Component({
    selector: 'rs-feed-settings',
    templateUrl: './feed-settings.component.html',
    styleUrls: ['./feed-settings.component.scss']
})
export class FeedSettingsComponent implements OnInit, OnChanges, AfterViewInit {
    @Input()
    public mode: Feed | 'new-url' | 'new-channel';
    @Input()
    public category: Category;
    @Output()
    public saved: EventEmitter<Feed> = new EventEmitter();
    @Output()
    public deleted: EventEmitter<void> = new EventEmitter();
    @Output()
    public modeChange: EventEmitter<Feed | 'new-url' | 'new-channel'> = new EventEmitter<Feed | 'new-url' | 'new-channel'>();

    @ViewChild('nameInput', {read: ElementRef, static: true})
    public nameInput: ElementRef;
    public hideEntriesAfterReadGlobal$: Observable<true | false | 'respect-feed-settings'>;

    public categories$: Observable<Category[]>;
    public form: FormGroup;
    public meta = {
        working: false,
        state: MetaState.DEFAULT,
        error: <string>null,
        resetTimeoutId: <any>null
    };


    public channelTypeOptions: AdvancedSelectOption<ChannelOption>[];
    public enabledChannelType: AdvancedSelectOption<ChannelOption>;

    public sourceCreation: SourceCreation;
    public sourceDataFocused = false;

    public constructor(
        private store: Store<RootState.State>,
        private formBuilder: FormBuilder,
        private feedService: FeedService,
        private sourceService: SourceService
    ) {
    }

    public ngOnInit() {
        this.sourceCreation = new SourceCreation(this.sourceService);
        this.hideEntriesAfterReadGlobal$ = this.store.select((state) => state.auth.preferences.hideEntriesAfterRead);
        this.channelTypeOptions = [
            new AdvancedSelectOption(
                'Youtube user / channel / playlist',
                'youtube',
                {
                    type: 'youtube',
                    label: 'URL or channel name'
                },
            ),
            new AdvancedSelectOption(
                'Twitter user',
                'twitter',
                {
                    type: 'twitter',
                    label: 'URL or nickname'
                },
            )
        ];
        this.enabledChannelType = this.channelTypeOptions[0];
        this.categories$ = this.store.pipe(
            select(NewsSelector.getSortedCategories)
        );
        this.setupForm();
    }

    public ngAfterViewInit(): void {
        setTimeout(() => {
            this.nameInput.nativeElement.focus();
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.mode && !changes.mode.isFirstChange()) {
            // we have to ignore if the same feed is updated
            if (
                (!this.isEditMode() && typeof changes.mode.previousValue === 'object') || // if was edit mode and is add mode now
                (this.isEditMode() && typeof changes.mode.previousValue !== 'object') || // if from add to edit mode
                changes.mode.previousValue.id !== (<Feed>this.mode).id // if feed changed in edit mode
            ) {
                this.setupForm();
            }
        }

        if (changes.mode && !changes.mode.isFirstChange()) {
            // reset source data if value does not fit with component content type
            // 1. say you add a youtube url into RSS mode
            // 2. you'll be switched to channel mode
            // 3. you click back to RSS mode
            // 4. you'd be moved to channel mode again - to avoid this, we reset the source data field
            if (this.sourceCreation.isValid) {
                const parsed = parseCreateSourceTypeFromRawString(
                    this.form.get('sourceData').value,
                    this.getComponentContentType() === 'channel' ? this.enabledChannelType.data.type : 'url'
                );

                if (parsed.type === CreateSourceType.RSS_URL && this.getComponentContentType() === 'channel' ||
                    parsed.type !== CreateSourceType.RSS_URL && this.getComponentContentType() === 'url') {
                    this.form.get('sourceData').setValue(null);
                    this.form.get('sourceData').markAsUntouched();
                    this.form.updateValueAndValidity();
                }
            }
        }

        if (changes.category && !changes.category.isFirstChange()) {
            this.form.patchValue({
                categoryId: this.category.id
            });
        }
    }

    private setupForm(): void {
        this.store.select((state) => state).pipe(
            take(1)
        ).subscribe((state) => {
            const config = state.config;

            this.form = this.formBuilder.group({
                name: [''],
                sourceData: [''],
                maxAmountOfItems: [
                    config.defaultFeedMaxAmountOfItemsPreference, [Validators.min(1), Validators.max(config.maxNumberOfEntriesPerFeed)]
                ],
                noOlderThan: [
                    config.defaultFeedNoOlderThanPreference, [Validators.min(0), Validators.max(365)]
                ],
                hideEntriesAfterRead: [
                    config.defaultFeedHideEntriesAfterReadPreference
                ],
                categoryId:
                    this.category === null ? [] : [this.category.id]
            }, {
                validators: (control) => {
                    // a bit hacky, doesn't do any actual validation of the form
                    // it's equivalent of onblur
                    this.getOrCreateSource(control.value.sourceData).subscribe(() => void 0, () => void 0);

                    return null;
                },
                updateOn: 'blur'
            });


            if (!this.isEditMode()) {
                this.enabledChannelType = this.channelTypeOptions[0];
            } else {
                const feed = <Feed>this.mode;
                this.form.patchValue({
                    name: feed.name,
                    maxAmountOfItems: feed.maxAmountOfItems,
                    sourceData: this.getUnderstandableSourceData(feed.source),
                    noOlderThan: feed.noOlderThan,
                    hideEntriesAfterRead: feed.hideEntriesAfterRead,
                    categoryId: feed.categoryId
                });

                if (this.getComponentContentType() === 'channel') {
                    const type = feed.source.type === SourceType.TWITTER_ACCOUNT_NAME ? 'twitter' : 'youtube';

                    this.enabledChannelType = this.channelTypeOptions.filter(value => value.data.type === type)[0];
                }
            }

            this.form.updateValueAndValidity();
        });
    }

    public save() {
        if (!this.form.valid || !this.sourceCreation.isValid) {
            this.form.markAsTouched();
            this.form.markAsDirty();

            for (const controlsKey in this.form.controls) {
                if (this.form.controls.hasOwnProperty(controlsKey)) {
                    this.form.get(controlsKey).markAsTouched();
                    this.form.get(controlsKey).markAsDirty();
                }
            }
            return;
        }

        if (this.meta.state === MetaState.WORKING) {
            return;
        }

        this.meta.state = MetaState.WORKING;
        clearTimeout(this.meta.resetTimeoutId);

        this.getOrCreateSource(this.form.value.sourceData)
            .pipe(
                catchError(() => throwError('ERROR')),
                switchMap((result) => {
                    const name = this.form.value.name.trim().length === 0 ? result.sourceName : this.form.value.name;

                    if (name.length === 0) {
                        return throwError('FILL NAME');
                    }

                    const values = {
                        name: name,
                        sourceId: result.sourceId,
                        hideEntriesAfterRead: this.form.value.hideEntriesAfterRead,
                        maxAmountOfItems: this.form.value.maxAmountOfItems,
                        noOlderThan: this.form.value.noOlderThan,
                        categoryId: this.form.value.categoryId
                    };

                    return !this.isEditMode() ?
                        this.feedService.createFeed(values) :
                        this.feedService.updateFeed(this.getFeedBeingEdited().id, values);
                }),
                finalize(() => {
                    clearTimeout(this.meta.resetTimeoutId);
                    this.meta.resetTimeoutId = setTimeout(() => {
                        this.meta.state = MetaState.DEFAULT;
                    }, 3000);
                })
            )
            .subscribe((feed) => {
                feed = Object.assign({}, feed);
                feed.source = Object.assign({}, feed.source, {error: false});
                this.meta.state = MetaState.SUCCESS;
                this.mode = feed;
                this.modeChange.emit(this.mode);
                this.saved.emit(feed);
                this.setupForm();
            }, (error) => {
                this.meta.state = MetaState.ERROR;
                this.meta.error = error.error || error;
            });
    }

    public delete() {
        if (this.isEditMode()) {
            if (confirm('Do you really want to remove this feed?')) {
                this.feedService.deleteFeed(this.getFeedBeingEdited()).subscribe();
                this.deleted.emit(void 0);
            }
        }
    }


    private getOrCreateSource(sourceData: string): Observable<{
        sourceId: string,
        sourceName: string,
        sourceData: string,
        sourceType: SourceType
    }> {
        if (this.isEditMode()) {
            const source = this.getFeedBeingEdited().source;
            return of({
                sourceId: source.id,
                sourceName: source.title,
                sourceData: source.source,
                sourceType: source.type,
            });
        }

        return this.sourceCreation.create(
            sourceData,
            this.getComponentContentType() === 'channel' ? this.enabledChannelType.data.type : 'url'
        ).pipe(
            // todo don't like this tap
            tap((data) => {

                if (!this.sourceDataFocused) {
                    const nameControl = this.form.get('name');
                    const sourceDataControl = this.form.get('sourceData');

                    if ((nameControl.value == null || nameControl.value.trim().length === 0) && data.sourceName.length !== 0) {
                        nameControl.setValue(data.sourceName);
                        nameControl.updateValueAndValidity();
                    }

                    if (!this.isEditMode()) {
                        if (
                            data.sourceType === SourceType.YOUTUBE_CHANNEL_ID &&
                            sourceDataControl.value !== data.sourceName &&
                            !isUrl(sourceDataControl.value)
                        ) {
                            sourceDataControl.setValue(data.sourceName);
                            sourceDataControl.updateValueAndValidity();
                        } else if (data.sourceType === SourceType.RSS_URL && sourceDataControl.value !== data.sourceData) {
                            sourceDataControl.setValue(data.sourceData);
                            sourceDataControl.updateValueAndValidity();
                        }
                    }
                }

                if (!this.isEditMode()) {
                    switch (data.sourceType) {
                        case SourceType.RSS_URL:
                            this.mode = 'new-url';
                            this.modeChange.emit(this.mode);
                            break;
                        case SourceType.YOUTUBE_CHANNEL_ID:
                        case SourceType.YOUTUBE_PLAYLIST_ID:
                            this.mode = 'new-channel';
                            this.modeChange.emit(this.mode);
                            this.enabledChannelType = this.channelTypeOptions.find(value => value.data.type === 'youtube');
                            break;
                        case SourceType.TWITTER_ACCOUNT_NAME:
                            this.mode = 'new-channel';
                            this.modeChange.emit(this.mode);
                            this.enabledChannelType = this.channelTypeOptions.find(value => value.data.type === 'twitter');
                            break;
                        default:
                            throw new Error('Unknown type');
                    }
                }
            })
        );
    }

    public getUnderstandableSourceData(source: Source): String {
        switch (source.type) {
            case SourceType.RSS_URL:
                return source.source;
            case SourceType.TWITTER_ACCOUNT_NAME:
                return `Twitter account - ${source.title}`;
            case SourceType.YOUTUBE_CHANNEL_ID:
                return `Youtube channel - ${source.title}`;
            case SourceType.YOUTUBE_PLAYLIST_ID:
                return `Youtube playlist - ${source.title}`;
            default:
                throw new Error('Unknown type');
        }
    }

    public selectChannelType(type: AdvancedSelectOption<ChannelOption>) {
        if (this.getComponentContentType() === 'url') {
            throw new Error('Cannot select channel type in url content type');
        }

        this.enabledChannelType = type;
        this.form.get('sourceData').setValue(null);
        this.form.updateValueAndValidity();
    }

    public getFeedBeingEdited(): Feed {
        if (this.isEditMode()) {
            return <Feed>this.mode;
        } else {
            throw new Error('No feed being edited in add mode.');
        }
    }


    public isEditMode(): boolean {
        return typeof this.mode === 'object';
    }

    public getComponentContentType(): ComponentContentType {
        if (typeof this.mode === 'object') {
            if (this.mode.source.type === SourceType.RSS_URL) {
                return 'url';
            } else {
                return 'channel';
            }
        } else {
            switch (this.mode) {
                case 'new-channel':
                    return 'channel';
                case 'new-url':
                    return 'url';
                default:
                    throw new Error('Unknown mode');
            }
        }
    }
}

class SourceCreation {
    private lastInput: {
        sourceRaw: string,
        preferredType: 'url' | 'twitter' | 'youtube'
    } | null = null;
    private lastSource: CreateSourceResult | null = null;

    private ongoing: Subject<CreateSourceResult> | null = null;
    private ongoingXHRRequestSubscription: Subscription = null;

    public error: string | null = null;
    public isValid = true;

    public constructor(
        private sourceService: SourceService,
    ) {
    }

    public create(sourceRaw: string, preferredType: 'url' | 'youtube' | 'twitter'): Observable<CreateSourceResult> {
        // if the same validation as the last one
        if (
            this.lastInput !== null &&
            this.lastInput.sourceRaw === sourceRaw &&
            this.lastInput.preferredType === preferredType
        ) {
            if (this.ongoing !== null) {
                // if it's still ongoing, return it
                return this.ongoing;
            } else {
                if (!this.isValid) {
                    return throwError(this.error);
                } else {
                    return of(this.lastSource);
                }
            }
        }

        // reset value for a new creation
        this.lastInput = {preferredType: preferredType, sourceRaw: sourceRaw};
        this.lastSource = null;
        this.error = null;
        this.isValid = true;

        // if there's a validation request going on, cancel it
        if (this.ongoingXHRRequestSubscription !== null) {
            this.ongoingXHRRequestSubscription.unsubscribe();
            this.ongoingXHRRequestSubscription = null;
        }

        if (sourceRaw == null || sourceRaw.trim().length === 0) {
            switch (preferredType) {
                case 'youtube':
                    return this.doError('Enter URL or channel name.');
                case 'url':
                    return this.doError('Enter URL pointing to a valid RSS source.');
                case 'twitter':
                    return this.doError('Enter a Twitter username.');
                default:
                    throw new Error('Unknown type');
            }
        }


        let sourceType: CreateSourceType;
        let sourceData: string;
        try {
            const data = parseCreateSourceTypeFromRawString(sourceRaw, preferredType);
            sourceType = data.type;
            sourceData = data.parsedValue;
        } catch (e) {
            switch (preferredType) {
                case 'twitter':
                    return this.doError('This doesn\'t appear to be a valid Twitter account.');
                case 'url':
                    return this.doError('This doesn\'t appear to be a RSS url.');
                case 'youtube':
                    return this.doError('This doesn\'t appear to be a Youtube channel or playlist.');
            }
        }

        if (this.ongoing === null) {
            this.ongoing = new Subject<CreateSourceResult>();
        }

        this.ongoingXHRRequestSubscription = this.sourceService.create(sourceType, sourceData).subscribe(result => {
            this.doSuccess(result);
        }, (error: HttpErrorResponse) => {
            if (error.status === 400) {
                switch (preferredType) {
                    case 'twitter':
                        this.doError('No Twitter account found.');
                        break;
                    case 'url':
                        this.doError('No valid news source found.');
                        break;
                    case 'youtube':
                        this.doError('No Youtube channel or playlist found.');
                        break;
                    default:
                        throw new Error('Unknown type');
                }
            } else {
                this.doError('Server connection troubles, unable to verify.');
            }
        });

        return this.ongoing;
    }

    private doSuccess(data: { sourceId: string, sourceName: string, sourceData: string, sourceType: SourceType }): void {
        this.lastSource = data;
        this.error = null;
        this.isValid = true;
        if (this.ongoing !== null) {
            this.ongoing.next(data);
            this.ongoing.complete();
            this.ongoing = null;
        }
    }

    private doError(error: string): Observable<any> {
        this.error = error;
        this.isValid = false;
        if (this.ongoing !== null) {
            this.ongoing.error({error: this.error});
            this.ongoing.complete();
            this.ongoing = null;
        }
        return throwError(this.error);
    }
}
