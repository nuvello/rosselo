import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedSettingsComponent } from './feed-settings.component';

describe('FeedRssSettingsComponent', () => {
  let component: FeedSettingsComponent;
  let fixture: ComponentFixture<FeedSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
