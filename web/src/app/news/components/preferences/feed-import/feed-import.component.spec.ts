import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedImportComponent } from './feed-import.component';

describe('FeedImportComponent', () => {
  let component: FeedImportComponent;
  let fixture: ComponentFixture<FeedImportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedImportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedImportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
