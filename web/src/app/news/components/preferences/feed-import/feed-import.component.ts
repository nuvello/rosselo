import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FeedService} from '../../../service/feed.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RootState} from '../../../../root-store';
import {select, Store} from '@ngrx/store';
import {Category} from '../../../news-store/data/category';
import {Observable} from 'rxjs';
import {NewsSelector} from '../../../news-store';
import {finalize} from 'rxjs/operators';

enum MetaState {
  DEFAULT = 0,
  WORKING = 1,
  SUCCESS = 2,
  ERROR = 3
}

@Component({
  selector: 'rs-feed-import',
  templateUrl: './feed-import.component.html',
  styleUrls: ['./feed-import.component.scss']
})
export class FeedImportComponent implements OnInit {
  @ViewChild('file', {read: ElementRef, static: false})
  public fileInput: ElementRef;

  public form: FormGroup;
  public categories$: Observable<Category[]>;
  public customCategory = false;

  public meta = {
    working: false,
    state: MetaState.DEFAULT,
    error: <string>null,
    resetTimeoutId: <any>null
  };

  public constructor(
    private store: Store<RootState.State>,
    private formBuilder: FormBuilder,
    private feedService: FeedService,
  ) {
  }

  public ngOnInit() {
    this.categories$ = this.store.pipe(
      select(NewsSelector.getSortedCategories)
    );
    this.form = this.formBuilder.group({
      categoryId: [''],
    });
    this.setCustomCategory(false);
  }

  public setCustomCategory(value: boolean): void {
    this.customCategory = value;

    if (value) {
      this.form.get('categoryId').setValidators(Validators.required);
    } else {
      this.form.get('categoryId').clearValidators();
    }
    this.form.get('categoryId').updateValueAndValidity();
  }

  public importOpml() {
    if (this.meta.state === MetaState.WORKING) {
      return;
    }

    if (!this.form.valid) {
      this.form.markAsTouched();
      this.form.markAsDirty();

      for (const controlsKey in this.form.controls) {
        if (this.form.controls.hasOwnProperty(controlsKey)) {
          this.form.get(controlsKey).markAsTouched();
          this.form.get(controlsKey).markAsDirty();
        }
      }
      return;
    }

    const evt = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    });
    this.fileInput.nativeElement.dispatchEvent(evt);
  }

  public initiateImport(event) {
    this.meta.error = null;
    const files = event.target.files;

    if (files.length === 0) {
      return;
    }

    this.meta.state = MetaState.WORKING;
    clearTimeout(this.meta.resetTimeoutId);

    const file = files[0];

    this.feedService.importFeeds(file, this.customCategory ? this.form.value.categoryId : null)
      .pipe(
        finalize(() => {
          this.fileInput.nativeElement.value = '';
          clearTimeout(this.meta.resetTimeoutId);
          this.meta.resetTimeoutId = setTimeout(() => {
            this.meta.state = MetaState.DEFAULT;
          }, 3000);
        })
      ).subscribe(() => {
      this.meta.state = MetaState.SUCCESS;
    }, (error) => {
      this.meta.state = MetaState.ERROR;
      this.meta.error = error;
    });
  }
}
