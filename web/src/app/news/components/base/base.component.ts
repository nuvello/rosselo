import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../../root-store';
import {Router} from '@angular/router';
import {AuthSelectors} from '../../../root-store/auth';
import {filter, map, takeUntil} from 'rxjs/operators';
import {combineLatest, Subject} from 'rxjs';
import {NewsSelector} from '../../news-store';
import {animate, style, transition, trigger} from '@angular/animations';
import {AuthStatus} from '../../../root-store/auth/auth.selector';

@Component({
  selector: 'rs-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
  animations: [
    trigger('fade', [
      transition(':leave', [
        style({
          opacity: 1
        }),
        animate('0.2s', style({
          opacity: 0
        }))
      ])
    ])
  ]
})
export class BaseComponent implements OnInit, OnDestroy {
  public loading = true;
  private onDestroy$ = new Subject();

  constructor(
    private store: Store<RootState.State>,
    private router: Router
  ) {
  }

  public ngOnInit() {
    this.store.pipe(
      takeUntil(this.onDestroy$),
      select(AuthSelectors.getAuthState),
      map((state) => state.loggedIn),
      filter((val) => !val)
    ).subscribe(() => {
      this.router.navigateByUrl('/login');
    });

    const loadingUser$ = this.store.pipe(
      select(AuthSelectors.getAuthStatus),
      map((status): boolean => {
        return status === AuthStatus.LOADING;
      })
    );

    const loadingData$ = this.store.pipe(
      select(NewsSelector.getNewsState),
      map(value => !value.loaded)
    );

    combineLatest(loadingData$, loadingUser$).pipe(
      takeUntil(this.onDestroy$),
      map((values) => values[0] || values[1])
    ).subscribe((loading) => {
      this.loading = loading;
    });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

}
