import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import {fromEvent, merge, Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {RouterExtService} from '../../../misc/router.extension';
import {Router} from '@angular/router';
import {isInputFocused} from '../../../misc/utils';

@Component({
  selector: 'rs-preferences-layout',
  templateUrl: './preferences-layout.component.html',
  styleUrls: ['./preferences-layout.component.scss'],
  animations: [
    trigger('slide', [
      state('open', style({
        height: '*',
      })),
      state('closed', style({
        height: 0,
        display: 'none'
      })),
      transition('open <=> closed', animate('0.2s ease')),
    ])
  ]
})
export class PreferencesLayoutComponent implements OnInit, OnDestroy {
  public showMenu = false;
  public mobile = false;
  private onDestroy$ = new Subject();
  private previousUrl: string;

  public constructor(
    private routerExtService: RouterExtService,
    private router: Router
  ) {
  }

  public ngOnInit() {
    this.previousUrl = this.routerExtService.getPreviousUrl();

    if (window.innerWidth <= 768) {
      this.mobile = true;
    }

    fromEvent(window, 'keyup').pipe(
      filter((event: KeyboardEvent) => event.key === 'Escape')
    ).pipe(
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      if (isInputFocused()) {
        return;
      }

      if (this.showMenu) {
        this.showMenu = false;
      } else {
        this.exit();
      }
    });

    merge(
      fromEvent(window, 'click'),
      fromEvent(window, 'contextmenu'),
    ).pipe(
      takeUntil(this.onDestroy$)
    )
      .subscribe(() => {
        if (this.showMenu) {
          this.showMenu = false;
        }
      });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

  public exit(): void {
    this.router.navigateByUrl(this.previousUrl == null ? '/app' : this.previousUrl);
  }

  @HostListener('window:resize')
  public onWindowResize() {
    const mobile = window.innerWidth <= 768;
    if (mobile && !this.mobile) {
      this.showMenu = false;
    }
    this.mobile = mobile;
  }
}
