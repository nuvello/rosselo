import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreferencesLayoutComponent } from './preferences-layout.component';

describe('PreferencesLayoutComponent', () => {
  let component: PreferencesLayoutComponent;
  let fixture: ComponentFixture<PreferencesLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreferencesLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreferencesLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
