import {ChangeDetectionStrategy, Component, HostBinding, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Feed} from '../../news-store/data/feed';
import {Store} from '@ngrx/store';
import {RootState} from '../../../root-store';
import {Entry} from '../../news-store/data/entry';
import {animate, style, transition, trigger} from '@angular/animations';
import {EntryService} from '../../service/entry.service';
import {FeedService} from '../../service/feed.service';

@Component({
  selector: 'rs-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('fadeOut', [
      transition(':leave', [
        animate(
          '0.2s',
          style({
            height: 0,
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class FeedComponent implements OnChanges {
  @Input()
  public feed: Feed;

  @HostBinding('class.simple-mode')
  @Input()
  public simpleMode = false;

  public faviconUrl: string;

  constructor(
    private store: Store<RootState.State>,
    private entryService: EntryService,
    private feedService: FeedService
  ) {
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.feed) {
      if (changes.feed.currentValue != null && (changes.feed.previousValue == null ||
        (<Feed>changes.feed.currentValue).source.link !== (<Feed>changes.feed.previousValue).source.link)) {
        const domain = (new URL(this.feed.source.link)).host.replace('www.', '');

        switch (domain) {
          case 'feeds.bbci.co.uk':
          case 'bbc.co.uk':
            this.faviconUrl = `/assets/images/feed-icons/bbc.svg`;
            break;
          case 'twitter.com':
          case 'twitrss.me':
            this.faviconUrl = `/assets/images/feed-icons/twitter.svg`;
            break;
          case 'youtube.com':
            this.faviconUrl = `/assets/images/feed-icons/youtube.svg`;
            break;
          case 'reddit.com':
            this.faviconUrl = `/assets/images/feed-icons/reddit.svg`;
            break;
          default:
            if (this.feed.source.smallIconUrl != null) {
              this.faviconUrl = this.feed.source.smallIconUrl;
            } else if (this.feed.source.lastExtraScrapAt == null) {
              this.faviconUrl = `https://www.google.com/s2/favicons?domain=${encodeURIComponent(domain)}&alt=feed`;
            } else {
              this.faviconUrl = null;
            }
            break;
        }
      }
    }
  }

  public onItemClick(feed: Feed, item: Entry, event: MouseEvent, button: 'left' | 'middle' | 'right') {
    event.preventDefault();
    switch (button) {
      case 'left':
      case 'middle':
        if (item.readAt === null && item.pinnedAt === null) {
          this.entryService.markAsRead(feed, item, true).subscribe();
        }
        // NEED TO BE CALLED AFTER MARKING READ, SO THAT ITEM REMOVAL AFTER
        // RETURNING BACK WORKS
        window.open(item.link, '_blank');
        break;
      case 'right':
        this.entryService.markAsRead(feed, item, item.readAt === null).subscribe();
        break;
    }
  }

  public getId(index, value) {
    return value.id;
  }

  public pinItem(feed: Feed, item: Entry, event: MouseEvent) {
    event.preventDefault();
    this.entryService.markAsPinned(feed, item, item.pinnedAt === null).subscribe();
  }

  public onFeedReadClick(feed: Feed, event: MouseEvent) {
    event.preventDefault();
    this.feedService.markAsRead(feed).subscribe();
  }
}
