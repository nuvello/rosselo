import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BaseComponent} from './components/base/base.component';
import {NewsPageComponent} from './pages/news-page/news-page.component';
import {PreferencesLayoutComponent} from './components/preferences-layout/preferences-layout.component';
import {CategoriesPageComponent} from './pages/preferences/categories-page/categories-page.component';
import {CategoryDetailPageComponent} from './pages/preferences/category-detail-page/category-detail-page.component';
import {FeedDetailPageComponent} from './pages/preferences/feed-detail-page/feed-detail-page.component';
import {SettingsPageComponent} from './pages/preferences/settings-page/settings-page.component';
import {AccountPageComponent} from './pages/preferences/account-page/account-page.component';
import {HotkeysPageComponent} from './pages/preferences/hotkeys-page/hotkeys-page.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'settings',
        component: PreferencesLayoutComponent,
        children: [
          {
            path: '',
            component: SettingsPageComponent
          },
          {
            path: 'account',
            component: AccountPageComponent
          },
          {
            path: 'hotkeys',
            component: HotkeysPageComponent
          },
          {
            path: 'groups',
            component: CategoriesPageComponent
          },
          {
            path: 'groups/:categoryId',
            component: CategoryDetailPageComponent
          },
          {
            path: 'groups/:categoryId/feeds/new/:type',
            component: FeedDetailPageComponent
          },
          {
            path: 'groups/:categoryId/feeds/:feedId',
            component: FeedDetailPageComponent
          },
        ]
      },
      {
        path: '',
        component: NewsPageComponent,
        children: [
          {
            path: ':id',
          },
          {
            path: '',
          },
        ]
      }
    ],
    component: BaseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NewsRoutingModule {
}
