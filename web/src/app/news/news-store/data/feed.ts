import {Entry} from './entry';
import {Source} from './source';

export interface Feed {
  id: string;
  column: number;
  source: Source;
  position: number;
  name: string;
  createdAt: Date;
  hideEntriesAfterRead: boolean;
  maxAmountOfItems: number | null;
  noOlderThan: number | null;
  categoryId: string;
  items: Entry[] | null;
  itemsLastRefresh: Date | null;
}
