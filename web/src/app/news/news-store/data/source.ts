import {SourceType} from '../../../dto/source-type';

export interface Source {
  id: string;
  type: SourceType;
  source: string;
  title: string;
  link: string;
  description: string | null;
  subscribers: number;
  error: boolean;
  lastSuccessfulFetchAt: Date;
  lastExtraScrapAt: Date;
  smallIconUrl: string | null;
  bigIconUrl: string | null;
}
