export interface Entry {
  id: string;
  createdAt: Date;
  title: string;
  link: string;
  pinnedAt: Date | null;
  unpinnedAt: Date | null;
  readAt: Date | null;
}

