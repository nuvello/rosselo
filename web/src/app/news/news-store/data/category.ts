export enum CategoryStatus {
  NOT_LOADED = 'not-loaded',
  LOADING = 'loading',
  REFRESHING = 'refreshing',
  LOADED = 'loaded'
}

export interface Category {
  id: string;
  name: string;
  slug: string;
  position: number;

  status: CategoryStatus;
  lastRefresh: Date | null;
}

