import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../root-store';
import {AuthActions, AuthSelectors} from '../../root-store/auth';
import {concatMap, delay, filter, map, mergeMap, switchMap, take, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {NewsActions, NewsSelector, NewsState} from './index';
import {forkJoin, from, merge, MonoTypeOperatorFunction, NEVER, Observable, of, race, timer} from 'rxjs';
import {LoadCategoryItemsAction, LoadedCategoryItemsAction, RefreshCategoryItemsAction, RefreshFeedAction} from './news.actions';
import {Category, CategoryStatus} from './data/category';
import {Feed} from './data/feed';
import {CategoryService} from '../service/category.service';
import {FeedService} from '../service/feed.service';
import {isMobile} from '../../misc/other';

function takeUntilUnload<T>(actions$: Actions): MonoTypeOperatorFunction<T> {
  return (source: Observable<T>) => {
    return source.pipe(
      takeUntil(actions$.pipe(
        ofType(NewsActions.Types.UNLOAD)
        )
      )
    );
  };
}

function shouldBeLoadedOrRefreshed(category: Category) {
  return category.status === CategoryStatus.NOT_LOADED ||
    (category.status === CategoryStatus.LOADED && new Date().getTime() - category.lastRefresh.getTime() > 1000 * 60);
}

@Injectable()
export class NewsEffects {


  public triggerLoad = createEffect(() => merge(
    // any time he is logged in
    this.actions$.pipe(
      ofType(AuthActions.Types.LOGGED_IN),
    ),
    // when the news module gets loaded, we need to emit if the user is already logged in
    this.store$.pipe(
      delay(0), // so that all other effects in this class are ready to respond
      take(1),
      select(AuthSelectors.getAuthState),
      filter(val => val.loggedIn)
    )
  ).pipe(
    switchMap(() => this.store$.pipe(
      take(1)
    )),
    select(NewsSelector.getNewsState),
    filter((newState) => !newState.loading && !newState.loaded),
    map(() => new NewsActions.LoadAction())
  ));


  public triggerUnload = createEffect(() => this.actions$.pipe(
    ofType(AuthActions.Types.LOGGED_OUT),
    map(() => new NewsActions.UnloadAction())
  ));


  public loadData = createEffect(() => this.actions$.pipe(
    ofType<NewsActions.LoadAction | NewsActions.RefreshAction>(
      NewsActions.Types.LOAD,
      NewsActions.Types.REFRESH,
    ),
    switchMap((action) => {
      return forkJoin(
        this.categoryService.findAll(),
        this.feedService.findAll()
      ).pipe(
        takeUntilUnload(this.actions$),
        map((data) => {
          if (action instanceof NewsActions.LoadAction) {
            return new NewsActions.LoadedAction(
              data[0],
              data[1]
            );
          } else {
            return new NewsActions.RefreshedAction(
              data[0],
              data[1]
            );
          }
        }),
      );
    }),
  ));


  public loadFeeds = createEffect(() => this.actions$.pipe(
    ofType(NewsActions.Types.LOADED),
    switchMap(() => timer(10000, 1000 * 60 * 3).pipe(
      takeUntilUnload(this.actions$)
    )),
    switchMap(() => this.store$.pipe(
      select(NewsSelector.getCategories),
      take(1)
    )),
    switchMap((categories: Category[]) => {
      if (categories === null) {
        return NEVER;
      }

      const filteredCategories = categories.filter(shouldBeLoadedOrRefreshed);

      return from(filteredCategories).pipe(
        takeUntilUnload(this.actions$)
      );
    }),
    concatMap((category: Category) => of(category).pipe(
      delay(1000),
      takeUntilUnload(this.actions$)
    )),
    // ONE MORE CHECK, AS IT COULD HAVE BEEN LOADED OR DELETED MEANWHILE
    switchMap((cat) => this.store$.pipe(
      select(NewsSelector.getCategoryById, {id: cat.id}),
      take(1)
    )),
    filter((cat) => cat != null),
    filter(shouldBeLoadedOrRefreshed),
    map((category) => {
      if (category.status === CategoryStatus.NOT_LOADED) {
        return new LoadCategoryItemsAction(category);
      } else {
        return new RefreshCategoryItemsAction(category);
      }
    }),
  ));


  public refreshErrorFeeds = createEffect(() => this.actions$.pipe(
    ofType(NewsActions.Types.LOADED),
    switchMap(() => timer(30000, 1000 * 60 * 5).pipe(
      takeUntilUnload(this.actions$)
    )),
    switchMap(() => this.store$.pipe(
      select(NewsSelector.getNewsState),
      take(1)
    )),
    switchMap((state: NewsState.State) => {
      const feeds = Object.values(state.feeds).filter(
        (feed) => feed.source.error,
      );
      return from(feeds).pipe(
        takeUntilUnload(this.actions$)
      );
    }),
    concatMap((feed: Feed) => of(feed).pipe(
      delay(200),
      takeUntilUnload(this.actions$)
    )),
    // ONE MORE CHECK, AS IT COULD HAVE BEEN LOADED OR DELETED MEANWHILE
    switchMap((feed) => this.store$.pipe(
      select(NewsSelector.getFeedById, {id: feed.id}),
      take(1)
    )),
    filter((feed) => feed != null),
    map((feed) => {
      return new RefreshFeedAction(feed);
    }),
  ));


  public refreshFeed = createEffect(() => this.actions$.pipe(
    ofType<NewsActions.RefreshFeedAction>(NewsActions.Types.REFRESH_FEED_ACTION),
    tap((action) => {
      this.feedService.refreshFeed(action.feed).subscribe();
    })
  ), {dispatch: false});


  public loadCategoryItems = createEffect(() => this.actions$.pipe(
    ofType<NewsActions.LoadCategoryItemsAction>(NewsActions.Types.LOAD_CATEGORY_ITEMS_ACTION),
    mergeMap((action) => {
      const date = new Date();

      return this.categoryService.getEntriesForCategory(action.category).pipe(
        map((data) => {
          return new LoadedCategoryItemsAction(
            action.category,
            data,
            date
          );
        }),
        takeUntilUnload(this.actions$)
      );
    }),
  ));


  public refreshCategoryItems = createEffect(() => this.actions$.pipe(
    ofType<NewsActions.RefreshCategoryItemsAction>(NewsActions.Types.REFRESH_CATEGORY_ITEMS_ACTION),
    tap((action) => {
      this.categoryService.refreshCategory(action.category).subscribe();
    }),
  ), {dispatch: false});


  public removeAfterRead = createEffect(() => this.actions$.pipe(
    ofType<NewsActions.ItemSetReadAction | NewsActions.ItemSetPinnedAction>(
      NewsActions.Types.ITEM_SET_READ, NewsActions.Types.ITEM_SET_PINNED
    ),
    // filter items that will be made read and that belongs to a feed that should
    // have read items hidden
    withLatestFrom(this.store$.pipe(
      select(AuthSelectors.getAuthState),
      map((state) => state.preferences),
    ), (action, config) => {
      if (config.hideEntriesAfterRead === 'respect-feed-settings') {
        if (!action.feed.hideEntriesAfterRead) {
          return null;
        }
      } else if (config.hideEntriesAfterRead === false) {
        return null;
      }

      if (action instanceof NewsActions.ItemSetReadAction) {
        return action.read ? action : null;
      }

      return !action.pinned && action.item.readAt !== null ? action : null;
    }),
    filter((item) => item !== null),
    // map every such action to a new observable emitting an action or null
    // if a cancelling action happens
    // process all of them, do not cancel previous if new emits
    mergeMap((action) => {
      const remove$ = Observable.create((observer) => {
        let timeout;

        const start = (target = 2000) => {
          if (isMobile()) {
            target += 2000;
          }

          timeout = setTimeout(() => {
            clear();
            observer.next(new NewsActions.ItemRemoveAction(action.feed, action.item));
            observer.complete();
          }, target);
        };

        const focusListener = () => {
          start(4000);
        };
        const blurListener = () => {
          clearTimeout(timeout);
        };
        const clear = () => {
          window.removeEventListener('focus', focusListener);
          window.removeEventListener('blur', blurListener);
          clearTimeout(timeout);
        };
        window.addEventListener('focus', focusListener);
        window.addEventListener('blur', blurListener);

        start();

        return () => {
          clear();
        };
      });
      // what happens first? 2000s delay or cancelling action?
      return race(
        merge(
          // set read | set pinned of the item
          this.actions$.pipe(
            ofType<NewsActions.ItemSetReadAction | NewsActions.ItemSetPinnedAction>(
              NewsActions.Types.ITEM_SET_READ, NewsActions.Types.ITEM_SET_PINNED
            ),
            filter((action2) => action.item.id === action2.item.id)
          ),
          // set read of the whole feed
          this.actions$.pipe(
            ofType<NewsActions.FeedSetReadAction>(
              NewsActions.Types.FEED_SET_READ
            ),
            filter((action2) => action.feed.id === action2.feed.id)
          ),
          // set read of the whole category
          this.actions$.pipe(
            ofType<NewsActions.CategorySetReadAction>(
              NewsActions.Types.CATEGORY_SET_READ
            ),
            filter((action2) => action.feed.categoryId === action2.category.id)
          ),
        ).pipe(
          map(() => null) // filtered at the end
        ),
        remove$
      ).pipe(
        // we want only the first emit of the winning observable
        take(1),
        takeUntilUnload(this.actions$),
      );
    }),
    // if cancelling action occurred, action would be null, so we ignore it
    filter((action) => action !== null)
  ));

  constructor(
    private actions$: Actions,
    private store$: Store<RootState.State>,
    private categoryService: CategoryService,
    private feedService: FeedService,
  ) {
  }
}
