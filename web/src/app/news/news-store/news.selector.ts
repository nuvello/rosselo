import {createFeatureSelector, createSelector, MemoizedSelector} from '@ngrx/store';
import {NewsState} from './index';
import {Category} from './data/category';
import {Feed} from './data/feed';

export const getNewsState = createFeatureSelector<NewsState.State>('news');
export const getCategories = createSelector(
  getNewsState,
  (state: NewsState.State): Category[] | null => {
    if (state.categories === null) {
      return null;
    }

    return Object.values(state.categories);
  }
);
export const getSortedCategories = createSelector(
  getCategories,
  (categories: Category[] | null): Category[] | null => {
    if (categories === null) {
      return categories;
    }

    categories.sort((a, b) => {
      return a.position - b.position;
    });
    return categories;
  }
);

export const getFirstCategory = createSelector(
  getSortedCategories,
  (categories: Category[]) => {
    if (categories === null || categories.length === 0) {
      return null;
    }

    return categories[0];
  }
);

export const getFeedById = createSelector(
  getNewsState,
  (state, params): Feed | null => {
    if (state.feeds === null) {
      return null;
    }

    return state.feeds[params.id] || null;
  }
);

export const getCategoryById = createSelector(
  getNewsState,
  (state, params): Category | null => {
    if (state.categories === null) {
      return null;
    }

    return state.categories[params.id] || null;
  }
);

export const getCategoryBySlug = createSelector(
  getNewsState,
  (state, params): Category | null => {
    if (state.categories === null) {
      return null;
    }

    return <any>Object.values(state.categories).find((cat: Category) => cat.slug === params.slug) || null;
  }
);

export const getFeedsForCategory = createSelector(
  getNewsState,
  (state, params): Feed[] => {
    if (state.feeds === null) {
      return null;
    }

    return <any>Object.values(state.feeds).filter((feed: Feed) => feed.categoryId === params.category.id);
  }
);

export const getSortedFeedsForCategory = createSelector(
  getFeedsForCategory,
  (state, params): Feed[] => {
    if (state === null) {
      return null;
    }

    state.sort((a, b) => {
      return a.createdAt.getTime() - b.createdAt.getTime();
    });

    return state;
  }
);
