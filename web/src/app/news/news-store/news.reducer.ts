import {defaultState, State} from './news.state';
import {NewsActionUnion, Types} from './news.actions';
import {CategoryStatus} from './data/category';
import produce from 'immer';
import {Feed} from './data/feed';
import {Entry} from './data/entry';
import {AuthActions} from '../../root-store/auth';

const itemComparator = (a: Entry, b: Entry) => {
  if (a.pinnedAt !== null && b.pinnedAt !== null) {
    // if both are pinned, ASC
    return a.pinnedAt.getTime() - b.pinnedAt.getTime();
  } else if (a.pinnedAt !== b.pinnedAt) {
    // if one of them are pinned
    return a.pinnedAt !== null ? -1 : 1;
  }

  // if none of them is pinned, DESC
  return (b.unpinnedAt === null ? b.createdAt : b.unpinnedAt).getTime() -
    (a.unpinnedAt === null ? a.createdAt : a.unpinnedAt).getTime();
};

export function newsReducer(state: State = defaultState, action: NewsActionUnion): State {
  'use-strict';
  return produce(state, draft => {
    switch (action.type) {
      // todo: find a better way to achieve this without this ugly hack
      //       probably just use effect to fire another event
      case <any>AuthActions.Types.UPDATE_PREFERENCES:
        for (const id in draft.categories) {
          if (draft.categories.hasOwnProperty(id)) {
            draft.categories[id].lastRefresh = null;
            draft.categories[id].status = CategoryStatus.NOT_LOADED;
          }
        }
        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            draft.feeds[id].items = null;
            draft.feeds[id].itemsLastRefresh = null;
          }
        }
        break;
      case Types.LOAD:
        draft.loading = true;
        break;
      case Types.REFRESHED:
        draft.categories = action.categories.reduce((map, cat) => {
          map[cat.id] = cat;
          return map;
        }, {});
        draft.feeds = action.feeds.reduce((map, feed) => {
          map[feed.id] = feed;
          return map;
        }, {});
        break;
      case Types.LOADED:
        draft.loaded = true;
        draft.loading = false;
        draft.categories = action.categories.reduce((map, cat) => {
          map[cat.id] = cat;
          return map;
        }, {});
        draft.feeds = action.feeds.reduce((map, feed) => {
          map[feed.id] = feed;
          return map;
        }, {});
        break;
      case Types.UNLOAD:
        draft.loaded = false;
        draft.loading = false;
        draft.feeds = null;
        draft.categories = null;
        break;
      case Types.LOAD_CATEGORY_ITEMS_ACTION: {
        const category = draft.categories[action.category.id];
        category.status = CategoryStatus.LOADING;
        break;
      }
      case Types.REFRESH_CATEGORY_ITEMS_ACTION: {
        const category = draft.categories[action.category.id];
        category.status = CategoryStatus.REFRESHING;
        break;
      }
      case Types.LOADED_CATEGORY_ITEMS_ACTION: {
        for (const id in action.items) {
          if (action.items.hasOwnProperty(id)) {
            draft.feeds[id].items = action.items[id];
            draft.feeds[id].itemsLastRefresh = action.date;
          }
        }
        const category = draft.categories[action.category.id];
        category.status = CategoryStatus.LOADED;
        category.lastRefresh = action.date;
        break;
      }
      case Types.REFRESHED_CATEGORY_ITEMS_ACTION: {
        for (const id in action.newItems) {
          if (action.newItems.hasOwnProperty(id)) {
            const feed = <Feed>draft.feeds[id];
            feed.itemsLastRefresh = action.date;

            if (action.newItems[id].length > 0) {
              const set = new Set<string>();
              feed.items.forEach((item) => set.add(item.id));
              const newItems = action.newItems[id].filter((item) => !set.has(item.id));

              if (newItems.length > 0) {
                feed.items = feed.items.concat(newItems);
                feed.items.sort(itemComparator);

                if (feed.maxAmountOfItems !== null && feed.items.length > feed.maxAmountOfItems) {
                  const toRemove = feed.items.length - feed.maxAmountOfItems;

                  feed.items.splice(feed.items.length - toRemove, feed.items.length - 1);
                }
              }
            }
          }
        }
        const category = draft.categories[action.category.id];
        category.status = CategoryStatus.LOADED;
        category.lastRefresh = action.date;
        break;
      }
      case Types.REFRESHED_FEED_ACTION: {
        draft.feeds[action.feed.id] = Object.assign({}, action.feed);
        draft.feeds[action.feed.id].items = action.items;
        draft.feeds[action.feed.id].itemsLastRefresh = action.date;
        break;
      }
      case Types.ITEM_SET_READ: {
        const feed = draft.feeds[action.feed.id];
        const item = feed.items.find((_item) => _item.id === action.item.id);

        if (action.read) {
          item.readAt = action.date;
          if (item.pinnedAt) {
            item.pinnedAt = null;
            item.unpinnedAt = action.date;
          }
        } else {
          item.readAt = null;
        }
        break;
      }
      case Types.ITEM_REMOVE: {
        const feed = draft.feeds[action.feed.id];
        const index = feed.items.findIndex((_item) => _item.id === action.item.id);
        feed.items.splice(index, 1);
        break;
      }
      case Types.ITEM_SET_PINNED: {
        const feed = draft.feeds[action.feed.id];
        const index = feed.items.findIndex((_item) => _item.id === action.item.id);
        const item = feed.items[index];
        item.pinnedAt = action.pinned ? action.date : null;
        item.unpinnedAt = action.pinned ? null : action.date;
        item.readAt = null;
        feed.items.sort(itemComparator);
        break;
      }
      case Types.FEED_SET_READ: {
        const feed = <Feed>draft.feeds[action.feed.id];
        for (const item of feed.items) {
          if (item.pinnedAt === null) {
            item.readAt = action.date;
          }
        }

        if (action.globalHideEntiresAfterReadSetting === true ||
          (action.globalHideEntiresAfterReadSetting === 'respect-feed-settings' && feed.hideEntriesAfterRead)) {
          feed.items = feed.items.filter(
            (item) => item.pinnedAt !== null || item.readAt === null
          );
        }
        break;
      }
      case Types.ALL_CATEGORIES_SET_READ: {
        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            const feed = draft.feeds[id];
            if (feed.items !== null) {
              for (const item of (<Feed>draft.feeds[id]).items) {
                if (item.pinnedAt === null) {
                  item.readAt = action.date;
                }
              }
              if (action.globalHideEntiresAfterReadSetting === true ||
                (action.globalHideEntiresAfterReadSetting === 'respect-feed-settings' && feed.hideEntriesAfterRead)) {
                feed.items = feed.items.filter(
                  (item) => item.pinnedAt !== null || item.readAt === null
                );
              }
            }
          }
        }
        break;
      }
      case Types.CATEGORY_SET_READ: {
        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            const feed = draft.feeds[id];
            if (draft.feeds[id].categoryId === action.category.id &&
              feed.items !== null) {
              for (const item of (<Feed>draft.feeds[id]).items) {
                if (item.pinnedAt === null) {
                  item.readAt = action.date;
                }
              }
              if (action.globalHideEntiresAfterReadSetting === true ||
                (action.globalHideEntiresAfterReadSetting === 'respect-feed-settings' && feed.hideEntriesAfterRead)) {
                feed.items = feed.items.filter(
                  (item) => item.pinnedAt !== null || item.readAt === null
                );
              }
            }
          }
        }
        break;
      }
      case Types.MOVE_CATEGORY: {
        if (action.toPosition === action.category.position) {
          return;
        }

        for (const id in draft.categories) {
          if (draft.categories.hasOwnProperty(id)) {
            const category = draft.categories[id];

            if (action.category.position > action.toPosition) {
              // moving up

              if (category.position >= action.toPosition && category.position < action.category.position) {
                category.position++;
              }

            } else {
              // moving down

              if (category.position > action.category.position && category.position <= action.toPosition) {
                category.position--;
              }
            }
          }
        }

        draft.categories[action.category.id].position = action.toPosition;
        break;
      }
      case Types.MOVE_FEED: {
        if (action.toColumn === action.feed.column && action.toPosition === action.feed.position) {
          return;
        }

        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            const feed = draft.feeds[id];

            if (feed.categoryId !== action.feed.categoryId) {
              continue;
            }

            if (feed.column !== action.toColumn && feed.column !== action.feed.column) {
              continue;
            }

            if (action.feed.column === action.toColumn) {
              if (action.feed.position > action.toPosition) {
                // moving up

                if (feed.position >= action.toPosition && feed.position < action.feed.position) {
                  feed.position++;
                }

              } else {
                // moving down

                if (feed.position > action.feed.position && feed.position <= action.toPosition) {
                  feed.position--;
                }
              }
            } else {
              if (feed.column === action.toColumn) {
                // new column

                if (feed.position >= action.toPosition) {
                  feed.position++;
                }
              } else if (feed.column === action.feed.column) {
                // old column

                if (feed.position >= action.feed.position) {
                  feed.position--;
                }
              }
            }
          }
        }

        draft.feeds[action.feed.id].column = action.toColumn;
        draft.feeds[action.feed.id].position = action.toPosition;
        break;
      }
      case Types.DELETE_CATEGORY: {
        delete draft.categories[action.category.id];

        // todo: better API for ordering
        for (const id in draft.categories) {
          if (draft.categories.hasOwnProperty(id)) {
            const category = draft.categories[id];

            if (category.position >= action.category.position) {
              category.position--;
            }
          }
        }

        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            if (draft.feeds[id].categoryId === action.category.id) {
              delete draft.feeds[id];
            }
          }
        }
        break;
      }
      case Types.DELETE_FEED: {
        delete draft.feeds[action.feed.id];
        for (const id in draft.feeds) {
          if (draft.feeds.hasOwnProperty(id)) {
            const feed = draft.feeds[id];

            if (feed.categoryId !== action.feed.categoryId) {
              continue;
            }

            if (feed.column !== action.feed.column) {
              continue;
            }

            if (feed.position >= action.feed.position) {
              feed.position--;
            }
          }
        }
        break;
      }
      case Types.CREATE_CATEGORY: {
        const category = Object.assign({}, action.category);
        category.position = Object.values(draft.categories).length;
        draft.categories[action.category.id] = category;
        category.lastRefresh = new Date();
        category.status = CategoryStatus.LOADED;
        break;
      }
      case Types.UPDATE_CATEGORY: {
        draft.categories[action.category.id] = action.category;
        break;
      }
      case Types.CREATE_FEED: {
        draft.feeds[action.feed.id] = Object.assign({}, action.feed);
        break;
      }
      case Types.UPDATE_FEED: {
        const originalFeed = draft.feeds[action.feed.id];

        if (action.feed.categoryId !== originalFeed.categoryId) {
          for (const id in draft.feeds) {
            if (draft.feeds.hasOwnProperty(id)) {
              const feed = draft.feeds[id];

              if (feed.categoryId !== originalFeed.categoryId) {
                continue;
              }

              if (feed.column !== originalFeed.column) {
                continue;
              }

              if (feed.position >= originalFeed.position) {
                feed.position--;
              }
            }
          }
        }

        draft.feeds[action.feed.id] = Object.assign({}, action.feed);
        draft.feeds[action.feed.id].items = action.items;
        draft.feeds[action.feed.id].itemsLastRefresh = action.date;
        break;
      }
    }
  });
}

