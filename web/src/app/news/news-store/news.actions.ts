import {Action} from '@ngrx/store';
import {Category} from './data/category';
import {Feed} from './data/feed';
import {Entry} from './data/entry';

export enum Types {
  LOAD = '[News] Load',
  REFRESH = '[News] Refresh',
  LOADED = '[News] Loaded',
  REFRESHED = '[News] Refreshed',
  LOAD_CATEGORY_ITEMS_ACTION = '[News] Load category items action',
  LOADED_CATEGORY_ITEMS_ACTION = '[News] Loaded category items action',
  REFRESH_CATEGORY_ITEMS_ACTION = '[News] Refresh category items action',
  REFRESHED_CATEGORY_ITEMS_ACTION = '[News] Refreshed category items action',
  REFRESH_FEED_ACTION = '[News] Refresh feed',
  REFRESHED_FEED_ACTION = '[News] Refreshed feed',
  UNLOAD = '[News] Unload',
  ITEM_SET_READ = '[News] Item set read status',
  ITEM_REMOVE = '[News] Remove item',
  ITEM_SET_PINNED = '[News] Item set pinned status',
  FEED_SET_READ = '[News] Feed set read status',
  CATEGORY_SET_READ = '[News] Category set read status',
  ALL_CATEGORIES_SET_READ = '[News] All categories set read status',
  MOVE_CATEGORY = '[News] Move category',
  MOVE_FEED = '[News] Move feed',
  DELETE_CATEGORY = '[News] Delete category',
  DELETE_FEED = '[News] Delete feed',
  CREATE_CATEGORY = '[News] Create category',
  UPDATE_CATEGORY = '[News] Rename category',
  CREATE_FEED = '[News] Create feed',
  UPDATE_FEED = '[News] Update feed'
}

export class ItemSetReadAction implements Action {
  public readonly type = Types.ITEM_SET_READ;

  public constructor(
    public readonly feed: Feed,
    public readonly item: Entry,
    public readonly read: boolean,
    public readonly date: Date
  ) {
  }
}

export class ItemRemoveAction implements Action {
  public readonly type = Types.ITEM_REMOVE;

  public constructor(
    public readonly feed: Feed,
    public readonly item: Entry,
  ) {
  }
}

export class ItemSetPinnedAction implements Action {
  public readonly type = Types.ITEM_SET_PINNED;

  public constructor(
    public readonly feed: Feed,
    public readonly item: Entry,
    public readonly pinned: boolean,
    public readonly date: Date
  ) {
  }
}

export class FeedSetReadAction implements Action {
  public readonly type = Types.FEED_SET_READ;

  public constructor(
    public readonly feed: Feed,
    public readonly date: Date,
    public readonly globalHideEntiresAfterReadSetting: true | false | 'respect-feed-settings'
  ) {
  }
}

export class CategorySetReadAction implements Action {
  public readonly type = Types.CATEGORY_SET_READ;

  public constructor(
    public readonly category: Category,
    public readonly date: Date,
    public readonly globalHideEntiresAfterReadSetting: true | false | 'respect-feed-settings'
  ) {
  }
}

export class AllCategoriesSetReadAction implements Action {
  public readonly type = Types.ALL_CATEGORIES_SET_READ;

  public constructor(
    public readonly date: Date,
    public readonly globalHideEntiresAfterReadSetting: true | false | 'respect-feed-settings'
  ) {
  }
}

export class LoadAction implements Action {
  public readonly type = Types.LOAD;
}

export class UnloadAction implements Action {
  public readonly type = Types.UNLOAD;
}

export class LoadedAction implements Action {
  public readonly type = Types.LOADED;

  public constructor(
    public readonly categories: Category[],
    public readonly feeds: Feed[],
  ) {
  }
}

export class RefreshAction implements Action {
  public readonly type = Types.REFRESH;
}

export class RefreshedAction implements Action {
  public readonly type = Types.REFRESHED;

  public constructor(
    public readonly categories: Category[],
    public readonly feeds: Feed[],
  ) {
  }
}


export class LoadCategoryItemsAction implements Action {
  public readonly type = Types.LOAD_CATEGORY_ITEMS_ACTION;

  public constructor(
    public readonly category: Category,
  ) {
  }
}

export class LoadedCategoryItemsAction implements Action {
  public readonly type = Types.LOADED_CATEGORY_ITEMS_ACTION;

  public constructor(
    public readonly category: Category,
    public readonly items: { [feedId: string]: Entry[] },
    public readonly date: Date
  ) {
  }
}

export class RefreshFeedAction implements Action {
  public readonly type = Types.REFRESH_FEED_ACTION;

  public constructor(
    public readonly feed: Feed,
  ) {
  }
}

export class RefreshedFeedAction implements Action {
  public readonly type = Types.REFRESHED_FEED_ACTION;

  public constructor(
    public readonly feed: Feed,
    public readonly items: Entry[],
    public readonly date: Date
  ) {
  }
}

export class RefreshCategoryItemsAction implements Action {
  public readonly type = Types.REFRESH_CATEGORY_ITEMS_ACTION;

  public constructor(
    public readonly category: Category,
  ) {
  }
}


export class RefreshedCategoryItemsAction implements Action {
  public readonly type = Types.REFRESHED_CATEGORY_ITEMS_ACTION;

  public constructor(
    public readonly category: Category,
    public readonly newItems: { [feedId: string]: Entry[] },
    public readonly date: Date
  ) {
  }
}

export class MoveCategoryAction implements Action {
  public readonly type = Types.MOVE_CATEGORY;

  public constructor(
    public readonly category: Category,
    public readonly toPosition: number
  ) {
  }
}

export class MoveFeedAction implements Action {
  public readonly type = Types.MOVE_FEED;

  public constructor(
    public readonly feed: Feed,
    public readonly toColumn: number,
    public readonly toPosition: number
  ) {
  }
}

export class DeleteFeedAction implements Action {
  public readonly type = Types.DELETE_FEED;

  public constructor(
    public readonly feed: Feed
  ) {
  }
}

export class DeleteCategoryAction implements Action {
  public readonly type = Types.DELETE_CATEGORY;

  public constructor(
    public readonly category: Category
  ) {
  }
}

export class CreateCategoryAction implements Action {
  public readonly type = Types.CREATE_CATEGORY;

  public constructor(
    public readonly category: Category
  ) {
  }
}

export class UpdateCategoryAction implements Action {
  public readonly type = Types.UPDATE_CATEGORY;

  public constructor(
    public readonly category: Category
  ) {
  }
}

export class CreateFeedAction implements Action {
  public readonly type = Types.CREATE_FEED;

  public constructor(
    public readonly feed: Feed
  ) {
  }
}

export class UpdateFeedAction implements Action {
  public readonly type = Types.UPDATE_FEED;

  public constructor(
    public readonly feed: Feed,
    public readonly items: Entry[],
    public readonly date: Date
  ) {
  }
}

export type NewsActionUnion =
  LoadAction |
  LoadedAction |
  RefreshAction |
  RefreshedAction |
  UnloadAction |
  LoadCategoryItemsAction |
  LoadedCategoryItemsAction |
  ItemSetPinnedAction |
  ItemSetReadAction |
  FeedSetReadAction |
  CategorySetReadAction |
  AllCategoriesSetReadAction |
  ItemRemoveAction |
  RefreshCategoryItemsAction |
  RefreshedCategoryItemsAction |
  RefreshFeedAction |
  RefreshedFeedAction |
  MoveCategoryAction |
  MoveFeedAction |
  DeleteFeedAction |
  DeleteCategoryAction |
  CreateCategoryAction |
  UpdateCategoryAction |
  CreateFeedAction |
  UpdateFeedAction;
