import * as NewsActions from './news.actions';
import * as NewsSelector from './news.selector';
import * as NewsState from './news.state';

export {newsReducer} from './news.reducer';
export {
  NewsActions,
  NewsState,
  NewsSelector
};
