import {Category} from './data/category';
import {Feed} from './data/feed';

export interface State {
  categories: { [id: string]: Category } | null;
  feeds: {[id: string]: Feed} | null;
  loaded: boolean;
  loading: boolean;
}

export const defaultState: State = {
  categories: null,
  feeds: null,
  loaded: false,
  loading: false
};
