import {Injectable} from '@angular/core';
import {Category, CategoryStatus} from '../news-store/data/category';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../root-store';
import {
  AllCategoriesSetReadAction,
  CategorySetReadAction,
  CreateCategoryAction,
  DeleteCategoryAction,
  MoveCategoryAction,
  RefreshedCategoryItemsAction,
  UpdateCategoryAction
} from '../news-store/news.actions';
import {Observable, of} from 'rxjs';
import {map, switchMap, take, tap, withLatestFrom} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {CategoryView} from '../../dto/category.view';
import {API_URI_PLACEHOLDER, convertDateForApi, parseDateFromApi} from '../../misc/api';
import {FeedView} from '../../dto/feed.view';
import {EntryDashboardView} from '../../dto/entry.dashboard-view';
import {Entry} from '../news-store/data/entry';
import {NewsSelector} from '../news-store';

type DashboardDataDto = {
  feed: FeedView,
  items: EntryDashboardView[]
}[];

@Injectable()
export class CategoryService {

  public constructor(
    private store: Store<RootState.State>,
    private http: HttpClient
  ) {
  }

  public findAll(): Observable<Category[]> {
    return this.http.get<CategoryView[]>(API_URI_PLACEHOLDER + '/categories').pipe(
      map((data) => {
        return data.map(((item) => {
          return Object.assign({}, item, {
            status: CategoryStatus.NOT_LOADED,
            lastRefresh: null
          });
        }));
      })
    );
  }

  public refreshCategory(category: Category): Observable<null> {
    const date = new Date();

    const url = API_URI_PLACEHOLDER + '/categories/' + category.id +
      '/dashboard-feeds?entries-since=' + encodeURIComponent(category.lastRefresh.toISOString());

    return this.http.get<DashboardDataDto>(url).pipe(
      map(data => {
        const result: { [feedId: string]: Entry[] } = {};

        data.forEach((feedWrap) => {
          feedWrap.items.forEach((entry) => {
            entry.createdAt = parseDateFromApi(entry.createdAt);
            entry.pinnedAt = parseDateFromApi(entry.pinnedAt);
            entry.unpinnedAt = parseDateFromApi(entry.unpinnedAt);
            entry.readAt = parseDateFromApi(entry.readAt);
          });

          result[feedWrap.feed.id] = feedWrap.items;
        });

        this.store.dispatch(new RefreshedCategoryItemsAction(category, result, date));

        return null;
      })
    );
  }

  public getEntriesForCategory(category: Category): Observable<{ [feedId: string]: Entry[] }> {
    return this.http.get<DashboardDataDto>(API_URI_PLACEHOLDER + '/categories/' + category.id + '/dashboard-feeds').pipe(
      map(data => {
        const result: { [feedId: string]: Entry[] } = {};

        data.forEach((feedWrap) => {
          feedWrap.items.forEach((entry) => {
            entry.createdAt = parseDateFromApi(entry.createdAt);
            entry.pinnedAt = parseDateFromApi(entry.pinnedAt);
            entry.unpinnedAt = parseDateFromApi(entry.unpinnedAt);
            entry.readAt = parseDateFromApi(entry.readAt);
          });

          result[feedWrap.feed.id] = feedWrap.items;
        });

        return result;
      })
    );
  }

  public createCategory(name: string): Observable<Category> {
    const date = new Date();

    return this.http.post<CategoryView>(API_URI_PLACEHOLDER + '/categories', {
      name: name
    }).pipe(
      map((categoryView) => {
        return Object.assign({}, categoryView, {
          status: CategoryStatus.LOADED,
          lastRefresh: date
        });
      }),
      tap((category) => {
        this.store.dispatch(new CreateCategoryAction(category));
      })
    );
  }

  public renameCategory(category: Category, name: string): Observable<Category> {
    return this.http.put<CategoryView>(API_URI_PLACEHOLDER + '/categories/' + category.id, {
      name: name
    }).pipe(
      withLatestFrom(this.store.pipe(
        select(NewsSelector.getCategoryById, {id: category.id}),
        take(1)
      ), (newCategory, currentCategory) => {
        return Object.assign({}, currentCategory, {
          name: newCategory.name,
          slug: newCategory.slug
        });
      }),
      tap((newCategory) => {
        this.store.dispatch(new UpdateCategoryAction(newCategory));
      })
    );
  }

  public deleteCategory(category: Category): Observable<null> {
    return of(null).pipe(
      tap(() => {
        this.store.dispatch(
          new DeleteCategoryAction(category)
        );
      }),
      switchMap(() => this.http.delete<null>(API_URI_PLACEHOLDER + '/categories/' + category.id)),
      map(() => null)
    );
  }

  public moveCategory(category: Category, toPosition: number): Observable<null> {
    return of(null).pipe(
      tap(() => {
        this.store.dispatch(new MoveCategoryAction(
          category,
          toPosition
        ));
      }),
      switchMap(() => this.http.put<null>(API_URI_PLACEHOLDER + '/categories/' + category.id + '/position', toPosition)),
    );
  }

  public markAsRead(category: Category): Observable<null> {
    const date = new Date();

    return of(null).pipe(
      tap(() => {
        this.store.select((state) => state.auth.preferences.hideEntriesAfterRead)
          .pipe(take(1))
          .subscribe((hideEntriesAfterReadConfig) => {
            this.store.dispatch(
              new CategorySetReadAction(category, date, hideEntriesAfterReadConfig)
            );
          });
      }),
      switchMap(() => {
        return this.http.put<null>(
          `${API_URI_PLACEHOLDER}/categories/${category.id}/read?entries-till=${encodeURIComponent(category.lastRefresh.toISOString())}`,
          convertDateForApi(date)
        );
      }),
      map(() => null)
    );
  }

  public markAllAsRead(): Observable<null> {
    const date = new Date();

    return of(null).pipe(
      tap(() => {
        this.store.select((state) => state.auth.preferences.hideEntriesAfterRead)
          .pipe(take(1))
          .subscribe((hideEntriesAfterReadConfig) => {
            this.store.dispatch(
              new AllCategoriesSetReadAction(date, hideEntriesAfterReadConfig)
            );
          });
      }),
      switchMap(() => {
        return this.http.put<null>(
          `${API_URI_PLACEHOLDER}/categories/read`,
          convertDateForApi(date)
        );
      }),
      map(() => null)
    );
  }
}
