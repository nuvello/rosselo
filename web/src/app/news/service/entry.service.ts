import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {RootState} from '../../root-store';
import {Observable, of} from 'rxjs';
import {Feed} from '../news-store/data/feed';
import {map, switchMap, tap} from 'rxjs/operators';
import {ItemSetPinnedAction, ItemSetReadAction} from '../news-store/news.actions';
import {HttpClient} from '@angular/common/http';
import {API_URI_PLACEHOLDER, convertDateForApi} from '../../misc/api';
import {Entry} from '../news-store/data/entry';

@Injectable()
export class EntryService {

  public constructor(
    private store: Store<RootState.State>,
    private http: HttpClient
  ) {
  }

  public markAsRead(feed: Feed, entry: Entry, read: boolean): Observable<null> {
    const date = new Date();

    return of(null).pipe(
      tap(() => {
        this.store.dispatch(
          new ItemSetReadAction(feed, entry, read, date)
        );
      }),
      switchMap(() => this.http.put<null>(`${API_URI_PLACEHOLDER}/entries/${entry.id}/read`, {
        read: read,
        date: convertDateForApi(date)
      })),
      map(() => null)
    );
  }

  public markAsPinned(feed: Feed, entry: Entry, pinned: boolean): Observable<null> {
    const date = new Date();

    return of(null).pipe(
      tap(() => {
        this.store.dispatch(
          new ItemSetPinnedAction(feed, entry, pinned, date)
        );
      }),
      switchMap(() => this.http.put<null>(`${API_URI_PLACEHOLDER}/entries/${entry.id}/pinned`, {
        pinned: pinned,
        date: convertDateForApi(date)
      })),
      map(() => null)
    );
  }
}
