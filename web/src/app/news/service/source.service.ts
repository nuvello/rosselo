import {Injectable} from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {API_URI_PLACEHOLDER} from '../../misc/api';
import {SourceSearchView} from '../../dto/source.search-view';
import {CreateSourceResult} from '../../dto/create-source.result';
import {map} from 'rxjs/operators';
import {EntryView} from '../../dto/entry.view';
import {CreateSourceType} from '../../dto/create-source-type';
import {Entry} from '../news-store/data/entry';

@Injectable()
export class SourceService {

    public constructor(
        private http: HttpClient
    ) {
    }

    public create(
        type: CreateSourceType,
        source: string
    ): Observable<CreateSourceResult> {
        return this.http.post<CreateSourceResult>(API_URI_PLACEHOLDER + '/sources', {
            type: type,
            source: source
        });
    }

    public search(
        query: string,
        page: number,
        languages: string[],
        tlds: string[]
    ): Observable<SourceSearchView[]> {
        return this.http.get<SourceSearchView[]>(API_URI_PLACEHOLDER + '/sources', {
            params: {
                query: query,
                page: String(page),
                languages: languages,
                tlds: tlds
            }
        });
    }

    public previewItems(
        id: string
    ): Observable<EntryView[]> {
        return this.http.get<{
            items: EntryView[]
        }>(API_URI_PLACEHOLDER + '/sources/' + encodeURIComponent(id)).pipe(
            map((data) => data.items)
        );
    }
}
