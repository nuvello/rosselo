import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {RootState} from '../../root-store';
import {Observable, of, throwError} from 'rxjs';
import {Feed} from '../news-store/data/feed';
import {catchError, map, switchMap, take, tap} from 'rxjs/operators';
import {
  CreateFeedAction,
  DeleteFeedAction,
  FeedSetReadAction,
  MoveFeedAction, RefreshAction,
  RefreshedFeedAction,
  UpdateFeedAction
} from '../news-store/news.actions';
import {FeedView} from '../../dto/feed.view';
import {HttpClient} from '@angular/common/http';
import {API_URI_PLACEHOLDER, convertDateForApi, parseDateFromApi} from '../../misc/api';
import {EntryDashboardView} from '../../dto/entry.dashboard-view';

@Injectable()
export class FeedService {

  public constructor(
    private store: Store<RootState.State>,
    private http: HttpClient
  ) {
  }

  public findAll(): Observable<Feed[]> {
    return this.http.get<FeedView[]>(API_URI_PLACEHOLDER + '/feeds').pipe(
      map((items) => {
        return items.map((item) => {
          return this.feedViewToFeed(item);
        });
      })
    );
  }

  public createFeed(data: {
    name: string;
    sourceId: string;
    hideEntriesAfterRead: boolean;
    maxAmountOfItems: number | null;
    noOlderThan: number | null;
    categoryId: string;
  }): Observable<Feed> {
    const date = new Date();

    return this.http.post<FeedView>(API_URI_PLACEHOLDER + '/feeds', {
      categoryId: data.categoryId,
      name: data.name,
      sourceId: data.sourceId,
      hideEntriesAfterRead: data.hideEntriesAfterRead,
      maxAmountOfItems: data.maxAmountOfItems,
      noOlderThan: data.noOlderThan
    }).pipe(
      switchMap((feed) => this.http.get<{
        feed: FeedView,
        items: EntryDashboardView[]
      }>(API_URI_PLACEHOLDER + '/feeds/' + feed.id + '/dashboard')),
      map((response) => {
        const feed = this.feedViewToFeed(response.feed);
        response.items.forEach((entry) => {
          entry.createdAt = parseDateFromApi(entry.createdAt);
          entry.pinnedAt = parseDateFromApi(entry.pinnedAt);
          entry.unpinnedAt = parseDateFromApi(entry.unpinnedAt);
          entry.readAt = parseDateFromApi(entry.readAt);
        });
        feed.items = response.items;
        feed.itemsLastRefresh = date;
        return feed;
      }),
      tap((feed) => {
        this.store.dispatch(new CreateFeedAction(feed));
      })
    );
  }

  public refreshFeed(feed: Feed): Observable<Feed> {
    const date = new Date();

    return this.http.get<{
      feed: FeedView,
      items: EntryDashboardView[]
    }>(API_URI_PLACEHOLDER + '/feeds/' + feed.id + '/dashboard').pipe(
      map((response) => {
        const newFeed = this.feedViewToFeed(response.feed);

        // todo
        response.items.forEach((entry) => {
          entry.createdAt = parseDateFromApi(entry.createdAt);
          entry.pinnedAt = parseDateFromApi(entry.pinnedAt);
          entry.unpinnedAt = parseDateFromApi(entry.unpinnedAt);
          entry.readAt = parseDateFromApi(entry.readAt);
        });

        this.store.dispatch(new RefreshedFeedAction(newFeed, response.items, date));

        return newFeed;
      }),
    );
  }

  public updateFeed(id: string, data: {
    sourceId: string;
    name: string;
    hideEntriesAfterRead: boolean;
    maxAmountOfItems: number | null;
    noOlderThan: number | null;
    categoryId: string
  }): Observable<Feed> {
    const date = new Date();
    return this.http.put<FeedView>(API_URI_PLACEHOLDER + '/feeds/' + id, {
      name: data.name,
      sourceId: data.sourceId,
      hideEntriesAfterRead: data.hideEntriesAfterRead,
      maxAmountOfItems: data.maxAmountOfItems,
      noOlderThan: data.noOlderThan,
      categoryId: data.categoryId
    }).pipe(
      switchMap(() => this.http.get<{
        feed: FeedView,
        items: EntryDashboardView[]
      }>(API_URI_PLACEHOLDER + '/feeds/' + id + '/dashboard')),
      map((response) => {
        const feed = this.feedViewToFeed(response.feed);

        response.items.forEach((entry) => {
          entry.createdAt = parseDateFromApi(entry.createdAt);
          entry.pinnedAt = parseDateFromApi(entry.pinnedAt);
          entry.unpinnedAt = parseDateFromApi(entry.unpinnedAt);
          entry.readAt = parseDateFromApi(entry.readAt);
        });

        this.store.dispatch(new UpdateFeedAction(feed, response.items, date));

        return feed;
      }),
    );
  }

  public deleteFeed(feed: Feed): Observable<null> {
    return of(null).pipe(
      tap(() => {
        this.store.dispatch(
          new DeleteFeedAction(feed)
        );
      }),
      switchMap(() => this.http.delete<null>(API_URI_PLACEHOLDER + '/feeds/' + feed.id)),
      map(() => null)
    );
  }


  public moveFeed(feed: Feed, toColumn: number, toPosition: number): Observable<null> {
    return of(null).pipe(
      tap(() => {
        this.store.dispatch(new MoveFeedAction(
          feed,
          toColumn,
          toPosition
        ));
      }),
      switchMap(() => this.http.put<null>(API_URI_PLACEHOLDER + '/feeds/' + feed.id + '/position', {
        column: toColumn,
        position: toPosition
      })),
    );
  }

  public markAsRead(feed: Feed): Observable<null> {
    const date = new Date();

    const url = `${API_URI_PLACEHOLDER}/feeds/${feed.id}/read?entries-till=${encodeURIComponent(feed.itemsLastRefresh.toISOString())}`;

    return of(null).pipe(
      tap(() => {
        this.store.select((state) => state.auth.preferences.hideEntriesAfterRead)
          .pipe(take(1))
          .subscribe((hideEntriesAfterReadConfig) => {
            this.store.dispatch(
              new FeedSetReadAction(feed, date, hideEntriesAfterReadConfig)
            );
          });
      }),
      switchMap(() => this.http.put<null>(url, convertDateForApi(date))),
      map(() => null)
    );
  }

  public importFeeds(file, categoryId?: string): Observable<null> {
    const formData = new FormData();
    formData.append('file', file, file.name);

    if (categoryId != null) {
      formData.append('categoryId', categoryId);
    }

    return this.http.post<null>(API_URI_PLACEHOLDER + '/content/import', formData).pipe(
      tap(() => {
        this.store.dispatch(new RefreshAction());
      }),
      catchError((error) => {
        if (error.error && error.error.details) {
          return throwError(error.error.details);
        }
        return throwError('An unknown error occurred while importing feeds.');
      })
    );
  }

  private feedViewToFeed(feedView: FeedView): Feed {
    return {
      id: feedView.id,
      column: feedView.column,
      source: {
        id: feedView.source.id,
        type: feedView.source.type,
        source: feedView.source.source,
        title: feedView.source.title,
        link: feedView.source.link,
        description: feedView.source.description,
        subscribers: feedView.source.subscribers,
        error: feedView.source.error,
        lastSuccessfulFetchAt: parseDateFromApi(feedView.source.lastSuccessfulFetchAt),
        lastExtraScrapAt: parseDateFromApi(feedView.source.lastExtraScrapAt),
        bigIconUrl: feedView.source.bigIconUrl,
        smallIconUrl: feedView.source.smallIconUrl
      },
      createdAt: parseDateFromApi(feedView.createdAt),
      position: feedView.position,
      name: feedView.name,
      hideEntriesAfterRead: feedView.hideEntriesAfterRead,
      maxAmountOfItems: feedView.maxAmountOfItems,
      noOlderThan: feedView.noOlderThan,
      categoryId: feedView.categoryId,
      items: null,
      itemsLastRefresh: null
    };
  }
}
