import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {StoreModule} from '@ngrx/store';
import {NewsRoutingModule} from './news-routing.module';
import {newsReducer} from './news-store';
import {BaseComponent} from './components/base/base.component';
import {SharedModule} from '../shared/shared.module';
import {EffectsModule} from '@ngrx/effects';
import {NewsEffects} from './news-store/news.effects';
import {NewsPageComponent} from './pages/news-page/news-page.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import { FeedComponent } from './components/feed/feed.component';
import { SettingsPageComponent } from './pages/preferences/settings-page/settings-page.component';
import { AccountPageComponent } from './pages/preferences/account-page/account-page.component';
import { CategoriesPageComponent } from './pages/preferences/categories-page/categories-page.component';
import { CategoryDetailPageComponent } from './pages/preferences/category-detail-page/category-detail-page.component';
import { FeedDetailPageComponent } from './pages/preferences/feed-detail-page/feed-detail-page.component';
import { PreferencesLayoutComponent } from './components/preferences-layout/preferences-layout.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CategoryService} from './service/category.service';
import {FeedService} from './service/feed.service';
import {EntryService} from './service/entry.service';
import { HotkeysPageComponent } from './pages/preferences/hotkeys-page/hotkeys-page.component';
import { FeedSettingsComponent } from './components/preferences/feed-settings/feed-settings.component';
import { FeedImportComponent } from './components/preferences/feed-import/feed-import.component';
import { FeedWebsitesComponent } from './components/preferences/feed-websites/feed-websites.component';
import {SourceService} from './service/source.service';
import { FeedWebsitesSearchResultComponent } from './components/preferences/feed-websites-search-result/feed-websites-search-result.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    BaseComponent,
    NewsPageComponent,
    FeedComponent,
    SettingsPageComponent,
    AccountPageComponent,
    CategoriesPageComponent,
    CategoryDetailPageComponent,
    FeedDetailPageComponent,
    PreferencesLayoutComponent,
    HotkeysPageComponent,
    FeedSettingsComponent,
    FeedImportComponent,
    FeedWebsitesComponent,
    FeedWebsitesSearchResultComponent
  ],
  imports: [
    CommonModule,
    NewsRoutingModule,
    SharedModule,
    StoreModule.forFeature('news', newsReducer),
    EffectsModule.forFeature([NewsEffects]),
    FormsModule,
    ReactiveFormsModule,
    DragDropModule,
    InfiniteScrollModule
  ],
  providers: [
    CategoryService,
    FeedService,
    EntryService,
    SourceService
  ]
})
export class NewsModule {
}
