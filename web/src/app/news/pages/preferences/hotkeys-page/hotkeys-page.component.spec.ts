import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotkeysPageComponent } from './hotkeys-page.component';

describe('HotkeysPageComponent', () => {
  let component: HotkeysPageComponent;
  let fixture: ComponentFixture<HotkeysPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotkeysPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotkeysPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
