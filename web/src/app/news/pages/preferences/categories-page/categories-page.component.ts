import {Component, OnInit} from '@angular/core';
import {RootState} from '../../../../root-store';
import {select, Store} from '@ngrx/store';
import {NewsSelector} from '../../../news-store';
import {Observable} from 'rxjs';
import {Category} from '../../../news-store/data/category';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {CategoryService} from '../../../service/category.service';
import {Meta, Title} from '@angular/platform-browser';
import {isMobile} from '../../../../misc/other';

@Component({
  selector: 'rs-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss']
})
export class CategoriesPageComponent implements OnInit {
  public categories$: Observable<Category[]>;
  public isMobile = isMobile();
  public categoriesLimit$: Observable<number>;

  public constructor(
    private store: Store<RootState.State>,
    private categoryService: CategoryService,
    private title: Title,
    private meta: Meta
  ) {
  }

  public ngOnInit() {
    this.title.setTitle('Rosselo');
    this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
    this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

    this.categories$ = this.store.pipe(
      select(NewsSelector.getSortedCategories)
    );

    this.categoriesLimit$ = this.store.select((state) => state.config.maxNumberOfCategoriesPerAccount);
  }

  public getSlug(category: Category): string {
    return category.slug;
  }

  public onCategoryDrop(event: CdkDragDrop<Category[]>) {
    this.categoryService.moveCategory(
      <Category>event.item.data,
      event.currentIndex
    ).subscribe();
  }

  public deleteCategory(category: Category) {
    if (confirm('Do you really want to remove the category?')) {
      this.categoryService.deleteCategory(
        category
      ).subscribe();
    }
  }
}
