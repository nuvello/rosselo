import {Component, OnDestroy, OnInit} from '@angular/core';
import {RootState} from '../../../../root-store';
import {select, Store} from '@ngrx/store';
import {AuthSelectors} from '../../../../root-store/auth';
import {map, takeUntil} from 'rxjs/operators';
import {Preferences} from '../../../../root-store/auth/auth.state';
import {Subject} from 'rxjs';
import {AuthService} from '../../../../shared/services/auth.service';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'rs-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.scss']
})
export class SettingsPageComponent implements OnInit, OnDestroy {
  public preferences: Preferences;
  public switchToggled = false;

  private onDestroy$ = new Subject();


  public constructor(
    private store: Store<RootState.State>,
    private authService: AuthService,
    private title: Title,
    private meta: Meta
  ) {
  }

  public ngOnInit() {
    this.title.setTitle('Rosselo');
    this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
    this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

    this.store.pipe(
      select(AuthSelectors.getAuthState),
      map(value => value.preferences),
      takeUntil(this.onDestroy$)
    ).subscribe((preferences) => {
      this.preferences = Object.assign({}, preferences);
    });
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();
  }

  public save(): void {
    this.authService
      .updatePreference(Object.assign({}, this.preferences))
      .subscribe();
  }

  public visitedClick(event: MouseEvent, value: true | false | 'respect-feed-settings') {
    event.preventDefault();
    this.switchToggled = !this.switchToggled;

    if (this.switchToggled) {
      return;
    }

    this.preferences.hideEntriesAfterRead = value;
    this.save();
  }
}
