import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../../../root-store';
import {Observable} from 'rxjs';
import {AuthSelectors, AuthState} from '../../../../root-store/auth';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../../../shared/services/auth.service';
import {finalize} from 'rxjs/operators';
import {Meta, Title} from '@angular/platform-browser';

enum FormState {
  DEFAULT = 0,
  WORKING = 1,
  SUCCESS = 2,
  ERROR = 3,
}

@Component({
  selector: 'rs-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent implements OnInit {

  public auth$: Observable<AuthState.State>;
  public changePasswordForm: FormGroup;
  public changeEmailForm: FormGroup;
  public mode: 'none' | 'change-password' | 'change-email' = 'none';
  public terminating = false;
  public terminatingError: string = null;

  public changePasswordMeta: {
    error: string,
    state: FormState,
    resetTimeoutId: any
  } = {
    error: null,
    state: FormState.DEFAULT,
    resetTimeoutId: null
  };
  public changeEmailMeta: {
    error: string,
    state: FormState,
    resetTimeoutId: any
  } = {
    error: null,
    state: FormState.DEFAULT,
    resetTimeoutId: null
  };

  public get oldPassword(): AbstractControl {
    return this.changePasswordForm.get('oldPassword');
  }

  public get newPassword(): AbstractControl {
    return this.changePasswordForm.get('newPassword');
  }

  public get newEmail(): AbstractControl {
    return this.changeEmailForm.get('newEmail');
  }

  public constructor(
    private store: Store<RootState.State>,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private title: Title,
    private meta: Meta
  ) {
  }

  public ngOnInit() {
    this.title.setTitle('Rosselo');
    this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
    this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

    this.auth$ = this.store.pipe(
      select(AuthSelectors.getAuthState)
    );

    this.changePasswordForm = this.formBuilder.group({
      oldPassword: ['', [Validators.required, Validators.minLength(6)]],
      newPassword: ['', [Validators.required, Validators.minLength(6)]]
    });
    this.changeEmailForm = this.formBuilder.group({
      newEmail: ['', [Validators.required, Validators.email]]
    });
  }

  public changePassword() {
    if (this.changePasswordMeta.state === FormState.WORKING || !this.changePasswordForm.valid) {
      return;
    }

    this.changePasswordMeta.state = FormState.WORKING;
    clearTimeout(this.changePasswordMeta.resetTimeoutId);

    this.authService
      .changePassword(this.oldPassword.value, this.newPassword.value)
      .pipe(
        finalize(() => {
          clearTimeout(this.changePasswordMeta.resetTimeoutId);
          this.changePasswordMeta.resetTimeoutId = setTimeout(() => {
            this.changePasswordMeta.state = FormState.DEFAULT;
          }, 3000);
        })
      )
      .subscribe(() => {
        this.changePasswordMeta.state = FormState.SUCCESS;
        this.changePasswordForm.reset();
      }, (error) => {
        this.changePasswordMeta.state = FormState.ERROR;
        this.changePasswordMeta.error = error;
      });
  }

  public changeEmail() {
    if (this.changeEmailMeta.state === FormState.WORKING || !this.changeEmailForm.valid) {
      return;
    }

    if (confirm('Do you really want to change your e-mail address?')) {
      this.changeEmailMeta.state = FormState.WORKING;
      clearTimeout(this.changeEmailMeta.resetTimeoutId);

      this.authService
        .changeEmail(this.newEmail.value)
        .pipe(
          finalize(() => {
            clearTimeout(this.changeEmailMeta.resetTimeoutId);
            this.changeEmailMeta.resetTimeoutId = setTimeout(() => {
              this.changeEmailMeta.state = FormState.DEFAULT;
            }, 3000);
          })
        )
        .subscribe(() => {
          this.changeEmailMeta.state = FormState.SUCCESS;
          this.changeEmailForm.reset();
        }, (error) => {
          this.changeEmailMeta.state = FormState.ERROR;
          this.changeEmailMeta.error = error;
        });
    }
  }

  public onDeleteClick(event: MouseEvent) {
    event.preventDefault();

    if (this.terminating) {
      return;
    }

    if (confirm('Do you really want to permanently delete your account?')) {
      this.terminating = true;
      this.terminatingError = null;
      this.authService.terminateAccount().subscribe(() => {
        this.terminating = false;
      }, (error) => {
        this.terminatingError = error;
        this.terminating = false;
      });
    }
  }

  public toggleMode(mode: 'none' | 'change-password' | 'change-email') {
    if (this.mode === mode) {
      this.mode = 'none';
    } else {
      this.mode = mode;
    }
  }
}
