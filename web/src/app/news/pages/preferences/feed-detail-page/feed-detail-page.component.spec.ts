import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedDetailPageComponent } from './feed-detail-page.component';

describe('FeedDetailPageComponent', () => {
  let component: FeedDetailPageComponent;
  let fixture: ComponentFixture<FeedDetailPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedDetailPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
