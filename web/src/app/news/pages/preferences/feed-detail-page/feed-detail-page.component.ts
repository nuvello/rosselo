import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../../../root-store';
import {ActivatedRoute, Router} from '@angular/router';
import {NewsSelector} from '../../../news-store';
import {map, switchMap, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Category} from '../../../news-store/data/category';
import {Feed} from '../../../news-store/data/feed';
import {Meta, Title} from '@angular/platform-browser';
import {RouterExtService} from '../../../../misc/router.extension';
import {AdvancedSelectOption} from '../../../../shared/components/advanced-select/advanced-select-option.class';
import {SourceType} from '../../../../dto/source-type';

enum Mode {
    SEARCH = 0,
    URL = 1,
    CHANNELS = 2,
    IMPORT = 3
}

@Component({
    selector: 'rs-feed-detail-page',
    templateUrl: './feed-detail-page.component.html',
    styleUrls: ['./feed-detail-page.component.scss']
})
export class FeedDetailPageComponent implements OnInit, OnDestroy {
    public mode: Mode = Mode.SEARCH;
    /**
     * Feed to edit or null in case of creation of a new one.
     */
    public feed: Feed | null;
    // todo docs
    public locked = false;
    /**
     * Category of the feed to edit or null in case of creation of a new one.
     */
    public category$: Observable<Category>;
    public availableMenuOptions: AdvancedSelectOption<{ url: string, mode: Mode }>[];
    public menuValue: AdvancedSelectOption<{ url: string, mode: Mode }>;

    public allMenuOptions: AdvancedSelectOption<{ url: string, mode: Mode }>[];

    public isOverLimit$: Observable<boolean>;
    public feedsLimit$: Observable<number>;

    public SourceType = SourceType;
    public fromDashboard = false;
    public previousUrl: string | null;
    public onDestroy$ = new Subject();

    public constructor(
        private store: Store<RootState.State>,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private title: Title,
        private metaTags: Meta,
        private routerExtService: RouterExtService,
    ) {
    }

    public ngOnInit() {
        this.title.setTitle('Rosselo');
        this.metaTags.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
        this.metaTags.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

        this.allMenuOptions = [
            new AdvancedSelectOption(
                'Websites',
                'menu',
                {url: 'website', mode: Mode.SEARCH}
            ),
            new AdvancedSelectOption(
                'Feed URL',
                'rss',
                {url: 'url', mode: Mode.URL}
            ),
            new AdvancedSelectOption(
                'Channels',
                'hash',
                {url: 'channel', mode: Mode.CHANNELS}
            ),
            new AdvancedSelectOption(
                'Import',
                'import',
                {url: 'import', mode: Mode.IMPORT}
            )
        ];

        this.previousUrl = this.routerExtService.getPreviousUrl();
        this.fromDashboard = this.previousUrl !== null && !this.previousUrl.startsWith('/app/settings');
        this.feedsLimit$ = this.store.select((state) => state.config.maxNumberOfFeedsPerAccount);

        this.category$ = this.activatedRoute.params.pipe(
            map((params) => params['categoryId']),
            switchMap((id) => {
                return this.store.pipe(
                    select(NewsSelector.getCategoryById, {id: id})
                );
            })
        );

        this.activatedRoute.params.pipe(
            switchMap((params) => {
                const feedId = params['feedId'];

                if (feedId == null) {
                    return of(null);
                }

                return this.store.pipe(
                    select(NewsSelector.getFeedById, {id: feedId}),
                    map((feed) => Object.assign({}, feed)),
                    take(1)
                );
            }),
            takeUntil(this.onDestroy$)
        ).subscribe((feed) => {
            this.feed = feed;
            this.rebuildMenu();
        });

        this.isOverLimit$ = this.store.pipe(
            select(NewsSelector.getNewsState),
            take(1),
            map((state) => Object.values(state.feeds).length),
            withLatestFrom(this.feedsLimit$, (feeds, limit) => {
                return feeds >= limit;
            })
        );

        this.activatedRoute.params.pipe(
            takeUntil(this.onDestroy$)
        ).subscribe((params) => {
            if (params['type']) {
                this.menuValue = this.availableMenuOptions[0];
                this.mode = this.menuValue.data.mode;

                for (const option of this.allMenuOptions) {
                    if (option.data.url === params['type']) {
                        this.mode = option.data.mode;
                        this.menuValue = option;
                    }
                }
            }
        });
    }

    public ngOnDestroy(): void {
        this.onDestroy$.next(null);
        this.onDestroy$.complete();
    }

    public goBack() {
        const tim = setTimeout(() => {
            this.router.navigateByUrl(this.previousUrl == null ? '/app' : this.previousUrl);
        }, 1200); // timeout just for effect

        this.onDestroy$.subscribe(() => clearTimeout(tim));
    }

    public setModeByFeedSettings(newValue: 'new-channel' | 'new-url' | Feed) {
        let mode = Mode.URL;

        if (newValue === 'new-channel') {
            mode = Mode.CHANNELS;
        } else if (typeof newValue === 'object') {
            // we disallow changing the tab, we'll go back
            this.locked = true;

            if (newValue.source.type !== SourceType.RSS_URL) {
                mode = Mode.CHANNELS;
            }
        }

        this.selectMenuValue(this.allMenuOptions.find(value => value.data.mode === mode));
    }

    public selectMenuValue($event: AdvancedSelectOption<{ url: string, mode: Mode }>) {
        if (this.locked || (this.menuValue === $event && this.mode === $event.data.mode)) {
            return;
        }

        this.menuValue = $event;
        this.mode = $event.data.mode;

        this.category$.subscribe((category) => {
            const categoryId = category == null ? 'null' : category.id;
            if (this.feed === null) {
                this.router.navigate(['/app/settings/groups/', categoryId, 'feeds', 'new', $event.data.url], {
                    replaceUrl: true
                });
            } else {
                this.router.navigate(['/app/settings/groups/', categoryId, 'feeds', this.feed.id], {
                    replaceUrl: true
                });
            }
        });

    }

    private rebuildMenu(): void {
        const source = this.feed !== null ? this.feed.source : null;

        if (source == null) {
            this.availableMenuOptions = this.allMenuOptions;
        } else if (source.type === SourceType.RSS_URL) {
            this.availableMenuOptions = this.allMenuOptions.filter((val) => val.data.mode === Mode.URL);
        } else {
            this.availableMenuOptions = this.allMenuOptions.filter((val) => val.data.mode === Mode.CHANNELS);
        }

        this.menuValue = this.availableMenuOptions[0];
        this.mode = this.availableMenuOptions[0].data.mode;
    }

    public getWhatToShowInFeedSettings(): Feed | 'new-channel' | 'new-url' {
        if (this.feed !== null) {
            return this.feed;
        }

        switch (this.mode) {
            case Mode.CHANNELS:
                return 'new-channel';
            case Mode.URL:
                return 'new-url';
            default:
                throw new Error('Unknown mode');
        }
    }
}
