import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {RootState} from '../../../../root-store';
import {Observable, of, Subject} from 'rxjs';
import {Category} from '../../../news-store/data/category';
import {NewsSelector} from '../../../news-store';
import {ActivatedRoute, Router} from '@angular/router';
import {filter, map, switchMap, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Feed} from '../../../news-store/data/feed';
import {CategoryService} from '../../../service/category.service';
import {FeedService} from '../../../service/feed.service';
import {Meta, Title} from '@angular/platform-browser';
import {RouterExtService} from '../../../../misc/router.extension';

@Component({
  selector: 'rs-category-detail-page',
  templateUrl: './category-detail-page.component.html',
  styleUrls: ['./category-detail-page.component.scss']
})
export class CategoryDetailPageComponent implements OnInit, OnDestroy, AfterViewInit {
  public category: Category;
  public feeds$: Observable<Feed[]>;
  public name = '';
  public placeholder = 'NAME YOUR GROUP';
  public creatingNew = false;
  public feedsLimit$: Observable<number>;
  public feedsCount$: Observable<number>;
  public fromDashboard = false;

  private onDestroy$ = new Subject();
  private working = false;

  @ViewChild('nameInput', {read: ElementRef, static: true})
  public nameInput: ElementRef;

  public constructor(
    private store: Store<RootState.State>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private routerExtService: RouterExtService,
    private categoryService: CategoryService,
    private feedService: FeedService,
    private title: Title,
    private meta: Meta
  ) {
  }

  public ngOnInit() {
    const previousUrl = this.routerExtService.getPreviousUrl();
    this.fromDashboard = previousUrl !== null && !previousUrl.startsWith('/app/settings');

    this.title.setTitle('Rosselo');
    this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
    this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

    this.feedsLimit$ = this.store.select((state) => state.config.maxNumberOfFeedsPerAccount);
    this.feedsCount$ = this.store.pipe(
      select(NewsSelector.getNewsState),
      map((data) => Object.values(data.feeds).length)
    );

    const category$ = this.activatedRoute.params.pipe(
      switchMap((params) => {
        if (params.categoryId === 'new') {
          return of(null);
        }

        return this.store.pipe(
          select(NewsSelector.getCategoryById, {id: params.categoryId})
        );
      }),
      takeUntil(this.onDestroy$)
    );

    category$.subscribe((category) => {
      if (this.working) {
        this.category = category;
        return;
      }

      this.creatingNew = category === null;
      if (this.category == null || category == null || this.category.id !== category.id) {
        if (category !== null) {
          this.name = category.name;
        } else {
          this.name = '';
        }
      }

      this.category = category;

      if (this.creatingNew && this.nameInput != null) {
        this.nameInput.nativeElement.focus();
      }

      if (this.creatingNew) {
        this.store.pipe(
          select(NewsSelector.getCategories),
          take(1),
          map((cats) => cats.length),
          withLatestFrom(this.store.select((state) => state.config.maxNumberOfCategoriesPerAccount), (cats, limit) => {
            return cats >= limit;
          }),
          filter((exceeded) => exceeded === true)
        ).subscribe(() => {
          this.router.navigate(['..'], {relativeTo: this.activatedRoute});
        });
      }
    });

    this.feeds$ = category$.pipe(
      switchMap((category) => {
        if (category === null) {
          return of(<Feed[]>[]);
        }

        return this.store.pipe(
          select(NewsSelector.getSortedFeedsForCategory, {category: category})
        );
      })
    );
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next(null);
    this.onDestroy$.complete();

    this.save();
  }

  public getId(feed: Feed): string {
    return feed.id;
  }

  public ngAfterViewInit(): void {
    setTimeout(() => {
      if (this.creatingNew) {
        this.nameInput.nativeElement.focus();
      }
    });
  }

  public save() {
    if (this.name.trim().length === 0 || this.working || (this.category !== null && this.category.name === this.name.trim())) {
      return;
    }

    this.working = true;
    if (this.creatingNew) {
      this.categoryService.createCategory(this.name)
        .subscribe((category) => {
          this.working = false;
          this.router.navigate(['../', category.id], {relativeTo: this.activatedRoute, replaceUrl: true});
        });
    } else {
      this.categoryService.renameCategory(this.category, this.name)
        .subscribe((category) => {
          this.working = false;
        });
    }
  }

  public deleteFeed(feed: Feed) {
    if (confirm('Do you really want to delete the feed?')) {
      this.feedService.deleteFeed(feed)
        .subscribe();
    }
  }
}
