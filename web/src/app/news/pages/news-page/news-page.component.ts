import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren
} from '@angular/core';
import {RootState} from '../../../root-store';
import {select, Store} from '@ngrx/store';
import {distinctUntilChanged, filter, map, shareReplay, startWith, switchMap, take, takeUntil, withLatestFrom} from 'rxjs/operators';
import {Category, CategoryStatus} from '../../news-store/data/category';
import {fromEvent, merge, Observable, of, ReplaySubject, Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Feed} from '../../news-store/data/feed';
import {AuthService} from '../../../shared/services/auth.service';
import * as NewsSelector from '../../news-store/news.selector';
import {AuthSelectors} from '../../../root-store/auth';
import * as NewsActions from '../../news-store/news.actions';
import {animate, style, transition, trigger} from '@angular/animations';
import {isTouchDevice} from '../../../misc/utils';
import {CdkDragDrop} from '@angular/cdk/drag-drop';
import {CategoryService} from '../../service/category.service';
import {FeedService} from '../../service/feed.service';
import {Meta, Title} from '@angular/platform-browser';
import {HIDE_TRIAL_NOTIFICATION_TOKEN_KEY} from '../../../misc/api';
import {isMobile} from '../../../misc/other';

@Component({
  selector: 'rs-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('fadeOut', [
      transition(':leave', [
        animate(
          '0.2s',
          style({
            height: 0,
            opacity: 0
          })
        )
      ])
    ]),
    trigger('slide', [
      transition(':enter', [
        style({
          height: 0,
          opacity: 0
        }),
        animate('0.1s ease', style({
          height: '*',
          opacity: 1
        }))
      ]),
      transition(':leave', [
        animate('0.1s ease', style({
          height: 0,
          display: 'none',
          opacity: 0
        }))
      ])
    ])
  ]
})
export class NewsPageComponent implements OnInit, OnDestroy, AfterViewInit {
  public categories: Category[];
  public activeCategory: Category | null = null;
  public feedColumns$: Observable<Array<Feed[]> | null>;
  public sortMode = false;
  public hideEmptyFeeds = false;
  public trialNotificationVisible$: Subject<boolean>;
  public mobileLayout = false;
  public isMobile = isMobile();
  public expandSubmenu = false;
  public expandCategories = false;
  public categoriesOverflowButtonMouseover = false;
  public overflowCategories: Category[] = [];
  public showAllRead$: Observable<boolean>;

  @ViewChildren('categoryElement', {read: ElementRef})
  public categoryListItemElement: QueryList<ElementRef>;

  @ViewChild('addGroupElement', {read: ElementRef, static: false})
  public addGroupElement: ElementRef;

  private onDestroy$: Subject<void> = new Subject();
  private moveAddGroupElement = false;

  constructor(
    private store: Store<RootState.State>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private changeDetectionRef: ChangeDetectorRef,
    private categoryService: CategoryService,
    private feedService: FeedService,
    private title: Title,
    private meta: Meta
  ) {
  }

  ngOnInit() {
    this.title.setTitle('Rosselo');
    this.meta.updateTag({name: 'description', content: 'Register Rosselo account and start using Rosselo RSS dashboard today'});
    this.meta.updateTag({name: 'tags', content: 'registration, signup, news, rss, feeds, dashboard, app, newsfeed'});

    fromEvent(window, 'resize').pipe(
      startWith(null),
      takeUntil(this.onDestroy$)
    ).subscribe(() => {
      const mobileLayout = window.innerWidth <= 768;

      if (this.mobileLayout !== mobileLayout) {
        this.expandSubmenu = false;
        this.expandCategories = false;
      }

      this.mobileLayout = mobileLayout;
      this.changeDetectionRef.markForCheck();
    });

    merge(
      fromEvent(window, 'click'),
      fromEvent(window, 'contextmenu'),
      fromEvent(window, 'keyup').pipe(
        filter((event: KeyboardEvent) => event.key === 'Escape')
      )
    ).pipe(
      takeUntil(this.onDestroy$)
    )
      .subscribe(() => {
        if (this.expandCategories && !this.categoriesOverflowButtonMouseover) {
          this.expandCategories = false;
        }
        if (this.expandSubmenu) {
          this.expandSubmenu = false;
        }
        this.changeDetectionRef.markForCheck();
      });

    // TODO UNTIL
    this.trialNotificationVisible$ = new ReplaySubject(1);
    this.store.pipe(
      takeUntil(this.onDestroy$),
      select(AuthSelectors.getAuthState),
      map((value) => {
        return false;
        // TODO: IMPLEMENT WHEN TRIAL IS NOT ENDLESS
      })
    ).subscribe(this.trialNotificationVisible$);

    this.store.pipe(
      takeUntil(this.onDestroy$),
      select(NewsSelector.getSortedCategories),
    ).subscribe((categories) => {
      this.categories = categories;
      this.recalculateOverflowingCategories();
      this.changeDetectionRef.markForCheck();
    });

    const activeSlug$ = merge(this.router.events, of(null))
      .pipe(
        map(() => this.activatedRoute.firstChild),
        filter((val) => val !== null),
        switchMap((val: ActivatedRoute) => val.params),
        map((params) => params.id),
        distinctUntilChanged(),
      );

    const activeCategory$ = activeSlug$.pipe(
      filter((value) => value !== undefined),
      switchMap((slug) => {
        return this.store.pipe(
          select(NewsSelector.getCategoryBySlug, {slug: slug}),
        );
      }),
      shareReplay(1),
      takeUntil(this.onDestroy$),
    );

    activeCategory$.subscribe((category) => {
      if (this.activeCategory && category && this.activeCategory.slug !== category.slug) {
        this.sortMode = false;
      }
      this.activeCategory = category;

      if (category !== null) {
        if (category.status === CategoryStatus.NOT_LOADED) {
          this.store.dispatch(new NewsActions.LoadCategoryItemsAction(
            category,
          ));
        }
      }

      this.changeDetectionRef.markForCheck();
    });

    merge(
      activeCategory$,
      activeSlug$
    ).pipe(
      filter((val) => val === undefined || val === null),
      switchMap(() => {
        return this.store.pipe(
          select(NewsSelector.getFirstCategory),
          take(1)
        );
      }),
      filter((cat) => cat !== null),
      map((cat) => cat!.slug)
    ).subscribe((slug) => {
      this.router.navigate(['/app', slug], {replaceUrl: true});
    });

    const columnCount$ = this.store.pipe(
      select(AuthSelectors.getAuthState),
      filter((value) => value.preferences !== null),
      map((value) => value.preferences!.columnCount)
    );

    this.store.pipe(
      takeUntil(this.onDestroy$),
      select(AuthSelectors.getAuthState),
      filter((value) => value.preferences !== null),
      map((value) => value.preferences!.hideEmptyFeeds),
    ).subscribe((hide) => {
      this.hideEmptyFeeds = hide;
      this.changeDetectionRef.markForCheck();
    });

    this.showAllRead$ = this.store.pipe(
      takeUntil(this.onDestroy$),
      select(AuthSelectors.getAuthState),
      filter((value) => value.preferences !== null),
      switchMap((value) => {
        if (!value.preferences!.hideEmptyFeeds) {
          return of(false);
        }

        return activeCategory$.pipe(
          filter((category) => category != null),
          switchMap((category) => {
            return this.store.pipe(
              select(NewsSelector.getFeedsForCategory, {category: category}),
              filter((feeds) => feeds !== null)
            );
          }),
          map((feeds: Feed[]) => {
            return !feeds.some((feed) => feed.source.error || (feed.items && feed.items.length > 0));
          })
        );
      }),
    );

    this.feedColumns$ = activeCategory$.pipe(
      filter((category) => category != null),
      switchMap((category) => {
        return this.store.pipe(
          select(NewsSelector.getFeedsForCategory, {category: category}),
          filter((feeds) => feeds !== null)
        );
      }),
      withLatestFrom(columnCount$, (feeds, columnCount) => {
        if (feeds.length === 0) {
          return [];
        }

        feeds.sort((a, b) => a.position - b.position);
        const columns = Array(columnCount).fill(0).map(() => <Feed[]>[]);

        feeds.forEach((feed) => {
          columns[Math.min(feed.column - 1, columns.length - 1)].push(feed);
        });

        return columns;
      })
    );
  }

  public ngAfterViewInit(): void {
    this.recalculateOverflowingCategories();
  }

  public ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  public logout() {
    this.authService.logout();
  }

  public getIndex(index) {
    return index;
  }

  public getId(index, value) {
    return value.id;
  }

  public onCategoryReadClick(category: Category, event: MouseEvent) {
    event.preventDefault();
    this.categoryService.markAsRead(category).subscribe();
  }

  public hideTrialMessage(): void {
    localStorage.setItem(HIDE_TRIAL_NOTIFICATION_TOKEN_KEY, 'true');
    this.trialNotificationVisible$.next(false);
  }

  @HostListener('window:resize')
  public onWindowResize(): void {
    this.recalculateOverflowingCategories();
  }

  @HostListener('window:keydown', ['$event'])
  public onKeydown(event: KeyboardEvent): void {
    if (event.ctrlKey || event.metaKey) {
      if (event.key === 'ArrowLeft') {
        const index = this.categories.findIndex((item) => item === this.activeCategory);
        let next: Category;
        if (index === 0) {
          next = this.categories[this.categories.length - 1];
        } else {
          next = this.categories[index - 1];
        }
        this.router.navigateByUrl('/app/' + next.slug);
        event.preventDefault();
      } else if (event.key === 'ArrowRight') {
        const index = this.categories.findIndex((item) => item === this.activeCategory);
        let next: Category;
        if (index === this.categories.length - 1) {
          next = this.categories[0];
        } else {
          next = this.categories[index + 1];
        }
        this.router.navigateByUrl('/app/' + next.slug);
        event.preventDefault();
      } else if (event.key === 'Enter') {
        if (event.shiftKey) {
          this.categoryService.markAllAsRead().subscribe();
        } else if (this.activeCategory !== null) {
          this.categoryService.markAsRead(this.activeCategory).subscribe();
        }
        event.preventDefault();
      }
    }
  }

  public toggleSubmenu() {
    this.expandSubmenu = !this.expandSubmenu;
    this.expandCategories = false;
  }

  public toggleCategories() {
    if (this.categoriesOverflowButtonMouseover) {
      return;
    }

    this.expandCategories = !this.expandCategories;
    this.expandSubmenu = false;
  }

  public onCategoriesMouseEnter() {
    if (isTouchDevice()) {
      return;
    }

    this.categoriesOverflowButtonMouseover = true;
    this.expandSubmenu = false;
    this.expandCategories = true;
  }

  public onCategoriesMouseLeave() {
    if (isTouchDevice()) {
      return;
    }

    this.expandSubmenu = false;
    this.categoriesOverflowButtonMouseover = false;
    this.expandCategories = false;
  }

  private recalculateOverflowingCategories(): void {
    setTimeout(() => {
      if (this.mobileLayout) {
        return;
      }

      const items = this.categoryListItemElement.map((item) => <HTMLElement>item.nativeElement);

      this.overflowCategories.length = 0;
      this.moveAddGroupElement = false;

      for (let i = 0; i < items.length; i++) {
        items[i].style.display = 'inline-flex';
        if (items[i].offsetTop === 50) {
          if (items[i] === this.addGroupElement.nativeElement) {
            this.moveAddGroupElement = true;
            break;
          }

          this.overflowCategories = this.categories.slice(i);

          for (let k = i; k < items.length; k++) {
            items[k].style.display = 'none';
          }
          break;
        }
      }

      this.changeDetectionRef.markForCheck();
    });
  }

  public onCategoryDrop(event: CdkDragDrop<Category[]>) {
    this.categoryService.moveCategory(
      this.categories[event.previousIndex],
      event.currentIndex - this.overflowCategories.length
    ).subscribe();
  }

  public onOverflowCategoryDrop(event: CdkDragDrop<Category[]>) {
    const item = this.overflowCategories[event.previousIndex];

    this.categoryService.moveCategory(
      item,
      this.categories.length - this.overflowCategories.length + event.currentIndex
    ).subscribe();
  }

  public onFeedDrop(column: number, event: CdkDragDrop<Feed[]>) {
    let toPosition = 0;

    if (event.container.data.length > 0) {
      if (event.container.data[event.currentIndex] != null) {
        toPosition = event.container.data[event.currentIndex].position;
      } else {
        toPosition = event.container.data[event.container.data.length - 1].position + 1;
      }
    }

    console.log(toPosition);

    this.feedService.moveFeed(
      event.previousContainer.data[event.previousIndex],
      column,
      toPosition
    ).subscribe();
  }
}
