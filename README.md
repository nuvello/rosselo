# ROSSELO

Rosselo is a tool to agregate all your sources in one application in a clear, concise way.

## Getting started

1. Install `docker` and `docker-compose` applications. Ensure they work properly.
2. Run `docker-compose up`. This starts the PostgreSQL database, the RabbitMQ queue, the backend server and the frontend behind a nginx proxy.
3. Open `http://localhost:4200` in your browser.

### Common operations

`docker-compose.yml` configures 4 services
- postgres: PostgreSQL database storing all user & feed data
- rabbitmq: RabbitMQ queue that stores tasks instructing the app to download particular feeds, send emails and other asynchronous operations
- backend: API server written in Kotlin + Spring Boot, which communicates with the database and queue and basically does all the work
- frontend: The Angular application that runs in your browser (i.e. what you see) that communicates with the backend.

**Some basic operations that you can do to operate with the services are the following:**
- To run the applications in the background, run `docker-compose up -d` (the **-d** parameter is important).
- To stop the application running in the background, run `docker-compose stop`. 
- To remove the containers that the application is running in, run `docker-compose down`. To also remove the database data & rabbitmq queue data, run it with `-v` parameter.
- If you removed the containers without the `-v` parmeter and wish to also remove the database/rabbitmq data, run `docker volume prune` (this removes all unused volumes, might affect other projects as well; alternatively you can delete `rosselodev_*` volumes manually using `docker volume rm rosselodev_*`). 
- To rebuild the application (after `git pull` or making changes), run `docker-compose build` (this only rebuilds the app) or `docker-compose up --build` (this also starts the app).

If you wish to only start/stop/rebuild only one of these services, most commands mentioned above allow specifying services to apply the command on, e.g. you can run `docker-compose up -d frontend` to start only the frontend.

## Architecture

There're currently two subprojects
- server (backend): API server written in Kotlin + Spring Boot
- web (frontend): Angular application served to browser communicating with the server

Details of these projects can be found inside their READMEs.