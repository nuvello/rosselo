create table account
(
	id uuid not null
		constraint account_pkey
			primary key,
	created_at timestamp not null,
	email varchar(255) not null,
	password_hash varchar(255) not null
);

create unique index account_email_uindex
	on account (email);

create table account_plan
(
	id uuid not null
		constraint account_plan_pkey
			primary key,
	type integer not null,
	valid_from timestamp not null,
	valid_to timestamp not null,
	account_id uuid not null
		constraint "account_plan_account_id_fk"
			references account
			on update cascade
			on delete cascade
);

create table category
(
	id uuid not null
		constraint category_pkey
			primary key,
	created_at timestamp not null,
	name varchar(255) not null,
	position integer not null,
	account_id uuid not null
		constraint "category_account_id_fk"
			references account
			on update cascade
			on delete cascade
);

create table limited_action
(
	type varchar(255) not null
		constraint limited_action_pkey
			primary key,
	max_per_day_per_ip integer,
	max_per_day_per_user integer,
	max_per_hour_per_ip integer,
	max_per_hour_per_user integer,
	max_per_week_per_ip integer,
	max_per_week_per_user integer
);

create table limited_action_log
(
	id bigserial not null
		constraint limited_action_log_pkey
			primary key,
	creation_datetime timestamp not null,
	ip_address varchar(255) not null,
	account_id uuid
		constraint "limited_action_log_account_id_fk"
			references account,
	limited_action_id varchar(255) not null
		constraint "limited_action_log_limited_action_id_fk"
			references limited_action
			on update cascade
			on delete cascade
);

create table password_reset_request
(
	id uuid not null
		constraint password_reset_request_pkey
			primary key,
	confirm_token varchar(255) not null,
	created_at timestamp not null,
	used_at timestamp,
	valid_until timestamp not null,
	account_id uuid not null
		constraint "password_reset_request_account_id_fk"
			references account
			on update cascade
			on delete cascade
);

create table preference
(
	type varchar(255) not null,
	value varchar(255) not null,
	account_id uuid not null
		constraint "preference_account_id_fk"
			references account
			on update cascade
			on delete cascade,
	constraint preference_pkey
		primary key (account_id, type)
);

create table source
(
	url varchar(255) not null
		constraint source_pkey
			primary key,
	created_at timestamp not null,
	description varchar(255),
	favicon bytea,
	favicon_last_fetch_at varchar(255),
	favicon_mime_type varchar(255),
	last_fetch_at timestamp,
	title varchar(255)
);

create table entry
(
	id uuid not null
		constraint entry_pkey
			primary key,
	created_at timestamp not null,
	identifier varchar(255) not null,
	link varchar(255) not null,
	title varchar(255) not null,
	source_id varchar(255)
		constraint "entry_source_id_fk"
			references source
			on update cascade
			on delete cascade
);

create table entry_account_meta
(
	meta integer not null,
	entry_id uuid not null
		constraint "entry_account_meta_entry_id_fk"
			references entry
			on update cascade
			on delete cascade,
	account_id uuid not null
		constraint "entry_account_meta_account_id_fk"
			references account
			on update cascade
			on delete cascade,
	constraint entry_account_meta_pkey
		primary key (account_id, entry_id)
);

create table feed
(
	id uuid not null
		constraint feed_pkey
			primary key,
	"column" integer not null,
	created_at timestamp not null,
	hide_entries_after_read boolean not null,
	max_amount_of_entries integer,
	name varchar(255) not null,
	no_older_than integer,
	position integer not null,
	account_id uuid not null
		constraint "feed_account_id_fk"
			references account
			on update cascade
			on delete cascade,
	category_id uuid not null
		constraint "feed_category_id_fk"
			references category
			on update cascade
			on delete cascade,
	source_id varchar(255) not null
		constraint "feed_source_id_fk"
			references source
			on update cascade
			on delete restrict
);

INSERT INTO limited_action (type, max_per_day_per_ip, max_per_day_per_user, max_per_hour_per_ip, max_per_hour_per_user, max_per_week_per_ip, max_per_week_per_user) VALUES ('NEW_ACCOUNT_CREATION', 30, null, 20, null, 50, null);
INSERT INTO limited_action (type, max_per_day_per_ip, max_per_day_per_user, max_per_hour_per_ip, max_per_hour_per_user, max_per_week_per_ip, max_per_week_per_user) VALUES ('UNSUCCESSFUL_LOGIN', 20, 20, 15, 15, 120, 50);
