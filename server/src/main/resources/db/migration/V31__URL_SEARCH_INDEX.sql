CREATE INDEX source_link_for_search_idx ON source(regexp_replace(link, '^(https?://)?(www\.)?', '') VARCHAR_PATTERN_OPS);

