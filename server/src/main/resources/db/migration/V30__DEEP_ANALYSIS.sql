alter table source
    add small_icon_hash nchar(64) default null;

alter table source
    add big_icon_hash nchar(64) default null;

alter table source
    add last_extra_scrap_at timestamp default null;

