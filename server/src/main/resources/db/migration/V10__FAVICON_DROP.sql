alter table source
  drop column favicon;

alter table source
  drop column favicon_last_fetch_at;

alter table source
  drop column favicon_mime_type;