alter table source add column in_queue_since timestamp default null;

update source set in_queue_since = CASE WHEN in_queue THEN now() ELSE null END WHERE TRUE;

alter table source drop column in_queue;

