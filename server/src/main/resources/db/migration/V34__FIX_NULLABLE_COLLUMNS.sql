alter table account alter column last_seen_at set not null;

alter table config alter column value set not null;

alter table entry alter column source_id set not null;

alter table source alter column title set not null;

alter table source alter column link set not null;

alter table source_download_error alter column created_at set not null;

alter table source_download_error alter column message set not null;

alter table source_download_error alter column type set not null;

alter table source_download_error alter column source_id set not null;

