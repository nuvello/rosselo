alter table source
  add last_successful_fetch_at timestamp;

alter table source
  add errors_since_last_successful int NOT NULL DEFAULT 0;