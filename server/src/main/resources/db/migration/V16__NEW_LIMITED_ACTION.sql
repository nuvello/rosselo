INSERT INTO limited_action (type, max_per_day_per_ip, max_per_day_per_user,
                            max_per_hour_per_ip, max_per_hour_per_user,
                            max_per_week_per_ip, max_per_week_per_user)
VALUES ('PASSWORD_RESET_REQUEST', 20, 6, 15, 5, 30, 10),
       ('PASSWORD_RESET', 20, 6, 15, 5, 30, 10);
