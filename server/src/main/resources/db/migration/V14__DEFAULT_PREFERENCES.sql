alter table config
       alter column id type varchar(255) using id::varchar(255);

INSERT INTO config (id, value)
VALUES ('TRIAL_PERIOD_IN_DAYS', '60'),
       ('PREMIUM_PRICE_YEAR_WITHOUT_VAT', '25'),
       ('DELETE_ACCOUNTS_INACTIVE_FOR_DAYS', '365'),
       ('SCRAPE_FEEDS_EVERY_SECONDS', '300'),
       ('MAX_NUMBER_OF_ENTRIES_PER_FEED', '100'),
       ('MAX_NUMBER_OF_FEEDS_PER_ACCOUNT', '250'),
       ('MAX_NUMBER_OF_CATEGORIES_PER_ACCOUNT', '20');
