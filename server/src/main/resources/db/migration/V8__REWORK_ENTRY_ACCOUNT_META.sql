alter table entry_account_meta
  drop column meta;

alter table entry_account_meta
  add read_at timestamp;

alter table entry_account_meta
  add pinned_at timestamp;

alter table entry_account_meta
  add unpinned_at timestamp;
