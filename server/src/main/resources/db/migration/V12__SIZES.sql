alter table source_download_error
  alter column message type varchar(2000) using message::varchar(2000);
alter table source
  alter column description type varchar(1000) using description::varchar(1000);
alter table entry
  alter column title type varchar(500) using title::varchar(500);
alter table entry
  alter column link type varchar(500) using link::varchar(500);
alter table entry
  alter column identifier type varchar(500) using identifier::varchar(500);

