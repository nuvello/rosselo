alter table account
  add last_seen_at timestamp default NOW();