alter table category
	add slug varchar(255) not null;

create unique index category_account_id_slug_uindex
	on category (account_id, slug);
