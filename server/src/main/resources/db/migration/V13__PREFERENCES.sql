alter table preference
  rename to account_preference;

create table config
(
  id    integer not null
    constraint config_pkey
      primary key,
  value varchar(255)
);

