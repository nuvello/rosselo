INSERT INTO limited_action (type, max_per_day_per_ip, max_per_day_per_user,
                            max_per_hour_per_ip, max_per_hour_per_user,
                            max_per_week_per_ip, max_per_week_per_user)
VALUES ('AUTH_TOKEN_REFRESH', 30, 15, 15, 10, 100, 40);
