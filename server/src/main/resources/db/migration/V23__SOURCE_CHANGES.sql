alter table source
    drop column next_fetch_at;

update source SET last_successful_fetch_at = NOW();

alter table source
    alter column last_successful_fetch_at set default now();

alter table source
    alter column last_successful_fetch_at set not null;


alter table source
    add subscribers int default 0 not null;

alter table source
    add entries_saved boolean default false not null;

alter table source
    add last_fetch_at timestamp default now() not null;

