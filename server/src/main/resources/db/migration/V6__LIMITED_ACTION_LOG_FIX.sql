alter table limited_action_log drop constraint limited_action_log_account_id_fk;

alter table limited_action_log
	add constraint limited_action_log_account_id_fk
		foreign key (account_id) references account
			on update cascade on delete set null;


