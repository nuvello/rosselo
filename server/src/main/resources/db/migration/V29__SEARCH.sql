alter table source
    add tld varchar(20);

UPDATE source
SET tld = substring(source FROM '^[^:]*://(?:[^/:]*:[^/@]*@)?(?:[^/:.]*\.)+([^:/]+)')
WHERE type = 'RSS_URL';

alter table source
    add language varchar(15);

alter table source
    add hidden_in_search boolean default false not null;

alter table source
    add tags varchar(15)[] default '{}' not null;

alter table source
    add topics varchar(100)[] default '{}' not null;

alter table source
    add categories varchar(50)[] default '{}' not null;

alter table source
    add subscribers2 integer;

alter table source
    add score int;

alter table source
    add number_of_new_entries_per_week int;

alter table source
    add total_subscribers int default 0 not null;

UPDATE source
SET total_subscribers = subscribers
WHERE TRUE;

CREATE OR REPLACE FUNCTION available_lang_or_simple(lang varchar)
    RETURNS varchar
AS
$BODY$

SELECT CASE
           WHEN lang IN (
                         'english', 'french', 'german', 'dutch', 'danish', 'finnish', 'hungarian', 'italian',
                         'norwegian', 'portuguese', 'romanian', 'russian',
                         'spanish', 'swedish', 'turkish'
               ) THEN lang
           ELSE 'simple'
           END;

$BODY$
    LANGUAGE sql
    IMMUTABLE;

CREATE OR REPLACE FUNCTION source_to_tsvector(lang varchar, title varchar, tags anyarray, categories anyarray,
                                              topics anyarray,
                                              description varchar)
    RETURNS tsvector
AS
$BODY$

SELECT setweight(to_tsvector(public.available_lang_or_simple(lang)::regconfig, public.unaccent(title)), 'A') ||
       setweight(to_tsvector(public.available_lang_or_simple(lang)::regconfig, public.unaccent(array_to_string(tags, ' '))), 'B') ||
       setweight(to_tsvector(public.available_lang_or_simple(lang)::regconfig, public.unaccent(array_to_string(categories, ' '))), 'B') ||
       setweight(to_tsvector(public.available_lang_or_simple(lang)::regconfig, public.unaccent(array_to_string(topics, ' '))), 'B') ||
       setweight(to_tsvector(public.available_lang_or_simple(lang)::regconfig, public.unaccent(coalesce(description, ''))), 'C')
$BODY$
    LANGUAGE sql
    IMMUTABLE;


-- todo numeric?
CREATE OR REPLACE FUNCTION rank_source(total_subscribers integer, score numeric)
    RETURNS numeric
AS
$BODY$

    -- todo values
    SELECT total_subscribers / 50000.0 * 0.3 + score / 5 * 0.1;

$BODY$
    LANGUAGE sql
    IMMUTABLE;

CREATE INDEX idx_source_tsvector_lang_specific ON source
    USING gin (source_to_tsvector(language, title, tags, categories, topics, description));

CREATE INDEX idx_source_tsvector_lang_basic ON source
    USING gin (source_to_tsvector('simple', title, tags, categories, topics, description));

