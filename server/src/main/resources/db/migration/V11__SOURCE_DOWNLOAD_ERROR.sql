alter table source
  rename column last_fetch_at to next_fetch_at;

create table source_download_error
(
  id         uuid not null
    constraint source_download_error_pkey
      primary key,
  created_at timestamp,
  message    varchar(255),
  type       varchar(255),
  source_id varchar(255)
    constraint source_download_error_source_id_fk
      references source
      on update cascade
      on delete cascade
);


