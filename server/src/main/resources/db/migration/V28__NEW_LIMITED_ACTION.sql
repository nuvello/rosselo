INSERT INTO limited_action (type, max_per_day_per_ip, max_per_day_per_user,
                            max_per_hour_per_ip, max_per_hour_per_user,
                            max_per_week_per_ip, max_per_week_per_user)
VALUES ('CREATE_SOURCE', 2000, 600, 400, 300, 5000, 700);
