alter table source rename column url to source;
alter table source alter column source type varchar(2048) using source::varchar(2048);
alter table source add id uuid default uuid_generate_v4();
alter table source add type varchar(100) default 'RSS_URL' not null;


alter table source_download_error drop constraint source_download_error_source_id_fk;

DELETE FROM source_download_error;
UPDATE source SET errors_since_last_successful = 0, last_successful_fetch_at = NULL, next_fetch_at = NULL;

alter table source_download_error alter column source_id type uuid using source_id::uuid;

alter table feed drop constraint feed_source_id_fk;
UPDATE feed SET source_id = (SELECT id FROM source WHERE source = source_id);
alter table feed alter column source_id type uuid using source_id::uuid;


alter table entry drop constraint entry_source_id_fk;
UPDATE entry SET source_id = (SELECT id FROM source WHERE source = source_id);
alter table entry alter column source_id type uuid using source_id::uuid;





alter table source drop constraint source_pkey;
alter table source add constraint source_pkey primary key (id);

create unique index source_source_type_uindex on source (source, type);


alter table feed
	add constraint feed_source_id_fk
		foreign key (source_id) references source
			on update cascade on delete restrict;
alter table entry
	add constraint entry_source_id_fk
		foreign key (source_id) references source
			on update cascade on delete cascade;

alter table source_download_error
	add constraint source_download_error_source_id_fk
		foreign key (source_id) references source
			on update cascade on delete cascade;
