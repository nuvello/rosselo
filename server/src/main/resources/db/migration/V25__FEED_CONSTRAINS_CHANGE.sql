alter table feed drop constraint feed_account_id_fk;

alter table feed
	add constraint feed_account_id_fk
		foreign key (account_id) references account
			on update cascade on delete restrict;

alter table feed drop constraint feed_category_id_fk;

alter table feed
	add constraint feed_category_id_fk
		foreign key (category_id) references category
			on update cascade on delete restrict;