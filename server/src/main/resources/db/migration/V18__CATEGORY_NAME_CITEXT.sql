CREATE EXTENSION IF NOT EXISTS citext;
alter table category
    alter column name type citext using name::citext;