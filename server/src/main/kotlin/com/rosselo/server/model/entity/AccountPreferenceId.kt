package com.rosselo.server.model.entity

import java.io.Serializable
import javax.persistence.*

@Embeddable
data class AccountPreferenceId(
        @Column(name = "type")
        @Enumerated(EnumType.STRING)
        val type: AccountPreferenceType,
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account
) : Serializable {
    override fun equals(other: Any?) =
            other is AccountPreferenceId && other.type == type && account == other.account

    override fun hashCode(): Int {
        return type.hashCode() * 31 + account.hashCode()
    }
}