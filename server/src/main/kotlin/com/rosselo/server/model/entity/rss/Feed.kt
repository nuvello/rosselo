package com.rosselo.server.model.entity.rss

import com.rosselo.server.model.entity.Account
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * Each [Account] has list of [Category], which have list of [Feed]. Feed is a
 * unique customization (set of preferences) of one [Source] specific for the
 * [Account].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "feed")
data class Feed(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account,
        @ManyToOne
        @JoinColumn(name = "category_id")
        var category: Category,
        @Column(name = "name")
        var name: String,
        @ManyToOne
        @JoinColumn(name = "source_id")
        var source: Source,
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        /**
         * Each category is split up into several columns. This identifies the
         * column this feed belongs to.
         */
        @Column(name = "\"column\"")
        var column: Int,
        /**
         * Position in account's category's column numbered from 0.
         */
        @Column(name = "position")
        var position: Int,
        /**
         * Should the read entries be hidden?
         */
        @Column(name = "hide_entries_after_read")
        var hideEntriesAfterRead: Boolean,
        /**
         * Do not show more than x entries.
         */
        @Column(name = "max_amount_of_entries")
        var maxAmountOfEntries: Int?,
        /**
         * Do not show entries created more x days before.
         */
        @Column(name = "no_older_than")
        var noOlderThan: Int?
) {
    override fun equals(other: Any?) =
            other is Feed && id == other.id

    override fun hashCode() = id.hashCode()
}