package com.rosselo.server.model.entity

import com.rosselo.server.service.LimitedActionService
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 * Log of a [LimitedAction] happening. Used by [LimitedActionService] to know when a certain ip address
 * or user has gone above limit of performing such an action.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "limited_action_log")
data class LimitedActionLog(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        val id: Long? = null,
        @ManyToOne
        @JoinColumn(name = "limited_action_id")
        val limitedAction: LimitedAction,

        /**
         * The user for which the action has been performed.
         */
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account? = null,
        @Column(name = "creation_datetime", nullable = false)
        @NotNull
        val creationDateTime: LocalDateTime? = null,
        @Column(name = "ip_address", nullable = false)
        @NotNull
        val ipAddress: String
) {
    override fun equals(other: Any?) =
            other is LimitedActionLog && other.id == id

    override fun hashCode() = id.hashCode()
}

