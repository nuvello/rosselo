package com.rosselo.server.model.entity

import javax.persistence.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "config")
data class Config(
        @Id
        @Column(name = "id")
        @Enumerated(EnumType.STRING)
        val id: ConfigType,
        @Column(name = "value")
        var value: String
) {
    override fun equals(other: Any?) =
            other is Config && other.id == id

    override fun hashCode(): Int {
        return id.hashCode()
    }
}