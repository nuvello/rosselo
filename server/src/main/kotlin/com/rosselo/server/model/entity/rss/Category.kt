package com.rosselo.server.model.entity.rss

import com.rosselo.server.model.entity.Account
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import javax.persistence.Column

/**
 * Represents a named category for related feeds of one account.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "category")
data class Category(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account,
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        @Column(name = "name")
        var name: String,
        @Column(name = "slug")
        var slug: String,
        /**
         * Position in account's categories numbered from 0. (Counting starts
         * from zero for every account.)
         */
        @Column(name = "position")
        var position: Int
) {
    override fun equals(other: Any?) = other is Category && other.id == id

    override fun hashCode() = id.hashCode()
}