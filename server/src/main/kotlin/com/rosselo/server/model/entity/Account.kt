package com.rosselo.server.model.entity

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

/**
 * Represent one user account.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "account")
data class Account(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        @Column(name = "last_seen_at")
        var lastSeenAt: LocalDateTime,
        @Column(name = "email")
        var email: String,
        @Column(name = "password_hash")
        var passwordHash: String
) : UserDetails {
    override fun getAuthorities(): MutableCollection<out GrantedAuthority> =
            Collections.singleton(SimpleGrantedAuthority("USER"))

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = email

    override fun isCredentialsNonExpired(): Boolean = true

    override fun getPassword(): String = passwordHash

    override fun isAccountNonExpired(): Boolean = true

    override fun isAccountNonLocked(): Boolean = true

    override fun equals(other: Any?) = other is Account && other.id == id

    override fun hashCode() = id.hashCode()
}