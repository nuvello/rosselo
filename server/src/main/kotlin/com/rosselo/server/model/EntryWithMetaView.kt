package com.rosselo.server.model

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Entry
import com.rosselo.server.model.entity.rss.EntryAccountMeta

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class EntryWithMetaView(
        val entry: Entry,
        val meta: EntryAccountMeta?
)