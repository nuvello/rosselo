package com.rosselo.server.model.entity

import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "account_preference")
data class AccountPreference(
        @EmbeddedId
        val id: AccountPreferenceId,
        @Column(name = "value")
        var value: String
) {
    override fun equals(other: Any?) =
            other is AccountPreference && other.id == id

    override fun hashCode(): Int {
        return id.hashCode()
    }
}