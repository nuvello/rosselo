package com.rosselo.server.model.entity

import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * Represent a time period of account's subscription to a plan (be it free / trial
 * or premium).
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "account_plan")
data class AccountPlan(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account,
        @Column(name = "type")
        val type: AccountPlanType,
        @Column(name = "valid_from")
        val validFrom: LocalDateTime,
        @Column(name = "valid_to")
        var validTo: LocalDateTime
) {
    override fun equals(other: Any?) = other is AccountPlan && other.id == id

    override fun hashCode() = id.hashCode()
}