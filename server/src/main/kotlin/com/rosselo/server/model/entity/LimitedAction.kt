package com.rosselo.server.model.entity

import com.rosselo.server.service.LimitedActionService
import javax.persistence.*
import javax.validation.constraints.NotNull

/**
 * Represent metadata for actions who should be limited to execute above some defined limit. To use it, see
 * [LimitedActionService].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "limited_action")
data class LimitedAction(
        @Id
        @Column(name = "type", nullable = false)
        @NotNull
        @Enumerated(EnumType.STRING)
        val type: LimitedActionType,

        @Column(name = "max_per_hour_per_ip")
        var maxPerHourPerIp: Int? = null,
        @Column(name = "max_per_day_per_ip")
        var maxPerDayPerIp: Int? = null,
        @Column(name = "max_per_week_per_ip")
        var maxPerWeekPerIp: Int? = null,

        @Column(name = "max_per_hour_per_user")
        var maxPerHourPerUser: Int? = null,
        @Column(name = "max_per_day_per_user")
        var maxPerDayPerUser: Int? = null,
        @Column(name = "max_per_week_per_user")
        var maxPerWeekPerUser: Int? = null
) {
    override fun equals(other: Any?) = other is LimitedAction && other
            .type == type

    override fun hashCode() = type.hashCode()
}
