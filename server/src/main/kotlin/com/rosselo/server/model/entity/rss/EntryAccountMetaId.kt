package com.rosselo.server.model.entity.rss

import com.rosselo.server.model.entity.Account
import java.io.Serializable
import javax.persistence.Embeddable
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Embeddable
data class EntryAccountMetaId(
        @ManyToOne
        @JoinColumn(name = "entry_id")
        val entry: Entry,
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account
) : Serializable {
    override fun equals(other: Any?) =
            other is EntryAccountMetaId && entry == other.entry && account ==
                    other.account

    override fun hashCode() = 38 * account.hashCode() + entry.hashCode()
}