package com.rosselo.server.model.entity.rss

import com.vladmihalcea.hibernate.type.array.StringArrayType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

/**
 * Represent one [Entry] source.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Suppress("JpaAttributeTypeInspection")
@TypeDefs(
        TypeDef(name = "string-array", typeClass = StringArrayType::class)
)
@Entity
@Table(name = "source")
data class Source(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),

        @Column(name = "type")
        @Enumerated(EnumType.STRING)
        val type: SourceType,
        @Column(name = "source")
        var source: String,

        @Column(name = "title")
        var title: String,
        @Column(name = "description")
        var description: String?,
        @Column(name = "link")
        var link: String, // lowercase
        @Column(name = "created_at")
        val createdAt: LocalDateTime,

        @Column(name = "small_icon_hash")
        var smallIconHash: String?,
        @Column(name = "big_icon_hash")
        var bigIconHash: String?,
        @Column(name = "last_extra_scrap_at")
        var lastExtraScrapAt: LocalDateTime?,

        @Column(name = "tld")
        var tld: String?,
        @Column(name = "language")
        var language: String?,
        @Column(name = "hidden_in_search")
        var hiddenInSearch: Boolean,
        @Type( type = "string-array" )
        @Column(name = "tags")
        var tags: Array<String>,
        @Type( type = "string-array" )
        @Column(name = "topics")
        var topics: Array<String>,
        @Type( type = "string-array" )
        @Column(name = "categories")
        var categories: Array<String>,
        @Column(name = "subscribers2")
        var subscribers2: Int?,
        @Column(name = "score")
        var score: Int?,
        @Column(name = "number_of_new_entries_per_week")
        var numberOfNewEntriesPerWeek: Int?,

        @Column(name = "total_subscribers")
        var totalSubscribers: Int, // todo update in a transaction
        @Column(name = "subscribers")
        var subscribers: Int,
        @Column(name = "entries_saved")
        var entriesSaved: Boolean,
        @Column(name = "last_fetch_at")
        var lastFetchAt: LocalDateTime,
        @Column(name = "last_successful_fetch_at")
        var lastSuccessfulFetchAt: LocalDateTime,
        @Column(name = "in_queue_since")
        var inQueueSince: LocalDateTime? = null,
        @Column(name = "errors_since_last_successful")
        var errorsSinceLastSuccessful: Int,

        @OneToMany(mappedBy = "source", cascade = [CascadeType.ALL], orphanRemoval = true)
        var downloadErrors: MutableSet<SourceDownloadError> = HashSet(),
        @OneToMany(mappedBy = "source", cascade = [CascadeType.ALL], orphanRemoval = true)
        var feeds: Set<Feed> = HashSet()
) {
    override fun equals(other: Any?) = other is Source && other.id == id

    override fun hashCode() = id.hashCode()
}