package com.rosselo.server.model.entity.rss

import com.rosselo.server.model.entity.Account
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.EmbeddedId
import javax.persistence.Entity
import javax.persistence.Table

/**
 * Every [Account] has unique set of information about every [Entry], which are
 * represented by this class.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "entry_account_meta")
data class EntryAccountMeta(
        @EmbeddedId
        val id: EntryAccountMetaId,
        @Column(name = "read_at")
        var readAt: LocalDateTime?,
        @Column(name = "pinned_at")
        var pinnedAt: LocalDateTime?,
        @Column(name = "unpinned_at")
        var unpinnedAt: LocalDateTime?
) {
    override fun equals(other: Any?) =
            other is EntryAccountMeta && id == other.id

    override fun hashCode() = id.hashCode()
}