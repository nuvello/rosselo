package com.rosselo.server.model.entity

enum class AccountPreferenceType(val defaultValue: String) {
    GLOBAL_COLUMN_COUNT("3"),
    GLOBAL_HIDE_EMPTY_FEEDS("false"),
    EMAIL_SERVICE_UPDATES("true"),
    EMAIL_TIPS_FREE_STUFF("false"),
    HIDE_ENTRIES_AFTER_READ(HideEntriesAfterReadPreference.RESPECT_FEED_SETTINGS.name),

}