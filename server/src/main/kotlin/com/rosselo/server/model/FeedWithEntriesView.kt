package com.rosselo.server.model

import com.rosselo.server.model.entity.rss.Feed

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class FeedWithEntriesView(
        val feed: Feed,
        val entries: List<EntryWithMetaView>
)