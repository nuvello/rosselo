package com.rosselo.server.model.entity.rss

import com.fasterxml.jackson.annotation.JsonIgnore
import org.hibernate.annotations.Type
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

/**
 * Represent one article of a [Source].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "Entry")
data class Entry(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        @Column(name = "published_at")
        val publishedAt: LocalDateTime?,
        /**
         * Unique identifier.
         */
        @Column(name = "identifier")
        var identifier: String,
        @Column(name = "title")
        var title: String,
        @Column(name = "link")
        var link: String,
        @ManyToOne
        @JoinColumn(name = "source_id")
        var source: Source,
        @OneToMany(mappedBy = "id.entry", cascade = [CascadeType.ALL], orphanRemoval = true)
        var  accountMetas: Set<EntryAccountMeta> = HashSet()
) {
    override fun equals(other: Any?) =
            other is Entry && id == other.id

    override fun hashCode() = id.hashCode()
}