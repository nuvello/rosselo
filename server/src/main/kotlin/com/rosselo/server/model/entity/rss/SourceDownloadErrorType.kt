package com.rosselo.server.model.entity.rss

enum class SourceDownloadErrorType {
    SECURITY,
    URI,
    TIMEOUT,
    IO,
    CANNOT_PARSE,
    OTHER
}

