package com.rosselo.server.model.entity

enum class AccountPlanType {
    EXPIRED,
    TRIAL,
    PREMIUM
}

