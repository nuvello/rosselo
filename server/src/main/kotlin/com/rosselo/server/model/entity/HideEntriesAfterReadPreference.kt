package com.rosselo.server.model.entity

enum class HideEntriesAfterReadPreference {
    TRUE,
    FALSE,
    RESPECT_FEED_SETTINGS
}