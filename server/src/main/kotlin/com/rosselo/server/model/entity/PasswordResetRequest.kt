package com.rosselo.server.model.entity

import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "password_reset_request")
data class PasswordResetRequest(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @ManyToOne
        @JoinColumn(name = "account_id")
        val account: Account,
        @Column(name = "confirm_token_hash")
        val confirmTokenHash: String,
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        @Column(name = "valid_until")
        val validUntil: LocalDateTime,
        @Column(name = "used_at")
        var usedAt: LocalDateTime? = null
) {
    override fun equals(other: Any?) = other is PasswordResetRequest && other.id == id

    override fun hashCode() = id.hashCode()
}