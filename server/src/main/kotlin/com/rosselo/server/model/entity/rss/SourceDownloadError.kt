package com.rosselo.server.model.entity.rss

import java.time.LocalDateTime
import java.util.*
import javax.persistence.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Entity
@Table(name = "source_download_error")
data class SourceDownloadError(
        @Id
        @Column(name = "id")
        val id: UUID = UUID.randomUUID(),
        @ManyToOne
        @JoinColumn(name = "source_id")
        val source: Source,
        @Column(name = "created_at")
        val createdAt: LocalDateTime,
        @Column(name = "message")
        val message: String,
        @Enumerated(EnumType.STRING)
        @Column(name = "type")
        val type: SourceDownloadErrorType
) {
    override fun equals(other: Any?) = other is SourceDownloadError && other.id == id

    override fun hashCode() = id.hashCode()
}