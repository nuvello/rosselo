package com.rosselo.server.model.entity.rss

enum class SourceType {
    RSS_URL,
    TWITTER_ACCOUNT_NAME,
    YOUTUBE_CHANNEL_ID,
    YOUTUBE_PLAYLIST_ID
}