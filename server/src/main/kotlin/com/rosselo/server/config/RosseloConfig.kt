package com.rosselo.server.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * Rosselo specific configuration options.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Configuration
@ConfigurationProperties(prefix = "rosselo")
class RosseloConfig {
    // todo byte array?

    /**
     * Is production environment active?
     */
    var production = false
    lateinit var domain: String
    lateinit var email: String
    lateinit var sendgridApiKey: String
    lateinit var youtubeApiKey: String
    lateinit var pepper: String

    val jwt = Jwt()
    val worker = Worker()
    val s3 = S3()

    class Jwt {
        lateinit var secret: String
    }

    class S3 {
        lateinit var accessKey: String
        lateinit var secretKey: String
        lateinit var endpoint: String

        val buckets = Buckets()

        class Buckets {
            lateinit var sourceIcons: String
        }
    }

    class Worker {
        var mail = Mail()
        var scrapForApi = ScrapForApi()
        var scrapRegularlyUnusedSources = ScrapRegularlyUnusedSources()
        var scrapRegularlyUsedSources = ScrapRegularlyUsedSources()
        var scheduleScrapTasks = ScheduleScrapTasks()

        class Mail {
            var enabled = false
            var concurrent = 8
        }

        class ScrapForApi {
            var enabled = false
            var concurrent = 8
        }

        class ScrapRegularlyUsedSources {
            var enabled = false
            var concurrent = 8
        }

        class ScrapRegularlyUnusedSources {
            var enabled = false
            var concurrent = 8
        }

        class ScheduleScrapTasks {
            var enabled = false
        }
    }
}
