package com.rosselo.server.config

import com.fasterxml.jackson.databind.SerializationFeature
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
/**
 * MvcConfig used to override default configuration. Currently only modifying
 * the JSON date time format.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class MvcConfig @Autowired constructor(
    private val rosseloConfig: RosseloConfig
) : WebMvcConfigurer {
    override fun extendMessageConverters(converters: MutableList<HttpMessageConverter<*>>) {
        for (converter in converters) {
            if (converter is MappingJackson2HttpMessageConverter) {
                val objectMapper = converter.objectMapper
                // disable writing local date time as timestamps
                objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            }
        }
    }

    override fun addCorsMappings(registry: CorsRegistry) {
        if (!rosseloConfig.production) {
            // we want the REST API to work even from different URLs
            // than the one the server operates on
            registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(-1)
        }
    }
}
