package com.rosselo.server.config

import com.fasterxml.jackson.databind.ObjectMapper
import com.rosselo.server.repository.AccountRepository
import com.rosselo.server.rest.ExceptionHandler
import com.rosselo.server.rest.filter.CorsFilter
import com.rosselo.server.rest.filter.JWTAuthenticationFilter
import com.rosselo.server.rest.filter.JWTAuthorizationFilter
import com.rosselo.server.service.AccountService
import com.rosselo.server.service.LimitedActionService
import com.rosselo.server.utils.JwtTokenGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.access.channel.ChannelProcessingFilter
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


/**
 * Security configuration of the application. Takes care of protecting routes (
 * authentication / authorization), CORS, configures password encoder and other
 * security related parts of the application.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Configuration
@EnableWebSecurity
class SecurityConfig @Autowired constructor(
        private val accountRepository: AccountRepository,
        private val accountService: AccountService,
        private val limitedActionService: LimitedActionService,
        private val rosseloConfig: RosseloConfig,
        private val passwordEncoder: PasswordEncoder,
        private val objectMapper: ObjectMapper,
        private val jwtTokenGenerator: JwtTokenGenerator
) : WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder) {
        // use our service and our encoder
        auth.userDetailsService<UserDetailsService>(accountService)
                .passwordEncoder(passwordEncoder)
    }

    override fun configure(http: HttpSecurity) {
        // every request must be authenticated
        http
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                // register
                .antMatchers(HttpMethod.POST, "/accounts").permitAll()
                .antMatchers(HttpMethod.POST, "/accounts/request-password-reset").permitAll()
                .antMatchers(HttpMethod.POST, "/accounts/password-reset").permitAll()
                .antMatchers(HttpMethod.GET, "/configs").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(ExceptionHandler(objectMapper))
                .and()
                .addFilterBefore(createCorsFilter(), ChannelProcessingFilter::class.java)
                .addFilterBefore(createJWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
                .addFilterBefore(createJWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter::class.java)
                .csrf().disable()
    }

    private fun createCorsFilter(): CorsFilter {
        return CorsFilter(rosseloConfig)
    }

    private fun createJWTAuthenticationFilter(): JWTAuthenticationFilter {
        val filter = JWTAuthenticationFilter(
                limitedActionService,
                accountRepository,
                rosseloConfig,
                jwtTokenGenerator
        )

        filter.setAuthenticationManager(authenticationManager())

        return filter
    }

    private fun createJWTAuthorizationFilter(): JWTAuthorizationFilter {
        return JWTAuthorizationFilter(authenticationManager(), accountRepository, rosseloConfig.jwt.secret)
    }
}