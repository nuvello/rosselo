package com.rosselo.server.config

import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.auth.policy.Policy
import com.amazonaws.auth.policy.Principal
import com.amazonaws.auth.policy.Resource
import com.amazonaws.auth.policy.Statement
import com.amazonaws.auth.policy.actions.S3Actions
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.rosselo.server.utils.catchAndIgnore
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class S3Config {
    @Bean
    fun amazonS3(rosseloConfig: RosseloConfig): AmazonS3 {
        val credentials = BasicAWSCredentials(rosseloConfig.s3.accessKey, rosseloConfig.s3.secretKey)
        val clientConfiguration = ClientConfiguration()
        clientConfiguration.signerOverride = "AWSS3V4SignerType"

        // todo check s3 config
        val client = AmazonS3ClientBuilder
                .standard()
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(rosseloConfig.s3.endpoint, Regions.US_EAST_1.name))
                .withPathStyleAccessEnabled(true)
                .withClientConfiguration(clientConfiguration)
                .withCredentials(AWSStaticCredentialsProvider(credentials))
                .build()

        if (!rosseloConfig.production) {
            // create if not already created
            catchAndIgnore {
                val buckets = listOf(
                        rosseloConfig.s3.buckets.sourceIcons
                )
                buckets.forEach {
                    client.createBucket(it)
                    val statement = Statement(Statement.Effect.Allow)
                            .withActions(S3Actions.GetObject)
                            .withPrincipals(Principal.All)
                            .withResources(Resource("arn:aws:s3:::$it/*"))
                    client.setBucketPolicy(it, Policy().withStatements(statement).toJson())
                }
            }
        }

        return client
    }
}