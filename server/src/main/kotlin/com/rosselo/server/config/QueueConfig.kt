package com.rosselo.server.config

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration


@Configuration
class QueueConfig {
    companion object {
        const val mailTaskQueueName = "mail_task_q"

        const val apiScrapTaskQueueName = "api_scrap_task_q"
        const val usedSourceScrapTaskQueueName = "used_source_scrap_task_q"
        const val unusedSourceScrapTaskQueueName = "unused_source_scrap_task_q"
    }

    @Bean
    fun mailTaskQueue(): Queue {
        return Queue(mailTaskQueueName, true, false, false);
    }

    @Bean
    fun apiScrapTaskQueue(): Queue {
        return Queue(apiScrapTaskQueueName, false, false, false);
    }

    @Bean
    fun usedSourceScrapTaskQueueName(): Queue {
        return Queue(usedSourceScrapTaskQueueName, true, false, false);
    }

    @Bean
    fun unusedSourceScrapTaskQueueName(): Queue {
        return Queue(unusedSourceScrapTaskQueueName, true, false, false);
    }
}