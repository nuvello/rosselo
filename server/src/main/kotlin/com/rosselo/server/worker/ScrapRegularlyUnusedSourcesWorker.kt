package com.rosselo.server.worker

import com.rosselo.server.config.QueueConfig
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.service.rss.scrapper.ScrapTask
import com.rosselo.server.service.rss.scrapper.ScrapTaskSourceType
import com.rosselo.server.service.rss.scrapper.ScrapperService
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import org.springframework.transaction.support.TransactionTemplate
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@Component
@ConditionalOnProperty("rosselo.worker.scrap-regularly-unused-sources.enabled", havingValue = "true")
class ScrapRegularlyUnusedSourcesWorker @Autowired constructor(
        private val scrapperServiceService: ScrapperService,
        private val sourceRepository: SourceRepository,
        private val transactionTemplate: TransactionTemplate
) {
    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @RabbitListener(
            queues = [QueueConfig.unusedSourceScrapTaskQueueName],
            concurrency = "\${rosselo.worker.scrap-regularly-unused-sources.concurrent}"
    )
    @Transactional
    fun doTask(task: ScrapTask) {
        // also in ScrapRegularlyUsedSourcesWorker
        @Suppress("DuplicatedCode")
        try {
            logger.info("Executing scrap task for unused source {}", task)
            transactionTemplate.execute {
                val scrap = scrapperServiceService.scrap(task)

                logger.info("Scrap task result {}", scrap)
                if (scrap.data != null) {
                    sourceRepository.findById(scrap.data.sourceId)
                            .ifPresent {
                                it.inQueueSince = null
                                sourceRepository.save(it)
                            }
                } else if (task.sourceType == ScrapTaskSourceType.SOURCE_ID) {
                    sourceRepository.findById(UUID.fromString(task.source))
                            .ifPresent {
                                it.inQueueSince = null
                                sourceRepository.save(it)
                            }
                }
            }
        } catch (e: Exception) {
            logger.error("Unable to do scrap", e)
            return
        }
    }
}