package com.rosselo.server.worker

import com.rosselo.server.config.QueueConfig
import com.rosselo.server.service.rss.scrapper.ScrapResult
import com.rosselo.server.service.rss.scrapper.ScrapResultStatus
import com.rosselo.server.service.rss.scrapper.ScrapTask
import com.rosselo.server.service.rss.scrapper.ScrapperService
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
@ConditionalOnProperty("rosselo.worker.scrap-for-api.enabled", havingValue = "true")
class ScrapForApiWorker @Autowired constructor(
        private val scrapperServiceService: ScrapperService
) {
    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @RabbitListener(
            queues = [QueueConfig.apiScrapTaskQueueName],
            concurrency = "\${rosselo.worker.scrap-for-api.concurrent}"
    )
    @Transactional
    fun doTask(task: ScrapTask): ScrapResult {
        return try {
            logger.info("Executing API scrap task {}", task)
            // todo timeouts
            val result = scrapperServiceService.scrap(task)
            logger.info("API scrap task result {}", result)

            return result
        } catch (e: Exception) {
            ScrapResult(ScrapResultStatus.ERROR, null)
        }
    }
}