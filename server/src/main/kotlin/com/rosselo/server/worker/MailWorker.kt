package com.rosselo.server.worker

import com.rosselo.server.config.QueueConfig
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@ConditionalOnProperty("rosselo.worker.mail.enabled", havingValue = "true")
class MailWorker {
    @RabbitListener(
            queues = [QueueConfig.mailTaskQueueName],
            concurrency = "\${rosselo.worker.mail.concurrent}"
    )
    fun doStuff(message: String) {
        // todo implementation
    }
}