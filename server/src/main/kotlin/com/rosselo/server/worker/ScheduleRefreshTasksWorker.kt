package com.rosselo.server.worker

import com.rosselo.server.config.QueueConfig
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.service.rss.scrapper.ScrapTask
import com.rosselo.server.service.rss.scrapper.ScrapTaskSourceType
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.support.TransactionTemplate
import java.time.LocalDateTime

/**
 * Started in [com.rosselo.server.Application]
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class ScheduleRefreshTasksWorker @Autowired constructor(
        private val sourceRepository: SourceRepository,
        private val rabbitTemplate: RabbitTemplate,
        private val transactionTemplate: TransactionTemplate
) {
    companion object {
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    @Async
    fun start() {
        while (true) {
            // todo: this probably doesn't work
            this.sourceRepository.findToBeRefreshedNotInQueue()
                    .forEach { source ->
                        logger.info("Scheduling ${source.id} ${source.type}/(${source.source}) for scrap")

                        transactionTemplate.execute {
                            source.inQueueSince = LocalDateTime.now()
                            sourceRepository.save(source)
                        }
                        try {
                            val usedSource = source.subscribers > 0
                            rabbitTemplate.convertAndSend(
                                    if (usedSource) QueueConfig.usedSourceScrapTaskQueueName else QueueConfig.unusedSourceScrapTaskQueueName,
                                    ScrapTask(
                                            ScrapTaskSourceType.SOURCE_ID,
                                            source.id.toString(),
                                            storeItems = usedSource,
                                            returnItems = false,
                                            quick = false
                                    )
                            )
                        } catch (e: Exception) {
                            transactionTemplate.execute {
                                source.inQueueSince = null
                                sourceRepository.save(source)
                            }
                            logger.error("Unable to schedule scrap task", e)
                        }
                    }

            try {
                Thread.sleep(3000)
            } catch (e: InterruptedException) {
                break
            }
        }
    }
}
