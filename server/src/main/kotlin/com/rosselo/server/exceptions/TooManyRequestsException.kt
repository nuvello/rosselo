package com.rosselo.server.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception to be thrown when the user executed the same action too many items and should not be allowed to execute it
 * one more time.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class TooManyRequestsException : RuntimeException()
