package com.rosselo.server.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception to be thrown when the user does not have access to the given content.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class ForbiddenException : RuntimeException()
