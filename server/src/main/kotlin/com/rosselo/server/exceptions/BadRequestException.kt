package com.rosselo.server.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception to be thrown when the request is in a bad form or has errors.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class BadRequestException : RuntimeException()
