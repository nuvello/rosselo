package com.rosselo.server.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception to be thrown when the requested part does not exist.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class NotFoundException : Exception()
