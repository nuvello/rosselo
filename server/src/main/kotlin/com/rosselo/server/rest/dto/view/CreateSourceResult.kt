package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.service.rss.scrapper.ScrapResultItem
import java.util.*

data class CreateSourceResult(
        val sourceId: UUID,
        val sourceName: String,
        val sourceData: String,
        val sourceType: SourceType
)