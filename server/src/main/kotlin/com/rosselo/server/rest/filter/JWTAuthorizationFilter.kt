package com.rosselo.server.rest.filter

import com.rosselo.server.repository.AccountRepository
import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import org.apache.catalina.connector.ClientAbortException
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter
import java.io.IOException
import java.time.LocalDateTime
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Filter that performs authorization based on JWT secret located in request's header.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class JWTAuthorizationFilter(
        authManager: AuthenticationManager,
        private val accountRepository: AccountRepository,
        private val secret: String
) : BasicAuthenticationFilter(authManager) {

    @Throws(IOException::class, ServletException::class)
    override fun doFilterInternal(req: HttpServletRequest,
                                  res: HttpServletResponse,
                                  chain: FilterChain) {
        val header = req.getHeader(JWTAuthenticationFilter.HEADER_STRING)
        if (header == null || !header.startsWith(JWTAuthenticationFilter.TOKEN_PREFIX)) {
            chain.doFilter(req, res)
            return
        }
        val authentication = getAuthentication(req)
        if (authentication != null) {
            SecurityContextHolder.getContext().authentication = authentication
        }
        try {
            chain.doFilter(req, res)
        } catch (e: ClientAbortException) {
            // ignore
        }
    }

    private fun getAuthentication(request: HttpServletRequest): UsernamePasswordAuthenticationToken? {
        val token = request.getHeader(JWTAuthenticationFilter.HEADER_STRING)
        try {
            if (token != null) {
                // parse the token.
                val id = Jwts.parser()
                        .setSigningKey(this.secret)
                        .parseClaimsJws(token.replace(JWTAuthenticationFilter.TOKEN_PREFIX, ""))
                        .body
                        .subject
                if (id != null) {
                    val user = accountRepository.findById(UUID.fromString(id))
                            .orElse(null)
                            ?: return null

                    user.lastSeenAt = LocalDateTime.now()
                    accountRepository.saveAndFlush(user)

                    return UsernamePasswordAuthenticationToken(user, null, user.authorities)
                }
                return null
            }
            return null
        } catch (e: JwtException) {
            return null
        }
    }
}
