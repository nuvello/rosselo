package com.rosselo.server.rest.controller

import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.TooManyRequestsException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.LimitedActionType
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.rest.dto.data.CreateSourceData
import com.rosselo.server.rest.dto.data.CreateSourceType
import com.rosselo.server.rest.dto.view.CreateSourceResult
import com.rosselo.server.rest.dto.view.SourcePreview
import com.rosselo.server.rest.dto.view.EntryView
import com.rosselo.server.rest.dto.view.SourceSearchView
import com.rosselo.server.rest.mapper.ViewMapper
import com.rosselo.server.service.LimitedActionService
import com.rosselo.server.service.SourceIconStorage
import com.rosselo.server.service.rss.SourceService
import com.rosselo.server.service.rss.scrapper.ScrapResultStatus
import com.rosselo.server.service.rss.scrapper.ScrapTask
import com.rosselo.server.service.rss.scrapper.ScrapTaskSourceType
import com.rosselo.server.service.rss.scrapper.ScrapperService
import com.sun.istack.NotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.InvalidDataAccessApiUsageException
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@RestController
@RequestMapping("sources")
class SourceResource @Autowired constructor(
        private val scrapperService: ScrapperService,
        private val limitedActionService: LimitedActionService,
        private val sourceService: SourceService,
        private val feedRepository: FeedRepository,
        private val sourceIconStorage: SourceIconStorage
) {
    @RequestMapping(method = [RequestMethod.GET])
    fun search(
            @RequestParam(name = "query", required = true) query: String,
            @RequestParam(name = "tlds", required = false) tlds: List<String>?,
            @RequestParam(name = "languages", required = false) languages: List<String>?,
            @RequestParam(name = "page", required = false, defaultValue = "1") page: Int,
            @AuthenticationPrincipal account: Account
    ): List<SourceSearchView> {
        try {
            // todo move regex
            val search = if (Regex("^(https?://(?:www\\.|(?!www)))?[-a-zA-Z0-9@:%._+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)\$").matches(query)) {
                sourceService.searchByUrl(
                        query.trim('/'),
                        page,
                        tlds,
                        languages
                )
            } else {
                sourceService.search(
                        query,
                        page,
                        tlds,
                        languages,
                        listOf(SourceType.RSS_URL)
                )
            }

            return search.map {
                // todo specific sql query
                val feeds = feedRepository.findBySourceAndAccount(it, account)

                SourceSearchView(
                        it.id,
                        it.type,
                        it.source,
                        it.title,
                        it.link,
                        it.description,
                        if (it.smallIconHash != null) sourceIconStorage.getUrlSmallIcon(it) else null,
                        if (it.bigIconHash != null) sourceIconStorage.getUrlBigIcon(it) else null,
                        if (feeds.isNotEmpty()) feeds.first().id else null,
                        if (feeds.isNotEmpty()) feeds.first().category.name else null
                )
            }
        } catch (e: InvalidDataAccessApiUsageException) {
            throw BadRequestException() //todo
        }
    }

    @RequestMapping(method = [RequestMethod.GET], path = ["{id}"])
    fun preview(
            @Valid @NotNull @PathVariable("id") id: UUID,
            @AuthenticationPrincipal account: Account
    ): SourcePreview {
        try {
        } catch (e: InvalidDataAccessApiUsageException) {
            throw BadRequestException() //todo
        }

        val result = scrapperService.scrapByWorker(ScrapTask(
                ScrapTaskSourceType.SOURCE_ID,
                id.toString(),
                storeItems = true,
                returnItems = true,
                quick = true
        ))

        if (result.status == ScrapResultStatus.SUCCESS && result.data != null && result.data.items != null) {
            return SourcePreview(
                    result.data.sourceId,
                    result.data.sourceName,
                    result.data.sourceData,
                    result.data.items.map { EntryView(it.title, it.link) }
            )
        } else {
            throw IllegalStateException("")//todo
        }
    }

    @RequestMapping(method = [RequestMethod.POST])
    fun create(
            @Valid @RequestBody data: CreateSourceData,
            @AuthenticationPrincipal account: Account,
            request: HttpServletRequest
    ): CreateSourceResult {
        if (!limitedActionService.canDo(LimitedActionType.CREATE_SOURCE, request.remoteAddr, account)) {
            throw TooManyRequestsException()
        }
        limitedActionService.log(LimitedActionType.CREATE_SOURCE, request.remoteAddr, account);

        val type = when (data.type) {
            CreateSourceType.TWITTER_ACCOUNT_NAME -> ScrapTaskSourceType.TWITTER_ACCOUNT_NAME
            CreateSourceType.RSS_URL -> ScrapTaskSourceType.URL
            CreateSourceType.YOUTUBE_CHANNEL_ID -> ScrapTaskSourceType.YOUTUBE_CHANNEL_ID
            CreateSourceType.YOUTUBE_CHANNEL_NAME -> ScrapTaskSourceType.YOUTUBE_CHANNEL_NAME
            CreateSourceType.YOUTUBE_PLAYLIST_ID -> ScrapTaskSourceType.YOUTUBE_PLAYLIST_ID
        }

        val result = scrapperService.scrapByWorker(ScrapTask(
                type,
                data.source.trim(),
                storeItems = true,
                returnItems = false,
                quick = true
        ))

        if (result.status == ScrapResultStatus.SUCCESS && result.data != null) {
            return CreateSourceResult(
                    result.data.sourceId,
                    result.data.sourceName,
                    result.data.sourceData,
                    result.data.sourceType
            )
        } else {
            throw BadRequestException()
        }
    }
}