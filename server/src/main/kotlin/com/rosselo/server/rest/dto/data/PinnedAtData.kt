package com.rosselo.server.rest.dto.data

import java.time.LocalDateTime

data class PinnedAtData(
        val pinned: Boolean,
        val date: LocalDateTime
)