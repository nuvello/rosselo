package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.rss.SourceType
import java.util.*

class SourceSearchView(
        val id: UUID,
        val type: SourceType,
        val source: String,
        var title: String,
        var link: String,
        var description: String?,
        var smallIconUrl: String?,
        var bigIconUrl: String?,

        var feedId: UUID?,
        var categoryName: String?
)
