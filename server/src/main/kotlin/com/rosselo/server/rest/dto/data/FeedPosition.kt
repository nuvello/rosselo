package com.rosselo.server.rest.dto.data

import javax.validation.constraints.Max
import javax.validation.constraints.Min

data class FeedPosition(
        @field:Min(1)
        @field:Max(5)
        val column: Int,
        @field:Min(0)
        val position: Int
)