package com.rosselo.server.rest

import com.fasterxml.jackson.databind.ObjectMapper
import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.NotFoundException
import com.rosselo.server.exceptions.TooManyRequestsException
import com.rosselo.server.exceptions.ForbiddenException
import com.rosselo.server.rest.dto.ErrorDetails
import org.springframework.beans.ConversionNotSupportedException
import org.springframework.beans.TypeMismatchException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.http.converter.HttpMessageNotWritableException
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.validation.BindException
import org.springframework.web.HttpMediaTypeNotAcceptableException
import org.springframework.web.HttpMediaTypeNotSupportedException
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.MissingPathVariableException
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.ServletRequestBindingException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.context.request.WebRequest
import org.springframework.web.context.request.async.AsyncRequestTimeoutException
import org.springframework.web.multipart.support.MissingServletRequestPartException
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.ValidationException
import org.springframework.web.servlet.NoHandlerFoundException
import javax.validation.ConstraintViolationException


@ControllerAdvice
@RestController
class ExceptionHandler @Autowired constructor(
        private val objectMapper: ObjectMapper
) :
        ResponseEntityExceptionHandler(), AuthenticationEntryPoint {

    override fun handleMethodArgumentNotValid(ex: MethodArgumentNotValidException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request", "Invalid data"), HttpStatus.BAD_REQUEST)
    }

    override fun handleHttpMessageNotReadable(ex: HttpMessageNotReadableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleMissingServletRequestParameter(ex: MissingServletRequestParameterException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleMissingPathVariable(ex: MissingPathVariableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleServletRequestBindingException(ex: ServletRequestBindingException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleHttpMediaTypeNotSupported(ex: HttpMediaTypeNotSupportedException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleHttpMediaTypeNotAcceptable(ex: HttpMediaTypeNotAcceptableException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleBindException(ex: BindException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
    }

    override fun handleHttpRequestMethodNotSupported(ex: HttpRequestMethodNotSupportedException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Request method not supported"), HttpStatus.METHOD_NOT_ALLOWED)
    }

    override fun handleNoHandlerFoundException(ex: NoHandlerFoundException, headers: HttpHeaders, status: HttpStatus, request: WebRequest): ResponseEntity<Any> {
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Not found"), HttpStatus.NOT_FOUND)
    }


    override fun commence(p0: HttpServletRequest, p1: HttpServletResponse, p2: AuthenticationException) {
        p1.status = HttpStatus.UNAUTHORIZED.value()
        val json = objectMapper.writeValueAsString(ErrorDetails(LocalDateTime.now(), "Unauthorized"))

        p1.writer.write(json)
        p1.flushBuffer()
    }


    @ExceptionHandler(Exception::class)
    fun handleMethodArgumentNotValid(ex: Exception, request: WebRequest): ResponseEntity<Any> {
        if (ex is com.rosselo.server.exceptions.ValidationException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request", ex.message), HttpStatus.BAD_REQUEST)
        }

        if (ex is ConstraintViolationException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request", ex.message), HttpStatus.BAD_REQUEST)
        }

        if (ex is ValidationException || ex is BadRequestException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Bad request"), HttpStatus.BAD_REQUEST)
        }

        if (ex is NotFoundException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Not found"), HttpStatus.NOT_FOUND)
        }

        if (ex is TooManyRequestsException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Too many requests"), HttpStatus.TOO_MANY_REQUESTS)
        }

        if (ex is ForbiddenException) {
            return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Forbidden"), HttpStatus.FORBIDDEN)
        }

        ex.printStackTrace()
        return ResponseEntity(ErrorDetails(LocalDateTime.now(), "Internal server error"), HttpStatus.INTERNAL_SERVER_ERROR)
    }
}
