package com.rosselo.server.rest.controller

import com.rosselo.server.utils.network.TwitterUtil
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("utils")
class UtilResource {
    @RequestMapping(path = ["twitter/{nickname}"])
    fun list(
            @PathVariable("nickname") nickname: String
    ): Boolean =
            TwitterUtil().checkIfNicknameExists(nickname)
}