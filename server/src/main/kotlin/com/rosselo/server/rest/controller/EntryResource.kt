package com.rosselo.server.rest.controller

import com.rosselo.server.exceptions.NotFoundException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.repository.rss.EntryRepository
import com.rosselo.server.rest.dto.data.PinnedAtData
import com.rosselo.server.rest.dto.data.ReadAtData
import com.rosselo.server.service.rss.EntryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("entries")
class EntryResource @Autowired constructor(
        private val entryService: EntryService,
        private val entryRepository: EntryRepository
) {
    @RequestMapping(path = ["{id}/read"], method = [RequestMethod.PUT])
    fun markAsRead(
            @PathVariable("id") entryId: UUID,
            @Valid @RequestBody data: ReadAtData,
            @AuthenticationPrincipal account: Account
    ) {
        val entry = entryRepository.findById(entryId)
                .orElseThrow { NotFoundException() }

        entryService.setRead(entry, account, data.read, data.date)
    }

    @RequestMapping(path = ["{id}/pinned"], method = [RequestMethod.PUT])
    fun markAsPinned(
            @PathVariable("id") entryId: UUID,
            @Valid @RequestBody data: PinnedAtData,
            @AuthenticationPrincipal account: Account
    ) {
        val entry = entryRepository.findById(entryId)
                .orElseThrow { NotFoundException() }

        entryService.setPinned(entry, account, data.pinned, data.date)
    }
}