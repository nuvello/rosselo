package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.AccountPreferenceType
import java.time.LocalDateTime
import java.util.*

data class AccountView(
        val id: UUID,
        var email: String,
        val createdAt: LocalDateTime,
        val preferences: Map<AccountPreferenceType, String>,
        val activePlan: AccountPlanView
)