package com.rosselo.server.rest.dto.view

import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class EntryDashboardView(
        val id: UUID,
        val createdAt: LocalDateTime,
        var title: String,
        var link: String,
        val pinnedAt: LocalDateTime?,
        val unpinnedAt: LocalDateTime?,
        val readAt: LocalDateTime?
)
