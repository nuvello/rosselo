package com.rosselo.server.rest.dto.view

data class FeedDashboardView(
    val feed: FeedView,
    val items: List<EntryDashboardView>
)