package com.rosselo.server.rest.controller

import com.rosselo.server.model.entity.ConfigType
import com.rosselo.server.rest.dto.view.ConfigsView
import com.rosselo.server.service.ConfigService
import com.rosselo.server.service.GeoIPService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.math.BigDecimal
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("configs")
class ConfigResource @Autowired constructor(
        private val configService: ConfigService,
        private val geoIPService: GeoIPService
) {
    @RequestMapping
    fun all(
            request: HttpServletRequest
    ): ConfigsView {
        val countryData = geoIPService.locateCountry(request.remoteAddr)

        val currency = when {
            countryData == null -> "USD"
            countryData.country.isoCode == "GB" -> "GBP"
            countryData.continent.code == "EU" -> "EUR"
            else -> "USD"
        }

        return ConfigsView(
                configService.getInt(ConfigType.TRIAL_PERIOD_IN_DAYS),
                BigDecimal(configService.get(ConfigType.PREMIUM_PRICE_YEAR_WITHOUT_VAT)),
                configService.getInt(ConfigType.MAX_NUMBER_OF_FEEDS_PER_ACCOUNT),
                configService.getInt(ConfigType.MAX_NUMBER_OF_CATEGORIES_PER_ACCOUNT),
                configService.getInt(ConfigType.MAX_NUMBER_OF_ENTRIES_PER_FEED),
                configService.get(ConfigType.DEFAULT_FEED_HIDE_ENTRIES_AFTER_READ_PREFERENCE) == "true",
                configService.getNullableInt(ConfigType.DEFAULT_FEED_MAX_AMOUNT_OF_ITEMS_PREFERENCE),
                configService.getNullableInt(ConfigType.DEFAULT_FEED_NO_OLDER_THAN_PREFERENCE),
                countryData?.continent?.code,
                countryData?.country?.isoCode,
                currency
        )
    }
}
