package com.rosselo.server.rest.dto.data

import org.hibernate.validator.constraints.Length
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull

@Validated
data class ResetPasswordData(
        @field:Email
        @field:NotNull
        val email: String,
        @field:NotNull
        val confirmToken: String,
        @field:NotNull
        @field:Length(min = 8)
        val newPassword: String
)