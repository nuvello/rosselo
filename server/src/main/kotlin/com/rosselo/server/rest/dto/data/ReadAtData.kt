package com.rosselo.server.rest.dto.data

import java.time.LocalDateTime

data class ReadAtData(
        val read: Boolean,
        val date: LocalDateTime
)