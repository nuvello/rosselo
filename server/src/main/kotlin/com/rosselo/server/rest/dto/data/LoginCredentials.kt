package com.rosselo.server.rest.dto.data

import org.hibernate.validator.constraints.Length
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

/**
 * Simple object representing login credentials sent by external applications.
 *
 * @author Juraj Mlich <jurajmlich></jurajmlich>@gmail.com>
 */
@Validated
class LoginCredentials(
        @field:Email
        @field:NotNull
        var email: String,
        @field:Length(min = 8)
        @field:NotEmpty
        var password: String
) {
    constructor() : this("", "") // needed for jackson
}
