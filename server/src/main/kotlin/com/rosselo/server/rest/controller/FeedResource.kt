package com.rosselo.server.rest.controller

import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.NotFoundException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.repository.rss.CategoryRepository
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.rest.dto.data.FeedData
import com.rosselo.server.rest.dto.data.FeedPosition
import com.rosselo.server.rest.dto.data.FeedUpdateData
import com.rosselo.server.rest.dto.view.EntryDashboardView
import com.rosselo.server.rest.dto.view.FeedDashboardView
import com.rosselo.server.rest.dto.view.FeedView
import com.rosselo.server.rest.mapper.ViewMapper
import com.rosselo.server.service.rss.FeedService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import java.util.*
import javax.validation.Valid

@RestController
@RequestMapping("feeds")
class FeedResource @Autowired constructor(
        private val feedService: FeedService,
        private val feedRepository: FeedRepository,
        private val categoryRepository: CategoryRepository,
        private val viewMapper: ViewMapper
) {
    @RequestMapping
    fun list(
            @AuthenticationPrincipal account: Account
    ): List<FeedView> =
            feedRepository.findByAccount(account).map { viewMapper.feedToView(it) }

    @RequestMapping(method = [RequestMethod.POST])
    fun create(
            @Valid @RequestBody feedData: FeedData,
            @AuthenticationPrincipal account: Account
    ): FeedView {
        // todo: check if maxAmountOfItems don't exceed the limit
        val category = categoryRepository.findById(feedData.categoryId)
                .orElseThrow { BadRequestException() }

        if (category.account != account) {
            throw BadRequestException()
        }

        return viewMapper.feedToView(feedService.create(
                account,
                category,
                feedData.name,
                feedData.sourceId,
                feedData.hideEntriesAfterRead,
                feedData.maxAmountOfItems,
                feedData.noOlderThan
        ))
    }

    @RequestMapping(path = ["{id}"], method = [RequestMethod.PUT])
    fun update(
            @PathVariable("id") id: UUID,
            @Valid @RequestBody feedData: FeedUpdateData,
            @AuthenticationPrincipal account: Account
    ): FeedView {
        // todo: check if maxAmountOfItems don't exceed the limit
        val feed =
                feedRepository.findById(id).orElseThrow { NotFoundException() }

        if (feed.account != account) {
            throw NotFoundException()
        }

        feedService.update(
                account,
                feed,
                feedData.name,
                feedData.sourceId,
                feedData.hideEntriesAfterRead,
                feedData.maxAmountOfItems,
                feedData.noOlderThan
        )

        if (feedData.categoryId != feed.category.id) {
            val category = categoryRepository.findById(feedData.categoryId)
                    .orElseThrow { BadRequestException() }

            feedService.moveToCategory(feed, category)
        }

        return viewMapper.feedToView(feed)
    }

    @RequestMapping(path = ["{id}"], method = [RequestMethod.DELETE])
    fun delete(
            @PathVariable("id") id: UUID,
            @AuthenticationPrincipal account: Account
    ) {
        val feed = feedRepository.findById(id)
                .orElseThrow { NotFoundException() }

        if (feed.account != account) {
            throw NotFoundException()
        }

        feedService.delete(feed)
    }

    @RequestMapping(path = ["{id}/position"], method = [RequestMethod.PUT])
    fun moveToPosition(
            @PathVariable("id") id: UUID,
            @Valid @RequestBody feedPosition: FeedPosition,
            @AuthenticationPrincipal account: Account
    ) {
        val feed = feedRepository.findById(id)
                .orElseThrow { NotFoundException() }

        if (feed.account != account) {
            throw NotFoundException()
        }

        feedService.moveToColumn(feed, feedPosition.column, feedPosition.position)
    }

    @RequestMapping(path = ["{id}/read"], method = [RequestMethod.PUT])
    fun markAsRead(
            @PathVariable("id") feedId: UUID,
            @RequestBody data: LocalDateTime,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(name = "entries-till") entriesTill: LocalDateTime?,
            @AuthenticationPrincipal account: Account
    ) {
        val feed = feedRepository.findById(feedId)
                .orElseThrow { NotFoundException() }

        feedService.markAsRead(feed, account, data, entriesTill)
    }

    @RequestMapping("{id}/dashboard")
    fun getCategoryFeedsForDashboard(
            @PathVariable("id") id: UUID,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(name = "entries-since") entriesSince: LocalDateTime?
    ): FeedDashboardView {

        val feed = feedRepository.findById(id)
                .orElseThrow { IllegalStateException() }

        val feedWithEntries = feedService.findWithEntries(
                feed,
                entriesSince
        )

        return FeedDashboardView(
                viewMapper.feedToView(feedWithEntries.feed),
                feedWithEntries.entries.map { item ->
                    EntryDashboardView(
                            item.entry.id,
                            item.entry.createdAt,
                            item.entry.title,
                            item.entry.link,
                            item.meta?.pinnedAt,
                            item.meta?.unpinnedAt,
                            item.meta?.readAt
                    )
                }
        )
    }
}