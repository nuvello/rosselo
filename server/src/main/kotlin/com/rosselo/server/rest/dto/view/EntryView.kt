package com.rosselo.server.rest.dto.view

data class EntryView (
    val title: String,
    val link: String
)