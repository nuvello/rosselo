package com.rosselo.server.rest.dto.view

import java.util.*

class CategoryView(
        val id: UUID,
        val name: String,
        val slug: String,
        val position: Int
)
