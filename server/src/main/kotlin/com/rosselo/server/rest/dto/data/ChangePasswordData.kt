package com.rosselo.server.rest.dto.data

import org.hibernate.validator.constraints.Length
import javax.validation.constraints.NotEmpty

data class ChangePasswordData(
        @field:NotEmpty
        val oldPassword: String,
        @field:NotEmpty
        @field:Length(min = 8)
        val newPassword: String
)