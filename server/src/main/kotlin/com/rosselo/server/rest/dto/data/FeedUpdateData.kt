package com.rosselo.server.rest.dto.data

import java.util.*
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

data class FeedUpdateData(
        @field:NotBlank
        val name: String,
        @field:NotNull
        val sourceId: UUID,
        val hideEntriesAfterRead: Boolean,
        @field:Min(1)
        val maxAmountOfItems: Int?,
        @field:Min(1)
        val noOlderThan: Int?,
        @field:NotNull
        val categoryId: UUID
)