package com.rosselo.server.rest.dto.data

import javax.validation.constraints.NotEmpty

data class CategoryData(
        @field:NotEmpty
        val name: String
)