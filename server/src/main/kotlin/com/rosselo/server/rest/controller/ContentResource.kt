package com.rosselo.server.rest.controller

import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.ValidationException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.repository.rss.CategoryRepository
import com.rosselo.server.service.rss.ContentService
import com.rosselo.server.utils.opml.OpmlCategory
import com.rosselo.server.utils.opml.OpmlParser
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import java.lang.Exception
import java.util.*


@RestController
@RequestMapping("content")
class ContentResource @Autowired constructor(
        private val categoryRepository: CategoryRepository,
        private val contentService: ContentService
) {
    @PostMapping("/import")
    fun importFeeds(
            @RequestParam("categoryId") categoryId: UUID?,
            @RequestParam("file") file: MultipartFile,
            @AuthenticationPrincipal account: Account
    ) {
        val opmlCategories: List<OpmlCategory>
        try {
            opmlCategories = OpmlParser().parse(file.bytes)
        } catch (e: Exception) {
               throw BadRequestException()
        }
        val category = if (categoryId != null) {
            categoryRepository.findById(categoryId)
                    .orElseThrow { BadRequestException() }
        } else {
            null
        }

        contentService.importOpmlFeeds(
                account,
                category,
                opmlCategories
        )

    }
}