package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.rss.SourceType
import java.time.LocalDateTime
import java.util.*

class SourceView(
        val id: UUID,
        val type: SourceType,
        val source: String,
        var title: String,
        var link: String,
        var description: String?,
        var subscribers: Int,
        var error: Boolean,
        var lastSuccessfulFetchAt: LocalDateTime,
        var lastExtraScrapAt: LocalDateTime?,
        var smallIconUrl: String?,
        var bigIconUrl: String?
)
