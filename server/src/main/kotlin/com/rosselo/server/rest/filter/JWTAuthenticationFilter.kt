package com.rosselo.server.rest.filter

import com.fasterxml.jackson.databind.ObjectMapper
import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.exceptions.TooManyRequestsException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.LimitedActionType
import com.rosselo.server.repository.AccountRepository
import com.rosselo.server.rest.dto.data.LoginCredentials
import com.rosselo.server.service.LimitedActionService
import com.rosselo.server.utils.JwtTokenGenerator
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.AuthenticationServiceException
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Filter that performs authentication from POST request with the body of JSON object with the scheme of [LoginCredentials]
 * object. If correct, Authorization header with JWT token is added to the response.
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class JWTAuthenticationFilter(
        private val limitedActionService: LimitedActionService,
        private val accountRepository: AccountRepository,
        private val rosseloConfig: RosseloConfig,
        private val jwtTokenGenerator: JwtTokenGenerator
) : AbstractAuthenticationProcessingFilter(AntPathRequestMatcher("/accounts/login")) {
    companion object {
        const val TOKEN_PREFIX = "Bearer "
        const val HEADER_STRING = "Authorization"
    }

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(req: ServletRequest, res: ServletResponse, chain: FilterChain) {
        try {
            super.doFilter(req, res, chain)
        } catch (e: TooManyRequestsException) {
            (res as HttpServletResponse).status =
                    HttpStatus.TOO_MANY_REQUESTS.value()
            res.setContentType("application/json")
            res.getOutputStream()
                    .write("{\"message\": \"Auth limit exceeded.\"}".toByteArray(StandardCharsets.UTF_8))
        }

    }

    @Throws(AuthenticationException::class)
    override fun attemptAuthentication(req: HttpServletRequest,
                                       res: HttpServletResponse): Authentication {
        if (req.method != "POST") {
            throw AuthenticationServiceException(
                    "Authentication method not supported: " + req.method)
        }

        try {
            val credentials = ObjectMapper()
                    .readValue(req.inputStream, LoginCredentials::class.java)

            val user = accountRepository.findByEmail(credentials.email)

            if (!limitedActionService.canDo(LimitedActionType.UNSUCCESSFUL_LOGIN, req.remoteAddr, user)) {
                throw TooManyRequestsException()
            }

            try {
                return authenticationManager.authenticate(
                        UsernamePasswordAuthenticationToken(
                                credentials.email,
                                credentials.password,
                                ArrayList<GrantedAuthority>())
                )
            } catch (e: BadCredentialsException) {
                limitedActionService.log(LimitedActionType.UNSUCCESSFUL_LOGIN, req.remoteAddr, user)

                throw e
            }

        } catch (e: IOException) {
            throw RuntimeException(e)
        }

    }

    override fun requiresAuthentication(request: HttpServletRequest, response: HttpServletResponse?): Boolean {
        // we do not want to validate OPTIONS requests
        return request.method != HttpMethod.OPTIONS.name && super.requiresAuthentication(request, response)
    }

    override fun successfulAuthentication(req: HttpServletRequest,
                                          res: HttpServletResponse,
                                          chain: FilterChain?,
                                          auth: Authentication) {
        val token =
                jwtTokenGenerator.generate((auth.principal as Account))

        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token)

        if (!rosseloConfig.production) {
            // so that external applications with different origin can see the header
            res.addHeader("Access-Control-Expose-Headers", "Authorization")
        }
    }
}
