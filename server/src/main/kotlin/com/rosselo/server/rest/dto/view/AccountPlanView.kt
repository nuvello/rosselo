package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.AccountPlanType
import java.time.LocalDateTime

data class AccountPlanView(
        val type: AccountPlanType,
        val validFrom: LocalDateTime,
        var validTo: LocalDateTime
)