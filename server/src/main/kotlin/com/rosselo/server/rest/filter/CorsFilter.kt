package com.rosselo.server.rest.filter

import com.rosselo.server.config.RosseloConfig
import org.springframework.http.HttpMethod
import java.io.IOException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class CorsFilter(
        private val rosseloConfig: RosseloConfig
) : Filter {
    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        if (!rosseloConfig.production) {
            val res = response as HttpServletResponse
            val req = request as HttpServletRequest

            res.setHeader("Access-Control-Allow-Origin", "*")
            res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
            res.setHeader("Access-Control-Allow-Credentials", "true")
            // todo this vs addCorsMappings
            res.setHeader("Access-Control-Allow-Headers",
                    "authorization, content-type, x-gwt-module-base, x-gwt-permutation, clientid, longpush, X-Xsrf-Token")

            if (req.method == HttpMethod.OPTIONS.name) {
                return
            }
        }

        chain.doFilter(request, response)
    }
}
