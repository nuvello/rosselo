package com.rosselo.server.rest.mapper

import com.rosselo.server.model.entity.rss.Category
import com.rosselo.server.model.entity.rss.Feed
import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.rest.dto.view.CategoryView
import com.rosselo.server.rest.dto.view.FeedView
import com.rosselo.server.rest.dto.view.SourceView
import com.rosselo.server.service.SourceIconStorage
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class ViewMapper(
        private val sourceIconStorage: SourceIconStorage
) {
    fun sourceToView(source: Source): SourceView {
        return SourceView(
                source.id,
                source.type,
                source.source,
                source.title,
                source.link,
                source.description,
                source.subscribers,
                source.errorsSinceLastSuccessful > 4, // todo constant
                source.lastSuccessfulFetchAt,
                source.lastExtraScrapAt,
                if (source.smallIconHash != null) sourceIconStorage.getUrlSmallIcon(source) else null,
                if (source.bigIconHash != null) sourceIconStorage.getUrlBigIcon(source) else null
        )
    }

    fun feedToView(feed: Feed): FeedView {
        return FeedView(
                feed.id,
                feed.name,
                sourceToView(feed.source),
                feed.category.id,
                feed.createdAt,
                feed.column,
                feed.position,
                feed.hideEntriesAfterRead,
                feed.maxAmountOfEntries,
                feed.noOlderThan
        )
    }

    fun categoryToView(category: Category): CategoryView =
            CategoryView(
                    id = category.id,
                    name = category.name,
                    slug = category.slug,
                    position = category.position
            )
}