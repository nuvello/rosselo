package com.rosselo.server.rest.dto.view

import java.time.LocalDateTime
import java.util.*

open class FeedView(
        val id: UUID,
        var name: String,
        val source: SourceView,
        val categoryId: UUID,
        val createdAt: LocalDateTime,
        var column: Int,
        var position: Int,
        var hideEntriesAfterRead: Boolean,
        var maxAmountOfItems: Int?,
        var noOlderThan: Int?
)
