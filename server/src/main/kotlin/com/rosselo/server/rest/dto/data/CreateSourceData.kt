package com.rosselo.server.rest.dto.data

enum class CreateSourceType {
    RSS_URL,
    YOUTUBE_CHANNEL_ID,
    YOUTUBE_CHANNEL_NAME,
    YOUTUBE_PLAYLIST_ID,
    TWITTER_ACCOUNT_NAME
}

data class CreateSourceData(
        val type: CreateSourceType,
        val source: String
)