package com.rosselo.server.rest.dto.view

import com.rosselo.server.model.entity.HideEntriesAfterReadPreference
import java.math.BigDecimal

data class ConfigsView(
        val trialPeriodInDays: Int,
        val premiumPriceYearWithoutVat: BigDecimal,
        val maxNumberOfFeedsPerAccount: Int,
        val maxNumberOfCategoriesPerAccount: Int,
        val maxNumberOfEntriesPerFeed: Int,
        val defaultFeedHideEntriesAfterReadPreference: Boolean,
        val defaultFeedMaxAmountOfItemsPreference: Int?,
        val defaultFeedNoOlderThanPreference: Int?,
        val continent: String?,
        val country: String?,
        val currency: String
)
