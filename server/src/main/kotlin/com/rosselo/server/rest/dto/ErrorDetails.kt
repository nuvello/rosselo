package com.rosselo.server.rest.dto

import java.time.LocalDateTime

data class ErrorDetails(val timestamp: LocalDateTime, val message: String, val details: String? = null)
