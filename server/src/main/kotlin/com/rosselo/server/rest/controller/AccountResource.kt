// todo fix exceptions in the whole app
package com.rosselo.server.rest.controller

import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.TooManyRequestsException
import com.rosselo.server.model.entity.*
import com.rosselo.server.repository.AccountPlanRepository
import com.rosselo.server.repository.AccountPreferenceRepository
import com.rosselo.server.repository.AccountRepository
import com.rosselo.server.rest.dto.data.ChangePasswordData
import com.rosselo.server.rest.dto.data.LoginCredentials
import com.rosselo.server.rest.dto.data.ResetPasswordData
import com.rosselo.server.rest.dto.view.AccountPlanView
import com.rosselo.server.rest.dto.view.AccountView
import com.rosselo.server.rest.filter.JWTAuthenticationFilter.Companion.HEADER_STRING
import com.rosselo.server.rest.filter.JWTAuthenticationFilter.Companion.TOKEN_PREFIX
import com.rosselo.server.service.AccountService
import com.rosselo.server.service.LimitedActionService
import com.rosselo.server.utils.JwtTokenGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import javax.validation.Valid
import javax.validation.constraints.Email


@RestController
@RequestMapping("accounts") // todo make constant (and use in security config)
@Validated
class AccountResource @Autowired constructor(
        private val accountRepository: AccountRepository,
        private val accountService: AccountService,
        private val limitedActionService: LimitedActionService,
        private val accountPreferenceRepository: AccountPreferenceRepository,
        private val accountPlanRepository: AccountPlanRepository,
        private val jwtTokenGenerator: JwtTokenGenerator,
        private val rosseloConfig: RosseloConfig
) {
    @RequestMapping(path = ["refresh-token"], method = [RequestMethod.GET])
    fun refreshToken(
            response: HttpServletResponse,
            request: HttpServletRequest,
            @AuthenticationPrincipal account: Account
    ) {
        if (!limitedActionService.canDo(LimitedActionType.AUTH_TOKEN_REFRESH, request.remoteAddr, account)) {
            throw TooManyRequestsException()
        }

        val token = jwtTokenGenerator.generate(account)

        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token)

        if (!rosseloConfig.production) {
            // so that external applications with different origin can see the header
            response.addHeader("Access-Control-Expose-Headers", "Authorization")
        }

        limitedActionService.log(LimitedActionType.AUTH_TOKEN_REFRESH, request.remoteAddr, account)
    }

    @RequestMapping(path = ["request-password-reset"], method = [RequestMethod.POST])
    fun requestPasswordReset(
            @Valid @Email @RequestBody email: String,
            request: HttpServletRequest
    ) {
        val account = accountRepository.findByEmail(email)

        if (!limitedActionService.canDo(LimitedActionType.PASSWORD_RESET_REQUEST, request.remoteAddr, account)) {
            throw TooManyRequestsException()
        }

        if (account != null) {
            this.accountService.requestResetPassword(account)
        }

        limitedActionService.log(LimitedActionType.PASSWORD_RESET_REQUEST, request.remoteAddr, account)
    }

    @RequestMapping(path = ["password-reset"], method = [RequestMethod.POST])
    fun resetPassword(
            @Valid @RequestBody resetPasswordData: ResetPasswordData,
            request: HttpServletRequest
    ) {
        val account = accountRepository.findByEmail(resetPasswordData.email)

        if (!limitedActionService.canDo(LimitedActionType.PASSWORD_RESET, request.remoteAddr, account)) {
            throw TooManyRequestsException()
        }

        if (account != null) {
            try {
                this.accountService.resetPassword(account, resetPasswordData.confirmToken, resetPasswordData.newPassword)
            } catch (e: IllegalStateException) {
                throw BadRequestException()
            }
        }

        limitedActionService.log(LimitedActionType.PASSWORD_RESET, request.remoteAddr, account)
    }

    /**
     * Permitted without being logged in in [com.rosselo.server.config.SecurityConfig]
     */
    @RequestMapping(method = [RequestMethod.POST])
    fun register(
            @Valid @RequestBody data: LoginCredentials,
            request: HttpServletRequest
    ) {
        if (!limitedActionService.canDo(LimitedActionType.NEW_ACCOUNT_CREATION, request.remoteAddr, null)) {
            throw TooManyRequestsException()
        }

        this.accountService.create(
                data.email,
                data.password
        )

        limitedActionService.log(LimitedActionType.NEW_ACCOUNT_CREATION, request.remoteAddr, null);
    }

    @RequestMapping("current")
    fun getCurrentUserData(
            @AuthenticationPrincipal account: Account
    ): AccountView {
        val preferences = accountPreferenceRepository
                .findFor(account)
                .map { it.id.type to it.value }
                .toMap()

        val activePlan = accountPlanRepository.findActiveFor(account)
                ?: AccountPlan(
                        account = account,
                        type = AccountPlanType.EXPIRED,
                        validFrom = LocalDateTime.now(),
                        validTo = LocalDateTime.now().plusYears(10)
                )

        return AccountView(
                account.id,
                account.email,
                account.createdAt,
                preferences,
                AccountPlanView(
                        activePlan.type,
                        activePlan.validFrom,
                        activePlan.validTo
                )
        )
    }

    @RequestMapping("current/email", method = [RequestMethod.PUT])
    fun changeEmail(
            @Valid @Email @RequestBody value: String,
            @AuthenticationPrincipal account: Account
    ) {
        this.accountService.updateEmail(
                account,
                value
        )
    }

    @RequestMapping("current/password", method = [RequestMethod.PUT])
    fun changePassword(
            @Valid @RequestBody value: ChangePasswordData,
            @AuthenticationPrincipal account: Account
    ) {
        this.accountService.updatePassword(
                account,
                value.oldPassword,
                value.newPassword
        )
    }

    @RequestMapping("current/preferences/{preference}", method = [RequestMethod.PUT])
    fun updatePreference(
            @PathVariable("preference") preference: AccountPreferenceType,
            @Valid @RequestBody value: String,
            @AuthenticationPrincipal account: Account
    ) {
        this.accountService.updatePreference(
                account,
                preference,
                value
        )
    }

    @RequestMapping("current", method = [RequestMethod.DELETE])
    fun terminate(
            @AuthenticationPrincipal account: Account
    ) {
        this.accountService.delete(
                account
        )
    }
}