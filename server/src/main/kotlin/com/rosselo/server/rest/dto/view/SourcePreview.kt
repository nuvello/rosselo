package com.rosselo.server.rest.dto.view

import java.util.*

data class SourcePreview(
        val sourceId: UUID,
        val sourceName: String,
        val sourceData: String,
        val items: List<EntryView>
)