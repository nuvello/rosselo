package com.rosselo.server.rest.controller

import com.rosselo.server.exceptions.NotFoundException
import com.rosselo.server.model.entity.Account
import com.rosselo.server.repository.rss.CategoryRepository
import com.rosselo.server.rest.dto.data.CategoryData
import com.rosselo.server.rest.dto.view.CategoryView
import com.rosselo.server.rest.dto.view.EntryDashboardView
import com.rosselo.server.rest.dto.view.FeedDashboardView
import com.rosselo.server.rest.mapper.ViewMapper
import com.rosselo.server.service.rss.CategoryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.web.bind.annotation.*
import java.time.LocalDateTime
import java.util.*
import javax.validation.Valid
import javax.validation.constraints.Min


@RestController
@RequestMapping("categories")
class CategoryResource @Autowired constructor(
        private val categoryService: CategoryService,
        private val categoryRepository: CategoryRepository,
        private val viewMapper: ViewMapper
) {
    @RequestMapping
    fun list(
            @AuthenticationPrincipal account: Account
    ): List<CategoryView> =
            categoryRepository.findAllByAccountOrderByPosition(account)
                    .map { viewMapper.categoryToView(it) }

    @RequestMapping(method = [RequestMethod.POST])
    fun create(
            @Valid @RequestBody categoryData: CategoryData,
            @AuthenticationPrincipal account: Account
    ): CategoryView =
            viewMapper.categoryToView(categoryService.create(account, categoryData.name))

    @RequestMapping(path = ["{id}"], method = [RequestMethod.PUT])
    fun update(
            @PathVariable("id") id: UUID,
            @Valid @RequestBody categoryData: CategoryData,
            @AuthenticationPrincipal account: Account
    ): CategoryView {
        val category = categoryRepository.findById(id)
                .orElseThrow { NotFoundException() }

        if (category.account != account) {
            throw NotFoundException()
        }

        return viewMapper.categoryToView(categoryService.rename(category, categoryData.name))
    }

    @RequestMapping(path = ["{id}"], method = [RequestMethod.DELETE])
    fun delete(
            @PathVariable("id") id: UUID,
            @AuthenticationPrincipal account: Account
    ) {
        val category = categoryRepository.findById(id)
                .orElseThrow { NotFoundException() }

        if (category.account != account) {
            throw NotFoundException()
        }

        categoryService.delete(category)
    }

    @RequestMapping(path = ["{id}/position"], method = [RequestMethod.PUT])
    fun moveToPosition(
            @PathVariable("id") id: UUID,
            @Valid @Min(0) @RequestBody newPosition: Int,
            @AuthenticationPrincipal account: Account
    ) {
        val category = categoryRepository.findById(id)
                .orElseThrow { NotFoundException() }

        if (category.account != account) {
            throw NotFoundException()
        }

        categoryService.moveToPosition(category, newPosition)
    }

    @RequestMapping("{id}/dashboard-feeds")
    fun getCategoryFeedsForDashboard(
            @PathVariable("id") id: UUID,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(name = "entries-since") entriesSince: LocalDateTime?
    ): List<FeedDashboardView> {

        val category = categoryRepository.findById(id)
                .orElseThrow { IllegalStateException() }
        return categoryService.findFeedsWithEntries(
                category,
                entriesSince
        ).map {
            FeedDashboardView(
                    viewMapper.feedToView(it.feed),
                    it.entries.map { item ->
                        EntryDashboardView(
                                item.entry.id,
                                item.entry.createdAt,
                                item.entry.title,
                                item.entry.link,
                                item.meta?.pinnedAt,
                                item.meta?.unpinnedAt,
                                item.meta?.readAt
                        )
                    }
            )
        }
    }

    @RequestMapping(path = ["{id}/read"], method = [RequestMethod.PUT])
    fun markAsRead(
            @PathVariable("id") feedId: UUID,
            @RequestBody data: LocalDateTime,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(name = "entries-till") entriesTill: LocalDateTime?,
            @AuthenticationPrincipal account: Account
    ) {
        val category = categoryRepository.findById(feedId)
                .orElseThrow { NotFoundException() }

        categoryService.markAsRead(category, account, data, entriesTill)
    }

    @RequestMapping(path = ["read"], method = [RequestMethod.PUT])
    fun markAllAsRead(
            @RequestBody data: LocalDateTime,
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @RequestParam(name = "entries-till") entriesTill: LocalDateTime?,
            @AuthenticationPrincipal account: Account
    ) {
        val categories =
                categoryRepository.findAllByAccountOrderByPosition(account)

        categories.forEach {
            categoryService.markAsRead(it, account, data, entriesTill)
        }
    }
}