package com.rosselo.server.repository

import com.rosselo.server.model.entity.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Repository
interface AccountRepository : JpaRepository<Account, UUID> {
    fun findByEmail(email: String): Account?
}