package com.rosselo.server.repository.rss

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Category
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
interface CategoryRepository : JpaRepository<Category, UUID> {
    fun findAllByNameAndAccount(name: String, account: Account): List<Category>
    fun findAllByAccountOrderByPosition(account: Account): List<Category>
    fun countByAccount(account: Account): Long
    fun countByAccountAndSlug(account: Account, slug: String): Long

    @Query("SELECT MAX(position) FROM Category WHERE account = :account")
    fun findMaxPosition(@Param("account") account: Account): Int?

    @Query("SELECT COUNT(id) FROM Category WHERE account = :account")
    fun findCount(@Param("account") account: Account): Int

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Category SET position = position - 1 WHERE position > " +
            ":biggerThan AND account = :account")
    fun decreasePositionIfBiggerThan(
            @Param("account") account: Account,
            @Param("biggerThan") biggerThan: Int
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Category SET position = position - 1 WHERE position BETWEEN " +
            ":between1 AND :between2 AND account = :account")
    fun decreasePositionBetween(
            @Param("account") account: Account,
            @Param("between1") between1: Int,
            @Param("between2") between2: Int
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Category SET position = position + 1 WHERE position BETWEEN " +
            ":between1 AND :between2 AND account = :account")
    fun increasePositionBetween(
            @Param("account") account: Account,
            @Param("between1") between1: Int,
            @Param("between2") between2: Int
    )
}