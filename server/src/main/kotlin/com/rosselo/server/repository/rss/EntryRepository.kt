package com.rosselo.server.repository.rss

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Entry
import com.rosselo.server.model.entity.rss.EntryAccountMeta
import com.rosselo.server.model.entity.rss.Source
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
interface EntryRepository : JpaRepository<Entry, UUID>,
        CustomizedEntryRepository {
    @Query("SELECT entry.identifier FROM Entry entry WHERE entry.identifier IN (" +
            ":uniqueIds) AND entry.source = :source")
    fun findAlreadyInsertedEntries(@Param("uniqueIds") uniqueIds: List<String>, @Param("source") source: Source): List<String>

    @Query("FROM EntryAccountMeta eam WHERE eam.id.account = :account AND eam.id.entry = :entry")
    fun findMeta(@Param("entry") entry: Entry, @Param("account") account: Account): EntryAccountMeta?
}