package com.rosselo.server.repository.rss

import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Lock
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*
import javax.persistence.LockModeType

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
interface SourceRepository : JpaRepository<Source, UUID>, CustomizedSourceRepository {
    fun findByTypeAndSource(type: SourceType, source: String): Source?
    fun findByTypeAndLinkStartingWith(type: SourceType, link: String): Source?

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("from Source where id IN :id")
    fun findByIdWithExclusiveLock(@Param("id") id: UUID): Source?

    @Lock(LockModeType.PESSIMISTIC_READ)
    @Query("from Source where id IN :id")
    fun findByIdWithSharedLock(@Param("id") id: UUID): Source?
}