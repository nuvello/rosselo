package com.rosselo.server.repository

import com.rosselo.server.model.entity.Config
import com.rosselo.server.model.entity.ConfigType
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Repository
interface ConfigRepository : JpaRepository<Config, ConfigType>