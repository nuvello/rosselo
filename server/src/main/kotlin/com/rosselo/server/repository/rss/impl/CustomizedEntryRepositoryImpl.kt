package com.rosselo.server.repository.rss.impl

import com.rosselo.server.model.EntryWithMetaView
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.*
import com.rosselo.server.repository.rss.CustomizedEntryRepository
import java.time.LocalDateTime
import javax.persistence.EntityManager
import javax.persistence.criteria.JoinType
import javax.persistence.criteria.Predicate

class CustomizedEntryRepositoryImpl constructor(
        private val entityManager: EntityManager
) : CustomizedEntryRepository {
    override fun keepOnlyNewestPerSource(source: Source, maxPerSource: Int) {
        // todo performance
        entityManager.createNativeQuery("" +
                "DELETE FROM entry " +
                "WHERE entry.id IN (" +
                "   SELECT id FROM entry " +
                "   WHERE source_id = :source_id " +
                "   ORDER BY entry.created_at DESC " +
                "   LIMIT 100000 OFFSET $maxPerSource" +
                ") AND entry.id NOT IN (" +
                "   SELECT id FROM entry " +
                "   JOIN entry_account_meta eam ON eam.entry_id = entry.id" +
                "   WHERE eam.pinned_at IS NOT NULL" +
                ")")
                .setParameter("source_id", source.id)
                .executeUpdate()
    }

    override fun findUnreadUnpinned(source: Source, account: Account, limit: Int, noNewerThan: LocalDateTime?): List<EntryWithMetaView> {
        // todo merge with the second method
        val cb = entityManager.criteriaBuilder
        val query = cb.createTupleQuery()
        val queryRoot = query.from(Entry::class.java)
        // left join on metas with condition for metas only for the given account
        // so that we can target them if present in further where conditions
        val queryMeta = queryRoot.join(Entry_.accountMetas, JoinType.LEFT)
        queryMeta.on(cb.equal(queryMeta.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.account), account))

        query.select(
                cb.tuple(
                        queryRoot,
                        cb.isNotNull(queryMeta.get(EntryAccountMeta_.readAt)),
                        cb.isNotNull(queryMeta.get(EntryAccountMeta_.pinnedAt)))
        )

        val conditions = mutableListOf<Predicate>(
                cb.equal(queryRoot.get(Entry_.source), source)
        )

        if (noNewerThan != null) {
            conditions.add(
                    cb.lessThan(queryRoot.get(Entry_.createdAt), noNewerThan)
            )
        }

        query.orderBy(
                // pinned items on top
                cb.asc(
                        cb.selectCase<Int>()
                                .`when`(cb.isNotNull(queryMeta.get(EntryAccountMeta_.pinnedAt)), 0)
                                .otherwise(1)
                ),
                // extracted from the condition bellow as we need ASC sorting here
                // for unpinned items pinnedAt is null, so everything works perfectly
                cb.asc(queryMeta.get(EntryAccountMeta_.pinnedAt)),
                // if unpinnedAt is not null, sort by it, otherwise by createdAt
                cb.desc(
                        cb.selectCase<LocalDateTime>()
                                .`when`(cb.isNotNull(queryMeta.get(EntryAccountMeta_.unpinnedAt)), queryMeta.get(EntryAccountMeta_.unpinnedAt))
                                .otherwise(queryRoot.get(Entry_.createdAt))
                )
        )

        query.where(*conditions.toTypedArray())

        val createQuery = entityManager.createQuery(query)
        createQuery.maxResults = limit

        val result =
                createQuery.resultList.filter { !it.get(1, Boolean::class.javaObjectType) && !it.get(2, Boolean::class.javaObjectType) }
                        .map { it.get(0, Entry::class.java) }

        if (result.isEmpty()) {
            return listOf()
        }


        // FIND EntryAccountMeta for the given items

        val q2 = cb.createQuery(EntryAccountMeta::class.java)
        val r2 = q2.from(EntryAccountMeta::class.java)
        q2.select(r2)
        q2.where(
                r2.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.entry).get(Entry_.id).`in`(result.map { it.id }),
                cb.equal(r2.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.account), account)
        )
        val metas = entityManager.createQuery(q2).resultList
        val metasMap = metas.map { it.id.entry to it }.toMap()

        return result.map { EntryWithMetaView(it, metasMap[it]) }
    }

    override fun findUserIndependentView(source: Source): List<Entry> {
        val cb = entityManager.criteriaBuilder
        val query = cb.createQuery(Entry::class.java)
        val queryRoot = query.from(Entry::class.java)

        query.select(queryRoot)

        val conditions = mutableListOf<Predicate>(
                cb.equal(queryRoot.get(Entry_.source), source)
        )

        query.orderBy(cb.desc(queryRoot.get(Entry_.createdAt)))
        query.where(*conditions.toTypedArray())

        return entityManager.createQuery(query).resultList
    }

    /**
     * Querying entry with meta view according to the following rules:
     * - always show pinned items and show them on top (the latest item to
     *   be pinned at the end of the list of pinned entries)
     * - hide read items if hideReadIfNotPinned is true
     * - hide items that are older than the noOlderThan parameter
     * - return max n items, where n = limit parameter
     * - the other unpinned items are ordered chronologically according to
     *   their creation date or unpinned date
     *
     *
     * Useful for querying data to show at dashboard.
     */
    override fun findForView(source: Source, account: Account, hideReadIfNotPinned: Boolean, limit: Int, noOlderThan: LocalDateTime?): List<EntryWithMetaView> {
        val cb = entityManager.criteriaBuilder
        val query = cb.createTupleQuery()
        val queryRoot = query.from(Entry::class.java)


        // left join on metas with condition for metas only for the given account
        // so that we can target them if present in further where conditions
        val queryMeta = queryRoot.join(Entry_.accountMetas, JoinType.LEFT)
        queryMeta.on(cb.equal(queryMeta.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.account), account))

        query.select(
                cb.tuple(
                        queryRoot,
                        cb.isNotNull(queryMeta.get(EntryAccountMeta_.readAt)),
                        cb.isNotNull(queryMeta.get(EntryAccountMeta_.pinnedAt)))
        )


        val conditions = mutableListOf<Predicate>(
                cb.equal(queryRoot.get(Entry_.source), source)
        )

        if (noOlderThan != null) {
            conditions.add(
                    cb.or(
                            cb.isNotNull(queryMeta.get(EntryAccountMeta_.pinnedAt)),
                            cb.greaterThanOrEqualTo(queryRoot.get(Entry_.createdAt), noOlderThan)
                    )
            )
        }

        query.orderBy(
                // pinned items on top
                cb.asc(
                        cb.selectCase<Int>()
                                .`when`(cb.isNotNull(queryMeta.get(EntryAccountMeta_.pinnedAt)), 0)
                                .otherwise(1)
                ),
                // extracted from the condition bellow as we need ASC sorting here
                // for unpinned items pinnedAt is null, so everything works perfectly
                cb.asc(queryMeta.get(EntryAccountMeta_.pinnedAt)),
                // if unpinnedAt is not null, sort by it, otherwise by createdAt
                cb.desc(
                        cb.selectCase<LocalDateTime>()
                                .`when`(cb.isNotNull(queryMeta.get(EntryAccountMeta_.unpinnedAt)), queryMeta.get(EntryAccountMeta_.unpinnedAt))
                                .otherwise(queryRoot.get(Entry_.createdAt))
                )
        )

        query.where(*conditions.toTypedArray())

        val createQuery = entityManager.createQuery(query)
        createQuery.maxResults = limit
        var rawResult = createQuery.resultList

        if (hideReadIfNotPinned) {
            rawResult =
                    rawResult.filter { it.get(2, Boolean::class.javaObjectType) || !it.get(1, Boolean::class.javaObjectType) }
        }

        val result = rawResult.map { it.get(0, Entry::class.java) }

        if (result.isEmpty()) {
            return listOf()
        }

        // FIND EntryAccountMeta for the given items

        val q2 = cb.createQuery(EntryAccountMeta::class.java)
        val r2 = q2.from(EntryAccountMeta::class.java)
        q2.select(r2)
        q2.where(
                r2.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.entry).get(Entry_.id).`in`(result.map { it.id }),
                cb.equal(r2.get(EntryAccountMeta_.id).get(EntryAccountMetaId_.account), account)
        )
        val metas = entityManager.createQuery(q2).resultList
        val metasMap = metas.map { it.id.entry to it }.toMap()

        return result.map { EntryWithMetaView(it, metasMap[it]) }
    }

    override fun saveMeta(entryAccountMeta: EntryAccountMeta) {
        entityManager.persist(entryAccountMeta)
    }
}