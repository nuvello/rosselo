package com.rosselo.server.repository.rss

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Category
import com.rosselo.server.model.entity.rss.Feed
import com.rosselo.server.model.entity.rss.Source
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
interface FeedRepository : JpaRepository<Feed, UUID> {
    fun findByAccount(account: Account): List<Feed>
    fun findByCategoryOrderByPosition(category: Category): List<Feed>
    fun findByCategory(category: Category): List<Feed>
    fun findBySourceAndAccount(source: Source, account: Account): List<Feed>

    @Query("SELECT COUNT(id) FROM Feed WHERE account = :account")
    fun findCount(@Param("account") account: Account): Int

    @Query("SELECT MAX(position) FROM Feed WHERE category = :category AND column = :column")
    fun findMaxPosition(
            @Param("category") category: Category,
            @Param("column") column: Int
    ): Int?

    @Query("FROM Feed WHERE account = :account AND column >= :column")
    fun findByAccountAndColumnGreaterThanEqual(account: Account, column: Int): List<Feed>

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Feed SET position = position - 1 WHERE category = :category" +
            " AND column = :column AND position > :biggerThan")
    fun decreasePositionIfBiggerThan(
            @Param("category") category: Category,
            @Param("column") column: Int,
            @Param("biggerThan") biggerThan: Int
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Feed SET position = position + 1 WHERE category = :category" +
            " AND column = :column AND position > :biggerThan")
    fun increasePositionIfBiggerThan(
            @Param("category") category: Category,
            @Param("column") column: Int,
            @Param("biggerThan") biggerThan: Int
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Feed SET source.id = :to WHERE source.id = :from")
    fun replaceSource(
            @Param("from") from: UUID,
            @Param("to") to: UUID
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Feed SET position = position - 1 WHERE category = :category" +
            " AND column = :column AND position BETWEEN :between1 AND :between2")
    fun decreasePositionBetween(
            @Param("category") category: Category,
            @Param("column") column: Int,
            @Param("between1") between1: Int,
            @Param("between2") between2: Int
    )

    @Modifying(flushAutomatically = true)
    @Query("UPDATE Feed SET position = position + 1 WHERE category = :category" +
            " AND column = :column AND position BETWEEN :between1 AND :between2")
    fun increasePositionBetween(
            category: Category,
            column: Int,
            between1: Int,
            between2: Int
    )
}