package com.rosselo.server.repository

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.AccountPlan
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Repository
interface AccountPlanRepository : JpaRepository<AccountPlan, UUID> {
    @Query("FROM AccountPlan accountPlan WHERE accountPlan.account = :account" +
            " AND (accountPlan.validTo = NULL OR accountPlan.validTo > current_timestamp)")
    fun findActiveFor(@Param("account") account: Account): AccountPlan?
}