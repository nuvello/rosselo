package com.rosselo.server.repository.rss

import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceType

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
interface CustomizedSourceRepository {
    fun findToBeRefreshedNotInQueue(): List<Source>
    fun merge(source: Source): Source
    fun findByUrlSearch(
            url: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?
    ): List<Source>

    fun findByFulltextSearchExact(
            query: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source>

    fun findByFulltextSearch(
            query: String,
            queryLang: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source>
}