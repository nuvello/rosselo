package com.rosselo.server.repository.rss

import com.rosselo.server.model.EntryWithMetaView
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Entry
import com.rosselo.server.model.entity.rss.EntryAccountMeta
import com.rosselo.server.model.entity.rss.Source
import java.time.LocalDateTime

interface CustomizedEntryRepository {
    fun findUnreadUnpinned(
            source: Source,
            account: Account,
            limit: Int,
            noNewerThan: LocalDateTime?
    ): List<EntryWithMetaView>

    fun findUserIndependentView(source: Source): List<Entry>
    fun findForView(
            source: Source,
            account: Account,
            hideReadIfNotPinned: Boolean,
            limit: Int,
            noOlderThan: LocalDateTime?
    ): List<EntryWithMetaView>

    fun saveMeta(entryAccountMeta: EntryAccountMeta)
    fun keepOnlyNewestPerSource(source:Source, maxPerSource: Int)
}