package com.rosselo.server.repository.rss.impl

import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.model.entity.rss.Source_
import com.rosselo.server.repository.rss.CustomizedSourceRepository
import com.rosselo.server.utils.network.URIUtil
import java.time.LocalDateTime
import javax.persistence.EntityManager

class CustomizedSourceRepositoryImpl(
        private val entityManager: EntityManager
) : CustomizedSourceRepository {
    override fun findToBeRefreshedNotInQueue(): List<Source> {
        // todo check preformance
        val cb = entityManager.criteriaBuilder

        val query = cb.createQuery(Source::class.java)
        val root = query.from(Source::class.java)
        query.select(root)
        query.where(
                cb.or(
                        // IF NO SUBSCRIBERS
                        cb.and(
                                cb.equal(root.get(Source_.subscribers), 0),
                                cb.or(
                                        // AND NO ERROR, EVERY 30 DAYS
                                        cb.and(
                                                cb.equal(root.get(Source_.errorsSinceLastSuccessful), 0),
                                                cb.or(
                                                        cb.isNull(root.get(Source_.lastExtraScrapAt)), // todo review whether I want this here
                                                        cb.lessThanOrEqualTo(root.get(Source_.lastFetchAt), LocalDateTime.now().minusDays(30))
                                                )
                                        ),
                                        // IF BETWEEN 1-4 ERRORS, TRY DAILY
                                        cb.and(
                                                cb.between(root.get(Source_.errorsSinceLastSuccessful), 1, 4),
                                                cb.lessThanOrEqualTo(root.get(Source_.lastFetchAt), LocalDateTime.now().minusDays(1))
                                        ),
                                        // IF MORE, EVERY 40 DAYS
                                        cb.and(
                                                cb.greaterThan(root.get(Source_.errorsSinceLastSuccessful), 5),
                                                cb.lessThanOrEqualTo(root.get(Source_.lastFetchAt), LocalDateTime.now().minusDays(40))
                                        )
                                )
                        ),
                        // IF HAS SUBCRIBERS
                        cb.and(
                                cb.greaterThan(root.get(Source_.subscribers), 0),
                                cb.lessThanOrEqualTo(root.get(Source_.lastFetchAt),
                                        cb.function("TO_TIMESTAMP", LocalDateTime::class.java,
                                                // AND NO ERROR
                                                cb.selectCase<LocalDateTime>().`when`(
                                                        cb.equal(root.get(Source_.errorsSinceLastSuccessful), 0),
                                                        cb.selectCase<LocalDateTime>().`when`(
                                                                // MORE THAN 10 SUBSCRIBERS EVERY 4 MINUTES
                                                                cb.greaterThan(root.get(Source_.subscribers), 10),
                                                                LocalDateTime.now().minusMinutes(4)
                                                        )
                                                                // OTHERWISE EVERY 10 MINUTES
                                                                .otherwise(LocalDateTime.now().minusMinutes(10))
                                                ).`when`(
                                                        cb.equal(root.get(Source_.errorsSinceLastSuccessful), 1),
                                                        LocalDateTime.now().minusMinutes(2)
                                                ).`when`(
                                                        cb.equal(root.get(Source_.errorsSinceLastSuccessful), 2),
                                                        LocalDateTime.now().minusMinutes(30)
                                                ).`when`(
                                                        cb.equal(root.get(Source_.errorsSinceLastSuccessful), 3),
                                                        LocalDateTime.now().minusMinutes(60 * 3)
                                                ).`when`(
                                                        cb.equal(root.get(Source_.errorsSinceLastSuccessful), 4),
                                                        LocalDateTime.now().minusMinutes(60 * 24)
                                                ).otherwise(
                                                        LocalDateTime.now().minusMinutes(60 * 24 * 2)
                                                ), cb.literal("YYYY-MM-DD\"T\"HH24:MI:SS\"Z\""))
                                )
                        )
                ),
                cb.isNull(root.get(Source_.inQueueSince))
        )


        return entityManager.createQuery(query).resultList
    }

    override fun merge(source: Source): Source {
        return entityManager.merge(source)
    }

    override fun findByFulltextSearchExact(
            query: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source> {
        return findByFulltextSearchInternal(
                query,
                "simple",
                true,
                minSubscribers,
                maxErrorsSinceLastSuccessful,
                showHiddenFromSearch,
                limit,
                offset,
                tlds,
                languages,
                channelTypes
        )
    }

    override fun findByFulltextSearch(
            query: String,
            queryLang: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source> {
        return findByFulltextSearchInternal(
                query,
                queryLang,
                false,
                minSubscribers,
                maxErrorsSinceLastSuccessful,
                showHiddenFromSearch,
                limit,
                offset,
                tlds,
                languages,
                channelTypes
        )
    }

    private fun findByFulltextSearchInternal(
            query: String,
            queryLang: String,
            useSimpleForSource: Boolean,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source> {
        val parameters = HashMap<String, Any>()
        val conditions = ArrayList<String>()

        if (tlds != null) {
            conditions.add("tld IN (:tlds)")
            parameters["tlds"] = tlds
        }

        if (languages != null) {
            conditions.add("language IN (:languages)")
            parameters["languages"] = languages
        }

        if (channelTypes != null) {
            conditions.add("type IN (:types)")
            parameters["types"] = channelTypes.map { it.name }
        }

        if (!showHiddenFromSearch) {
            conditions.add("NOT hidden_in_search")
        }

        val sqlQuery = entityManager.createNativeQuery(
                """
                    SELECT * FROM source 
                    WHERE 
                    ${conditions.joinToString(" AND ")}
                    ${if (conditions.isNotEmpty()) " AND " else ""}
                    errors_since_last_successful < :max_errors_since_last_successful AND
                    total_subscribers > :min_subscribers AND
                    source_to_tsvector(${if (useSimpleForSource) "'simple'" else "lang"}, title, tags, categories, topics, description) @@ to_tsquery(:query_lang, unaccent(:query)) 
                    ORDER BY ts_rank(source_to_tsvector(${if (useSimpleForSource) "'simple'" else "lang"}, title, tags, categories, topics, description), to_tsquery(:query_lang, unaccent(:query))) + rank_source(total_subscribers, score) DESC
                    """.trimIndent(),
                Source::class.java
        )
        sqlQuery.maxResults = limit
        sqlQuery.firstResult = offset

        parameters["query_lang"] = queryLang
        parameters["max_errors_since_last_successful"] = maxErrorsSinceLastSuccessful
        parameters["min_subscribers"] = minSubscribers
        val processedQuery = query
                .trim()
                // remove symbols & non-letter/digit characters
                .replace("[^\\p{LD} ]".toRegex(), " ")
                // replace double spaces
                .replace(" +".toRegex(), " ")

        if (processedQuery.isEmpty()) {
            throw IllegalArgumentException("Cannot process empty query.")
        }

        parameters["query"] = processedQuery
                .split(' ')
                .filter { it.isNotEmpty() }
                // turn the words into a query ("a bc d" into "a:* bc:* d:*")
                .joinToString(" & ") { "$it:*" }

        parameters.forEach { (name, value) -> sqlQuery.setParameter(name, value) }

        @Suppress("UNCHECKED_CAST")
        return sqlQuery.resultList as List<Source>
    }

    override fun findByUrlSearch(
            url: String,
            minSubscribers: Int,
            maxErrorsSinceLastSuccessful: Int,
            showHiddenFromSearch: Boolean,
            limit: Int,
            offset: Int,
            tlds: List<String>?,
            languages: List<String>?
    ): List<Source> {
        // todo duplicate code
        val parameters = HashMap<String, Any>()
        val conditions = ArrayList<String>()


        if (tlds != null) {
            conditions.add("tld IN (:tlds)")
            parameters["tlds"] = tlds
        }

        if (languages != null) {
            conditions.add("language IN (:languages)")
            parameters["languages"] = languages
        }


        if (!showHiddenFromSearch) {
            conditions.add("NOT hidden_in_search")
        }

        val urlEscaped = URIUtil.removeSchemeAndWww(url).replace("\\", "\\\\")
                .replace("\b", "\\b")
                .replace("\n", "\\n")
                .replace("\r", "\\r")
                .replace("\t", "\\t")
                .replace("\\x1A", "\\Z")
                .replace("\\x00", "\\0")
                .replace("'", "\\'")
                .replace("\"", "\\\"")
                .toLowerCase()

        // we don't use parametric queries because that prevents the use of index in PostgreSQL (
        // PSQL as the only DB assumes leading % when building execution plan, so it leaves out
        // the use of index)
        conditions.add("regexp_replace(link, '^(https?://)?(www\\.)?', '') LIKE '$urlEscaped%'")

        val sqlQuery = entityManager.createNativeQuery(
                """
                    SELECT * FROM source 
                    WHERE 
                    ${conditions.joinToString(" AND ")}
                    ${if (conditions.isNotEmpty()) " AND " else ""}
                    errors_since_last_successful < :max_errors_since_last_successful AND
                    total_subscribers > :min_subscribers AND
                    type = :type
                    ORDER BY rank_source(total_subscribers, score) DESC
                    """.trimIndent(),
                Source::class.java
        )
        sqlQuery.maxResults = limit
        sqlQuery.firstResult = offset

        parameters["type"] = SourceType.RSS_URL.name
        parameters["max_errors_since_last_successful"] = maxErrorsSinceLastSuccessful
        parameters["min_subscribers"] = minSubscribers

        parameters.forEach { (name, value) -> sqlQuery.setParameter(name, value) }

        @Suppress("UNCHECKED_CAST")
        return sqlQuery.resultList as List<Source>
    }
}
