package com.rosselo.server.repository

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.LimitedAction
import com.rosselo.server.model.entity.LimitedActionLog
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime


/**
 * Repository to be used for manipulation with [LimitedActionLog].
 *
 * @author Juraj Mlich <jurajmlich></jurajmlich>@gmail.com>
 */
@Repository
interface LimitedActionLogRepository : CrudRepository<LimitedActionLog, Long> {
    @Query("SELECT count(log) FROM LimitedActionLog log WHERE log.limitedAction = :limitedAction AND log.ipAddress = :ipAddress AND log.creationDateTime > :creationDateTimeAfter")
    fun count(
            @Param("limitedAction") limitedAction: LimitedAction,
            @Param("ipAddress") ipAddress: String,
            @Param("creationDateTimeAfter") creationDateTimeAfter: LocalDateTime
    ): Int

    @Query("SELECT count(log) FROM LimitedActionLog log WHERE log.limitedAction = :limitedAction AND log.account = :account AND log.creationDateTime > :creationDateTimeAfter")
    fun count(
            @Param("limitedAction") limitedAction: LimitedAction,
            @Param("account") account: Account,
            @Param("creationDateTimeAfter") creationDateTimeAfter: LocalDateTime
    ): Int
    @Modifying(flushAutomatically = true)
    fun deleteAllByCreationDateTimeBefore(
            olderThan: LocalDateTime
    )

}
