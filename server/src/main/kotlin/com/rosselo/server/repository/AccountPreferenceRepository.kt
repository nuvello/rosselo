package com.rosselo.server.repository

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.AccountPreference
import com.rosselo.server.model.entity.AccountPreferenceId
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Repository
interface AccountPreferenceRepository : JpaRepository<AccountPreference, AccountPreferenceId> {
    @Query("FROM AccountPreference preference WHERE preference.id.account = :account")
    fun findFor(@Param("account") account: Account): List<AccountPreference>
}