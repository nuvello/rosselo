package com.rosselo.server.repository;

import com.rosselo.server.model.entity.LimitedAction;
import com.rosselo.server.model.entity.LimitedActionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Repository to be used for manipulation with {@link LimitedAction}.
 *
 * @author Juraj Mlich <jurajmlich@gmail.com>
 */
@Repository
public interface LimitedActionRepository extends CrudRepository<LimitedAction, Long> {
    Optional<LimitedAction> findByType(@Param("type") LimitedActionType type);
}
