package com.rosselo.server.repository

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.PasswordResetRequest
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDateTime
import java.util.*

/**
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Repository
interface PasswordResetRepository : JpaRepository<PasswordResetRequest, UUID> {
    fun findByAccountAndConfirmTokenHash(account: Account, confirmTokenHash: String): PasswordResetRequest?
    @Modifying(flushAutomatically = true)
    fun deleteAllByCreatedAtBefore(
            olderThan: LocalDateTime
    )
}