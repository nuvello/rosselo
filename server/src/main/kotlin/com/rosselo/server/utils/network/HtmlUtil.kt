package com.rosselo.server.utils.network

import org.jsoup.nodes.Document
import java.net.URI

class HtmlUtil {
    companion object {
        fun getBaseUri(document: Document): URI? {
            val baseEl = document.selectFirst("base")

            return if (baseEl != null && baseEl.attr("href").isNotEmpty()) {
                URI.create(baseEl.attr("href"))
            } else {
                null
            }
        }
    }
}