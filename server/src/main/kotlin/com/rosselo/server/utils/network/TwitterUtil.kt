package com.rosselo.server.utils.network

import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.HttpClientBuilder

class TwitterUtil {
    fun checkIfNicknameExists(nickname: String): Boolean {
        val context = HttpClientContext.create()


        val response = HttpClientBuilder.create().build().execute(
                HttpGet("https://twitter.com/" + EncodingUtil.encodeURIComponent(nickname)),
                context
        )

        if (response.statusLine.statusCode > 299) {
            return false
        }

        return context.redirectLocations == null || context.redirectLocations.isEmpty()
    }
}
