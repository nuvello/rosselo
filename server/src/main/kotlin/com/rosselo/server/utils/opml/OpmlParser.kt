package com.rosselo.server.utils.opml

import org.w3c.dom.Element
import org.w3c.dom.Node
import java.io.ByteArrayInputStream
import javax.xml.parsers.DocumentBuilderFactory

class OpmlParser {
    fun parse(opmlFile: ByteArray): List<OpmlCategory> {
        val document = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(ByteArrayInputStream(opmlFile))

        val categories = HashMap<String, OpmlCategory>()

        document.documentElement.normalize()

        val body = document.getElementsByTagName("body").item(0)

        for (i in 0 until body.childNodes.length) {
            val item = body.childNodes.item(i)

            if (item.nodeType == Node.ELEMENT_NODE && item is Element) {
                if (item.hasAttribute("type") && item.getAttribute("type") == "rss") {
                    parseFeed(item, "Uncategorized", categories)
                } else {
                    val categoryName = item.getAttribute("text")

                    for (k in 0 until item.childNodes.length) {
                        val feed = item.childNodes.item(k)

                        if (feed.nodeType == Node.ELEMENT_NODE && feed is Element) {
                            if (feed.hasAttribute("type") && feed.getAttribute("type") == "rss") {
                                parseFeed(feed, categoryName, categories)
                            }
                        }
                    }
                }
            }
        }

        return categories.values.toList()
    }

    private fun parseFeed(item: Element, category: String, categories: HashMap<String, OpmlCategory>) {
        if (!categories.containsKey(category)) {
            categories[category] = OpmlCategory(category)
        }

        categories[category]!!.feeds.add(
                OpmlFeed(
                        item.getAttribute("text"),
                        item.getAttribute("xmlUrl")
                )
        )
    }
}