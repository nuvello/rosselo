package com.rosselo.server.utils.opml

data class OpmlCategory(
        val name: String,
        val feeds: MutableList<OpmlFeed> = ArrayList()
)