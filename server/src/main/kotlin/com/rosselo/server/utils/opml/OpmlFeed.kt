package com.rosselo.server.utils.opml

data class OpmlFeed(
        public val name: String,
        public val url: String
)