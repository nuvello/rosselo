package com.rosselo.server.utils

import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.model.entity.Account
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.util.*

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class JwtTokenGenerator @Autowired constructor(
        private val rosseloConfig: RosseloConfig
) {
    fun generate(account: Account): String {
        return Jwts.builder()
                .setSubject(account.id.toString())
                .setExpiration(Date(System.currentTimeMillis() + 1000 * 60 * 60 * 24 * 30L)) // 30 days
                .signWith(SignatureAlgorithm.HS512, rosseloConfig.jwt.secret)
                .compact()
    }
}