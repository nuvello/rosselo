package com.rosselo.server.utils

inline fun catchAndIgnore(action: () -> Unit) {
    try {
        action()
    } catch (e: Exception) {
    }
}