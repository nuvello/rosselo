package com.rosselo.server.utils.network

import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.service.rss.scrapper.downloaders.RssDownloader
import org.apache.http.HttpEntity
import org.apache.http.HttpStatus
import org.apache.http.ProtocolException
import org.apache.http.client.config.CookieSpecs
import org.apache.http.client.config.RequestConfig
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIUtils
import org.apache.http.entity.ContentType
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.util.Args
import org.apache.http.util.ByteArrayBuffer
import org.springframework.stereotype.Service
import java.io.IOException
import java.net.URI
import java.net.URISyntaxException
import java.nio.charset.Charset
import java.util.regex.Pattern
import kotlin.streams.toList

/**
 * A wrapper for [CloseableHttpClient] that is used whenever fetching any
 * HTTP url for [Source] purposes.
 *
 * @see RosseloHttpClient.downloadUri
 * @see RssDownloader
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class RosseloHttpClient constructor(
        private val httpClient: CloseableHttpClient
) {
    private companion object {
        private const val DEFAULT_USER_AGENT_HEADER = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36 Rosselobot"
        private const val GOOGLEBOT_USER_AGENT_HEADER = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
        private const val DEFAULT_BUFFER_SIZE = 4096
        private const val DEFAULT_MAX_SIZE = 5242880 * 2; // 10MB

        private val ipv4Regex = Pattern.compile("^((0|1\\\\d?\\\\d?|2[0-4]?\\\\d?|25[0-5]?|[3-9]\\\\d?)\\\\.){3}(0|1\\\\d?\\\\d?|2[0-4]?\\\\d?|25[0-5]?|[3-9]\\\\d?)$")
        private val ipv6Regex = Pattern.compile("^(([0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]+|::(ffff(:0{1,4})?:)?((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1?[0-9])?[0-9])\\.){3}(25[0-5]|(2[0-4]|1?[0-9])?[0-9]))$")
        private val allowedTlds: HashSet<String> = this::class.java.getResourceAsStream("/tlds.txt").bufferedReader().lines().toList().toHashSet()
        private val requestConfig = RequestConfig.custom().setRedirectsEnabled(false).setCookieSpec(CookieSpecs.STANDARD).build()
        private val contentTypeToMimeType = mapOf(
                Pair(ResultContentType.RSS, listOf(
                        Pair("application/rss+xml", null),
                        Pair("application/rdf+xml", 0.8),
                        Pair(ContentType.APPLICATION_ATOM_XML.mimeType, null),
                        Pair(ContentType.APPLICATION_XML.mimeType, 0.8),
                        Pair(ContentType.TEXT_XML.mimeType, 0.4)
                )),
                Pair(ResultContentType.HTML_OR_XHTML, listOf(
                        Pair(ContentType.TEXT_HTML.mimeType, null),
                        Pair(ContentType.APPLICATION_XHTML_XML.mimeType, null),
                        Pair(ContentType.APPLICATION_XML.mimeType, null)
                )),
                Pair(ResultContentType.IMAGE, listOf(
                        Pair(ContentType.IMAGE_JPEG.mimeType, null),
                        Pair(ContentType.IMAGE_PNG.mimeType, null),
                        Pair(ContentType.IMAGE_SVG.mimeType, null),
                        Pair("image/vnd.microsoft.icon", 0.9),
                        Pair("image/x-icon", 0.9),
                        Pair(ContentType.IMAGE_BMP.mimeType, 0.6)
                )),
                Pair(ResultContentType.OTHER, listOf(
                        Pair(ContentType.WILDCARD.mimeType, null)
                ))
        )
        private val contentTypeToAcceptHeader = contentTypeToMimeType.mapValues {
            it.value.sortedByDescending { type -> if (type.second == null) 1.0 else type.second }
                    .joinToString(",") { type ->
                        val preference = type.second
                        if (preference != null) {
                            "${type.first};q=${"%.1f".format(preference)}"
                        } else {
                            type.first
                        }
                    }
        }
    }

    enum class ResultContentType {
        RSS,
        HTML_OR_XHTML,
        IMAGE,
        OTHER
    }

    data class Result(
            /**
             * The originally requested URL.
             */
            val originalUrl: URI,
            /**
             * The URL that was lastly requested before the first temporary redirect.
             */
            val finalUrl: URI,
            val contentType: ResultContentType,
            val mimeType: String,
            val content: ByteArray,
            val charset: Charset?
    ) {
        fun contentAsString(): String {
            return String(content, charset ?: Charset.forName("UTF-8"))
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Result

            if (originalUrl != other.originalUrl) return false
            if (finalUrl != other.finalUrl) return false
            if (contentType != other.contentType) return false
            if (mimeType != other.mimeType) return false
            if (!content.contentEquals(other.content)) return false

            return true
        }

        override fun hashCode(): Int {
            var result = originalUrl.hashCode()
            result = 31 * result + finalUrl.hashCode()
            result = 31 * result + contentType.hashCode()
            result = 31 * result + mimeType.hashCode()
            result = 31 * result + content.contentHashCode()
            return result
        }

    }

    /**
     * Download the given URI. Follow redirects (max 5 attempts). Ensure the
     * URI is safe (see [RosseloHttpClient.ensureUriSafe]).
     *
     * @throws URISyntaxException in case the given or redirect URI is not in the
     * correct format
     * @throws IOException in case of an IO problem
     * @throws SecurityException in case the given or redirect URI is unsafe
     * @throws ProtocolException in case the redirect URI is not in the correct format
     */
    fun downloadUri(initialUriNotNormalized: URI, preferredContentType: ResultContentType, maxSize: Int? = DEFAULT_MAX_SIZE): Result {
        val initialUri = URIUtils.normalizeSyntax(initialUriNotNormalized)

        // the url to download next in the while loop below
        var nextUrlToDownload = initialUri
        // the url lastly requested before the first temporary redirect
        var lastPermanentUrl = initialUri
        // was already redirected temporarily?
        var redirectedTemporarily = false
        var attempt = 0

        while (true) {
            attempt++
            if (attempt > 5) {
                throw IOException("Max attempts reached.")
            }

            val request = buildRequest(nextUrlToDownload, preferredContentType)
            val response = httpClient.execute(request)
            val statusCode = response.statusLine.statusCode
            when {
                // SUCCESS
                statusCode < 300 -> {
                    val contentType = ContentType.getOrDefault(response.entity)
                    val body = toByteArray(response.entity, maxSize)

                    return buildResult(
                            initialUri,
                            lastPermanentUrl,
                            body,
                            contentType.mimeType,
                            contentType.charset ?: null
                    )
                }

                // PERMANENT REDIRECT
                statusCode == HttpStatus.SC_MOVED_PERMANENTLY -> {
                    nextUrlToDownload = URIUtils.normalizeSyntax(getRedirectURI(request, response))
                    if (!redirectedTemporarily) {
                        lastPermanentUrl = nextUrlToDownload
                    }
                }

                // TEMPORARY REDIRECT
                statusCode == HttpStatus.SC_MOVED_TEMPORARILY || statusCode == HttpStatus.SC_TEMPORARY_REDIRECT -> {
                    nextUrlToDownload = URIUtils.normalizeSyntax(getRedirectURI(request, response))
                    redirectedTemporarily = true
                }

                // REREQUEST THE SAME URL
                statusCode == HttpStatus.SC_SEE_OTHER -> {
                    // don't do anything, reexecute the same url
                }
                else -> throw IOException("Status code: $statusCode")
            }
        }
    }

    private fun buildResult(initialUri: URI, lastPermanentUrl: URI, body: ByteArray, mimeType: String, charset: Charset?): Result {
        val contentType = contentTypeToMimeType.entries.firstOrNull() {
            it.value.firstOrNull { type ->
                type.first == mimeType
            } != null
        }?.key ?: ResultContentType.OTHER

        return Result(initialUri, lastPermanentUrl, contentType, mimeType, body, charset)
    }

    private fun buildRequest(uri: URI, targetContentType: ResultContentType): HttpGet {
        ensureUriSafe(uri)

        val acceptHeader = contentTypeToAcceptHeader[targetContentType]
                ?: throw IllegalStateException("Unknown content type")

        val request = HttpGet(uri)
        request.addHeader("Accept", acceptHeader)
        request.addHeader("User-Agent", getUserAgent(uri))
        request.config = requestConfig

        return request
    }

    /**
     * The URI must have a scheme of http or https. It must not be a IPv4 or
     * IPv6 address and it's top level domain name must be one of the
     * [RosseloHttpClient.allowedTlds].
     */
    private fun ensureUriSafe(uri: URI) {
        if (uri.scheme != "http" && uri.scheme != "https") {
            throw SecurityException("Scheme must be http or https")
        }

        if (ipv4Regex.matcher(uri.host).matches() || ipv6Regex.matcher(uri.host).matches()) {
            throw SecurityException("IP addresses not allowed")
        }

        val parts = uri.host.split("\\.")
        if (allowedTlds.contains(parts[parts.size - 1])) {
            throw SecurityException("Tld not allowed")
        }
    }

    private fun getUserAgent(uri: URI): String {
        return if (uri.host.endsWith("tumblr.com") || uri.host.endsWith("techcrunch.com")) {
            GOOGLEBOT_USER_AGENT_HEADER
        } else {
            DEFAULT_USER_AGENT_HEADER
        }
    }

    private fun getRedirectURI(request: HttpGet, response: CloseableHttpResponse): URI {
        val locationHeader = response.getFirstHeader("location")
                ?: throw ProtocolException(
                        "Received redirect response " + response.statusLine + " but no location header"
                )

        try {
            return URIUtil.makeRelativeTo(
                    URI(locationHeader.value),
                    URI(request.requestLine.uri)
            )
        } catch (ex: URISyntaxException) {
            throw ProtocolException("Invalid redirect URI: ${locationHeader.value}", ex)
        } catch (ex: IllegalArgumentException) {
            throw ProtocolException("Invalid redirect URI: ${locationHeader.value}", ex)
        }
    }

    /**
     * Read the contents of an entity and return it as a byte array.
     *
     * @param entity the entity to read from=
     * @return byte array containing the entity content. May be null if
     * [HttpEntity.getContent] is null.
     * @throws IOException if an error occurs reading the input stream
     * @throws IllegalArgumentException if entity is null or if content length &gt; Integer.MAX_VALUE
     */
    @Throws(IOException::class)
    private fun toByteArray(entity: HttpEntity, maxSize: Int?): ByteArray {
        Args.notNull(entity, "Entity")
        val inStream = entity.content ?: throw IllegalStateException("error") // todo
        return inStream.use {
            // todo refactor
            Args.check(entity.contentLength <= Int.MAX_VALUE, "HTTP entity too large to be buffered in memory")
            Args.check(maxSize == null || entity.contentLength < maxSize, "HTTP entity larger than allowed value")

            var capacity = entity.contentLength.toInt()
            if (capacity < 0) {
                capacity = DEFAULT_BUFFER_SIZE
            }
            val buffer = ByteArrayBuffer(capacity)
            val tmp = ByteArray(DEFAULT_BUFFER_SIZE)
            var l: Int
            while (inStream.read(tmp).also { l = it } != -1) {
                buffer.append(tmp, 0, l)

                if (maxSize != null && buffer.length() > maxSize) {
                    throw IllegalStateException("HTTP entity larger than allowed value")
                }
            }
            buffer.toByteArray()
        }
    }
}