package com.rosselo.server.utils.network

import org.apache.http.client.utils.URIUtils
import java.net.URI

class URIUtil {
    companion object {
        fun removeSchemeAndWww(url: String): String {
            // todo regex
            return url.replace(Regex("^(https?://)?(www\\.)?"), "")
        }

        fun makeRelative(uri: URI, relativeTo: URI?, base: URI?): URI {
            return when {
                base != null -> makeRelativeToBase(uri, base)
                relativeTo != null -> makeRelativeTo(uri, relativeTo)
                else -> uri
            }
        }

        fun makeRelativeTo(uri: URI, base: URI): URI {
            val baseNormalized = base.normalize()
            return makeRelativeToBase(
                    uri,
                    if (!baseNormalized.path.endsWith("/")) baseNormalized.resolve(".") else {
                        base
                    }
            )
        }

        fun makeRelativeToBase(uriRaw: URI, relativeToRaw: URI): URI {
            val uri = uriRaw.normalize()
            val relativeTo = relativeToRaw.normalize()

            val scheme = uri.scheme ?: relativeTo.scheme
            val host = uri.host ?: relativeTo.host

            val path = if (uri.path.startsWith("/")) {
                uri.path
            } else {
                val relativeToNormalizedPath = if (relativeTo.path.endsWith("/")) {
                    relativeTo.path
                } else {
                    relativeTo.path + "/"
                }

                if (uri.path.startsWith("./")) {
                    relativeToNormalizedPath + uri.path.substring(2)
                } else {
                    relativeToNormalizedPath + uri.path
                }
            }

            val result = URI(scheme, uri.userInfo, host, uri.port, path, uri.query, uri.fragment)

            return URIUtils.normalizeSyntax(result)
        }
    }
}