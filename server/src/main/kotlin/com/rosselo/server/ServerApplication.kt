package com.rosselo.server

import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.worker.ScheduleRefreshTasksWorker
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.util.*
import javax.annotation.PostConstruct


@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@EnableWebMvc
@EnableAsync
class Application @Autowired constructor(
        private val rosseloConfig: RosseloConfig,
        private val scheduleRefreshTasksWorker: ScheduleRefreshTasksWorker
) {
    @PostConstruct
    fun initializeFeedRefresh() {
        if (rosseloConfig.worker.scheduleScrapTasks.enabled) {
            scheduleRefreshTasksWorker.start()
        }
    }
}

fun main(args: Array<String>) {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))

    runApplication<Application>(*args)
}
