package com.rosselo.server.service.rss

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.rss.Entry
import com.rosselo.server.model.entity.rss.EntryAccountMeta
import com.rosselo.server.model.entity.rss.EntryAccountMetaId
import com.rosselo.server.model.entity.rss.Feed
import com.rosselo.server.repository.rss.EntryRepository
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import javax.transaction.Transactional

/**
 * Domain service used for common operations on [Feed].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class EntryService(private val entryRepository: EntryRepository) {
    @Transactional
    fun setRead(entry: Entry, account: Account, read: Boolean, date: LocalDateTime) {
        val meta = entryRepository.findMeta(entry, account) ?: EntryAccountMeta(
                EntryAccountMetaId(entry, account),
                null,
                null,
                null
        )

        meta.readAt = if (read) date else null

        if (read && meta.pinnedAt != null) {
            meta.pinnedAt = null
            meta.unpinnedAt = date
        }

        entryRepository.saveMeta(meta)
    }

    @Transactional
    fun setPinned(entry: Entry, account: Account, pinned: Boolean, date: LocalDateTime?) {
        val meta = entryRepository.findMeta(entry, account) ?: EntryAccountMeta(
                EntryAccountMetaId(entry, account),
                null,
                null,
                null
        )

        meta.pinnedAt = if (pinned) date else null
        meta.unpinnedAt = if (pinned) null else date
        meta.readAt = if (pinned) null else meta.readAt

        entryRepository.saveMeta(meta)
    }
}