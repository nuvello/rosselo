package com.rosselo.server.service.rss

import com.github.slugify.Slugify
import com.rosselo.server.exceptions.BadRequestException
import com.rosselo.server.exceptions.ValidationException
import com.rosselo.server.model.FeedWithEntriesView
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.ConfigType
import com.rosselo.server.model.entity.rss.Category
import com.rosselo.server.repository.AccountPreferenceRepository
import com.rosselo.server.repository.rss.CategoryRepository
import com.rosselo.server.repository.rss.EntryRepository
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.service.ConfigService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import javax.transaction.Transactional

/**
 * Domain service used for common operations on [Category].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class CategoryService @Autowired constructor(
        private val feedService: FeedService,
        private val categoryRepository: CategoryRepository,
        private val feedRepository: FeedRepository,
        private val configService: ConfigService
) {
    fun findFeedsWithEntries(category: Category, entriesSince: LocalDateTime?): List<FeedWithEntriesView> {
        val feeds = feedRepository.findByCategory(category)

        // nicetodo: one query for all feeds at once
        return feeds.map {
            feedService.findWithEntries(it, entriesSince)
        }
    }

    @Transactional
    fun markAsRead(category: Category, account: Account, dateTime: LocalDateTime, entriesTill: LocalDateTime?) {
        // nicetodo: performance
        val feeds =
                feedRepository.findByCategoryOrderByPosition(category)

        feeds.forEach {
            feedService.markAsRead(it, account, dateTime, entriesTill)
        }
    }

    @Transactional(rollbackOn = [])
    fun create(account: Account, name: String): Category {
        val maxAmount = configService.getInt(ConfigType.MAX_NUMBER_OF_CATEGORIES_PER_ACCOUNT)
        if (categoryRepository.findCount(account) >= maxAmount) {
            throw ValidationException("Cannot create new groups, your number of groups would exceed the limit of $maxAmount")
        }

        var position = categoryRepository.findMaxPosition(account) ?: -1

        position++

        val category = Category(
                account = account,
                createdAt = LocalDateTime.now(),
                name = name,
                position = position,
                slug = Slugify().slugify(name)
        )

        this.saveCategoryEnsureSlugUnique(category)

        return category
    }

    private fun saveCategoryEnsureSlugUnique(category: Category) {
        var added = 2
        val originalSlug = category.slug

        while (categoryRepository.countByAccountAndSlug(category.account, category.slug) > 0) {
            category.slug = "$originalSlug-$added"
            added++
        }

        categoryRepository.saveAndFlush(category)
    }

    @Transactional
    fun rename(category: Category, newName: String): Category {
        category.name = newName
        category.slug = Slugify().slugify(newName)

        this.saveCategoryEnsureSlugUnique(category)

        return category
    }

    @Transactional
    fun delete(category: Category) {
        if (categoryRepository.countByAccount(category.account) == 1L) {
            throw BadRequestException()
        }

        feedRepository.findByCategory(category)
                .forEach {
                    feedService.delete(it)
                }

        categoryRepository.decreasePositionIfBiggerThan(
                category.account,
                category.position
        )
        categoryRepository.delete(category)
    }

    @Transactional
    fun moveToPosition(category: Category, toPosition: Int) {
        // not bigger than max + 1 and no less than 0
        val newPosition = Math.max(Math.min(
                toPosition,
                (categoryRepository.findMaxPosition(category.account) ?: -1) + 1
        ), 0)

        // nicetodo: abstraction together with feed
        if (category.position == newPosition) {
            return
        }


        if (category.position > newPosition) {
            // moving up
            categoryRepository.increasePositionBetween(
                    category.account,
                    newPosition,
                    category.position
            )
        } else {
            // moving down
            categoryRepository.decreasePositionBetween(
                    category.account,
                    category.position,
                    newPosition
            )
        }

        category.position = newPosition
        categoryRepository.save(category)
    }
}