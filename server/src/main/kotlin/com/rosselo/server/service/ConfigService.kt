package com.rosselo.server.service

import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.ConfigType
import com.rosselo.server.repository.ConfigRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

/**
 * Domain service used for common operations on [Account].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class ConfigService @Autowired constructor(
        private val configRepository: ConfigRepository
) {
    @Volatile
    private var configCache: Map<ConfigType, String>? = null

    fun get(configType: ConfigType): String {
        return getCache()[configType] ?: throw IllegalStateException(
                "$configType not found"
        )
    }

    fun getInt(configType: ConfigType): Int {
        return (getCache()[configType] ?: throw IllegalStateException(
                "$configType not found"
        )).toInt()
    }

    fun getNullableInt(configType: ConfigType): Int? {
        val value = (getCache()[configType] ?: throw IllegalStateException(
                "$configType not found"
        ))

        if (value.isEmpty() || value == "null") {
            return null
        }

        return value.toInt()
    }

    fun getLong(configType: ConfigType): Long {
        return (getCache()[configType] ?: throw IllegalStateException(
                "$configType not found"
        )).toLong()
    }

    fun getNullableLong(configType: ConfigType): Long? {
        val value = (getCache()[configType] ?: throw IllegalStateException(
                "$configType not found"
        ))

        if (value.isEmpty() || value == "null") {
            return null
        }

        return value.toLong()
    }

    fun getCache(): Map<ConfigType, String> {
        var cache = configCache

        if (cache == null) {
            cache =
                    configRepository.findAll().map { it.id to it.value }.toMap()
            configCache = cache
        }

        return cache
    }
}