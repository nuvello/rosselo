package com.rosselo.server.service.rss

import com.rosselo.server.exceptions.ValidationException
import com.rosselo.server.model.FeedWithEntriesView
import com.rosselo.server.model.entity.*
import com.rosselo.server.model.entity.rss.Category
import com.rosselo.server.model.entity.rss.EntryAccountMeta
import com.rosselo.server.model.entity.rss.EntryAccountMetaId
import com.rosselo.server.model.entity.rss.Feed
import com.rosselo.server.repository.AccountPreferenceRepository
import com.rosselo.server.repository.rss.EntryRepository
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.service.ConfigService
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional
import kotlin.math.max
import kotlin.math.min

/**
 * Domain service used for common operations on [Feed].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class FeedService(
        private val entryRepository: EntryRepository,
        private val feedRepository: FeedRepository,
        private val sourceRepository: SourceRepository,
        private val accountPreferenceRepository: AccountPreferenceRepository,
        private val configService: ConfigService
) {
    @Transactional
    fun markAsRead(feed: Feed, account: Account, dateTime: LocalDateTime, entriesTill: LocalDateTime?) {
        val max =
                configService.getInt(ConfigType.MAX_NUMBER_OF_ENTRIES_PER_FEED)

        val entries = entryRepository.findUnreadUnpinned(
                feed.source,
                feed.account,
                min(feed.maxAmountOfEntries ?: max, max),
                entriesTill
        )

        entries.forEach {
            if (it.meta == null) {
                val newMeta = EntryAccountMeta(
                        EntryAccountMetaId(it.entry, feed.account),
                        dateTime,
                        null,
                        null
                )

                entryRepository.saveMeta(newMeta)
            } else {
                it.meta.readAt = dateTime
                entryRepository.saveMeta(it.meta)
            }
        }
    }

    fun findWithEntries(it: Feed, entriesSince: LocalDateTime?): FeedWithEntriesView {
        val notOlderThan = if (it.noOlderThan != null) {
            var temp = LocalDateTime.now().minusDays(it.noOlderThan!!.toLong())

            if (entriesSince !== null && entriesSince.isAfter(temp)) {
                temp = entriesSince
            }

            temp
        } else {
            entriesSince
        }


        val max =
                configService.getInt(ConfigType.MAX_NUMBER_OF_ENTRIES_PER_FEED)

        val hideEntriesAfterReadAccountPreference =
                HideEntriesAfterReadPreference.valueOf((accountPreferenceRepository.findById(
                        AccountPreferenceId(AccountPreferenceType.HIDE_ENTRIES_AFTER_READ, it.account)
                ).orElseThrow { IllegalStateException() }).value) // todo message

        val hideEntriesAfterRead =
                when (hideEntriesAfterReadAccountPreference) {
                    HideEntriesAfterReadPreference.RESPECT_FEED_SETTINGS ->
                        it.hideEntriesAfterRead
                    HideEntriesAfterReadPreference.FALSE ->
                        false
                    HideEntriesAfterReadPreference.TRUE ->
                        true
                }

        val entries = entryRepository.findForView(
                it.source,
                it.account,
                hideEntriesAfterRead,
                min(it.maxAmountOfEntries ?: max, max),
                notOlderThan
        )

        return FeedWithEntriesView(
                it,
                entries
        )
    }

    @Transactional
    fun moveToColumn(feed: Feed, column: Int, toPosition: Int? = null) {
        val newPosition = if (toPosition != null) {
            var i = feedRepository.findMaxPosition(
                    feed.category,
                    column
            ) ?: -1

            if (feed.column != column) {
                i++
            }

            max(min(toPosition, i), 0)
        } else {
            (feedRepository.findMaxPosition(
                    feed.category,
                    column
            ) ?: -1) + 1
        }

        if (feed.column == column && feed.position == newPosition) {
            return
        }

        // if movement within the original column
        if (feed.column == column) {
            if (feed.position > newPosition) {
                // moving up
                feedRepository.increasePositionBetween(
                        feed.category,
                        feed.column,
                        newPosition,
                        feed.position
                )
            } else {
                // moving down
                feedRepository.decreasePositionBetween(
                        feed.category,
                        feed.column,
                        feed.position,
                        newPosition
                )
            }

            feed.position = newPosition
            feedRepository.save(feed)
        } else {
            // if move to another column
            feedRepository.decreasePositionIfBiggerThan(
                    feed.category,
                    feed.column,
                    feed.position
            )
            feedRepository.increasePositionIfBiggerThan(
                    feed.category,
                    column,
                    newPosition - 1
            )
            feed.column = column
            feed.position = newPosition
            feedRepository.save(feed)
        }
    }

    @Transactional
    fun moveToCategory(feed: Feed, category: Category) {
        if (feed.category == category) {
            return
        }

        feedRepository.decreasePositionIfBiggerThan(
                feed.category,
                feed.column,
                feed.position
        )

        val (column, newPosition) = determineColumnAndPositionForNewFeedIn(category)

        feed.category = category
        feed.column = column
        feed.position = newPosition

        feedRepository.save(feed)
    }

    @Transactional
    fun create(
            account: Account,
            category: Category,
            name: String,
            sourceId: UUID,
            hideEntriesAfterRead: Boolean,
            maxAmountOfItems: Int?,
            noOlderThan: Int?
    ): Feed {
        val maxAmount = configService.getInt(ConfigType.MAX_NUMBER_OF_FEEDS_PER_ACCOUNT)
        if (feedRepository.findCount(account) >= maxAmount) {
            throw ValidationException("Cannot create new feeds, your number of feeds would exceed the limit of $maxAmount")
        }

        val (column, newPosition) = determineColumnAndPositionForNewFeedIn(category)

        val source = sourceRepository.findByIdWithExclusiveLock(sourceId)
                ?: throw ValidationException("Invalid source")

        val feed = Feed(
                account = account,
                category = category,
                column = column,
                position = newPosition,
                createdAt = LocalDateTime.now(),
                maxAmountOfEntries = maxAmountOfItems,
                name = name,
                noOlderThan = noOlderThan,
                hideEntriesAfterRead = hideEntriesAfterRead,
                source = source
        )

        source.subscribers++
        feedRepository.save(feed)
        sourceRepository.save(source)

        return feed
    }

    private fun determineColumnAndPositionForNewFeedIn(category: Category): Pair<Int, Int> {
        val preference =
                (accountPreferenceRepository.findById(
                        AccountPreferenceId(AccountPreferenceType.GLOBAL_COLUMN_COUNT, category.account)
                ).orElseThrow { IllegalStateException() }).value.toInt() // todo message

        var column = 1
        var maxPosition: Int? = null

        // todo lock
        for (i in 1..preference) {
            val position = feedRepository.findMaxPosition(
                    category,
                    i
            ) ?: -1

            if (maxPosition == null || position < maxPosition) {
                column = i
                maxPosition = position
            }
        }

        val newPosition = if (maxPosition == null) {
            0
        } else {
            maxPosition + 1
        }
        return Pair(column, newPosition)
    }

    @Transactional
    fun update(
            account: Account,
            feed: Feed,
            name: String,
            sourceId: UUID,
            hideEntriesAfterRead: Boolean,
            maxAmountOfItems: Int?,
            noOlderThan: Int?
    ): Feed {
        if (feed.source.id != sourceId) {
            val originalSource = sourceRepository.findByIdWithExclusiveLock(feed.source.id)
                    ?: throw IllegalStateException("Feed changed under the hood " +
                            "when trying to modify it")
            originalSource.subscribers--
            sourceRepository.save(originalSource)

            feed.source = sourceRepository.findByIdWithExclusiveLock(sourceId)
                    ?: throw ValidationException("Invalid source")

            feed.source.subscribers++
            sourceRepository.save(feed.source)
        }

        feed.name = name
        feed.hideEntriesAfterRead = hideEntriesAfterRead
        feed.maxAmountOfEntries = maxAmountOfItems
        feed.noOlderThan = noOlderThan

        feedRepository.save(feed)

        return feed
    }


    @Transactional
    fun delete(feed: Feed) {
        feed.source = sourceRepository.findByIdWithExclusiveLock(feed.source.id)
                ?: throw ValidationException("Feed changed under the hood")

        feed.source.subscribers--

        sourceRepository.save(feed.source)

        feedRepository.delete(feed)
        feedRepository.decreasePositionIfBiggerThan(
                feed.category,
                feed.column,
                feed.position
        )
    }
}