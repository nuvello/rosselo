package com.rosselo.server.service

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.youtube.YouTube
import com.rosselo.server.config.RosseloConfig
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class YoutubeService(
        private val rosseloConfig: RosseloConfig
) {
    companion object {
        private val JSON_FACTORY: JsonFactory = JacksonFactory.getDefaultInstance()
    }

    private val service: YouTube

    init {
        val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
        service = YouTube.Builder(httpTransport, JSON_FACTORY, null)
                .setApplicationName("Rosselo server")
                .build()
    }

    fun getChannelId(accountName: String): String {
        val response = service.search()
                .list("id")
                .setQ(accountName)
                .setMaxResults(1)
                .setKey(rosseloConfig.youtubeApiKey)
                .execute()

        if (response.items.isEmpty()) {
            throw IllegalStateException("Account not found")
        }

        return response.items.first().id.channelId
    }
}