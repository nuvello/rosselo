package com.rosselo.server.service.rss.scrapper

import com.rosselo.server.service.rss.scrapper.downloaders.ParsedSource
import com.rosselo.server.service.utility.Image
import com.rosselo.server.service.utility.ImageService
import com.rosselo.server.service.utility.LanguageDetector
import com.rosselo.server.utils.Either
import com.rosselo.server.utils.catchAndIgnore
import com.rosselo.server.utils.network.HtmlUtil
import com.rosselo.server.utils.network.RosseloHttpClient
import com.rosselo.server.utils.network.URIUtil
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.net.URI

data class ExtraScrapError(val reason: Reason, val message: String? = null) {
    enum class Reason {
        UNABLE_TO_DOWNLOAD_LINK_PAGE,
        LINK_PAGE_WRONG_CONTENT_TYPE,
        UNABLE_TO_PARSE_LINK_PAGE,
        UNABLE_TO_FIND_ICONS,
        UNABLE_TO_DOWNLOAD_ICON,
        UNABLE_TO_RESIZE_ICON
    }

    override fun toString(): String {
        return "ExtraScrapError(reason=$reason, message=$message)"
    }
}

data class ExtraScrapResult(
        val smallIcon: Image? = null,
        val bigIcon: Image? = null,
        val language: String? = null,
        val description: String? = null,
        val errors: List<ExtraScrapError> = arrayListOf()
)

@Service
class ExtraScrapExecutor(
        private val rosseloHttpClient: RosseloHttpClient,
        private val imageService: ImageService,
        private val languageDetector: LanguageDetector
) {
    private companion object {
        private const val SMALL_ICON_MAX_SIZE = 50
        private const val BIG_ICON_MAX_SIZE = 200

        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    // todo exception handling

    fun execute(parsedSource: ParsedSource): ExtraScrapResult {
        // DOWNLOAD LINK PAGE
        val linkHtml = try {
            rosseloHttpClient.downloadUri(
                    URI(parsedSource.link),
                    RosseloHttpClient.ResultContentType.HTML_OR_XHTML
            )
        } catch (e: Exception) {
            logger.debug(e.message)
            e.printStackTrace() // todo nope
            return ExtraScrapResult(errors = listOf(ExtraScrapError(ExtraScrapError.Reason.UNABLE_TO_DOWNLOAD_LINK_PAGE)))
        }

        if (linkHtml.contentType != RosseloHttpClient.ResultContentType.HTML_OR_XHTML) {
            return ExtraScrapResult(errors = listOf(ExtraScrapError(ExtraScrapError.Reason.LINK_PAGE_WRONG_CONTENT_TYPE)))
        }

        // PARSE LINK PAGE
        val linkDocument = try {
            Jsoup.parse(linkHtml.contentAsString())
        } catch (e: Exception) {
            return ExtraScrapResult(errors = listOf(ExtraScrapError(ExtraScrapError.Reason.UNABLE_TO_PARSE_LINK_PAGE)))
        }

        var result = ExtraScrapResult()

        result = addIconsToResult(URI.create(parsedSource.link), linkDocument, result)
        result = addLanguageToResult(parsedSource, linkDocument, result)
        result = addDescriptionToResult(linkDocument, result)

        return result
    }

    private fun addDescriptionToResult(linkDocument: Document, result: ExtraScrapResult): ExtraScrapResult {
        val description = linkDocument
                .head()
                .select("meta[name='description'], meta[property='og:description']")
                .map { it.attr("content") }
                .firstOrNull { it.trim().isNotEmpty() }

        return if (description != null && description.length > 10) {
            result.copy(description = description)
        } else {
            result
        }
    }

    private fun addLanguageToResult(parsedSource: ParsedSource, linkDocument: Document, result: ExtraScrapResult): ExtraScrapResult {
        val texts = mutableListOf(
                parsedSource.title,
                parsedSource.description
        )

        catchAndIgnore {
            texts.addAll(linkDocument.head().select("meta[name='keywords'], meta[name='description']").map { it.attr("content") })
        }
        catchAndIgnore {
            texts.addAll(linkDocument.head().select("title,h1,h2,h3").map { it.text() })
        }
        catchAndIgnore {
            texts.addAll(parsedSource.items.take(12).map { it.title })
        }

        val text = texts.filterNotNull().take(12).joinToString(" ")

        return result.copy(language = if (text.length > 12) {
            languageDetector.detect(text)
        } else {
            LanguageDetector.UNKNOWN_LANGUAGE
        })
    }

    private fun addIconsToResult(linkUri: URI, linkDocument: Document, result: ExtraScrapResult): ExtraScrapResult {
        val base = HtmlUtil.getBaseUri(linkDocument)

        val errors = result.errors.toMutableList()

        val iconsEl: List<Element> = linkDocument
                .select("link[rel='icon'],link[rel='shortcut icon'],link[rel='apple-touch-icon']")
                .filter { it.attr("href").isNotEmpty() }

        // todo try /favicon.ico
        // todo retry?

        if (iconsEl.isEmpty()) {
            errors.add(ExtraScrapError(ExtraScrapError.Reason.UNABLE_TO_FIND_ICONS));
            return result.copy(errors = errors)
        }

        val iconsMappedBySize = iconsEl.associateBy {
            if (it.hasAttr("sizes")) {
                val sizes = it.attr("sizes").split("x", " ")

                sizes[0].toInt() * sizes[1].toInt()
            } else {
                // if without size, prefer apple-touch-icon
                when (it.attr("rel")) {
                    "apple-touch-icon" -> 2
                    "shortcut icon" -> 1
                    else -> 0
                }
            }
        }

        val sorted = iconsMappedBySize.keys.toList().sorted()
        val max = sorted.last()
        var min = sorted.first()

        for (i in sorted) {
            if (i > 50 * 50) {
                break;
            } else {
                min = i;
            }
        }

        fun process(element: Element, maxSize: Int): Image? {
            val downloadResult = downloadAndResizeIcon(
                    URIUtil.makeRelative(URI.create(element.attr("href")), linkUri, base),
                    maxSize
            )

            return when (downloadResult) {
                is Either.Left -> downloadResult.value
                is Either.Right -> {
                    errors.add(downloadResult.value)
                    null
                }
            }
        }

        val bigIcon = process(iconsMappedBySize[max] ?: error("impossible"), BIG_ICON_MAX_SIZE)
        val smallIcon = process(iconsMappedBySize[min] ?: error("impossible"), SMALL_ICON_MAX_SIZE)

        return result.copy(smallIcon = smallIcon, bigIcon = bigIcon)
    }

    private fun downloadAndResizeIcon(imageUri: URI, maxSize: Int): Either<Image, ExtraScrapError> {
        val image = try {
            imageService.download(imageUri)
        } catch (e: Exception) {
            return Either.Right(ExtraScrapError(ExtraScrapError.Reason.UNABLE_TO_DOWNLOAD_ICON, e.message))
        }

        return try {
            Either.Left(imageService.ensureMaxDimensions(
                    image,
                    maxSize
            ))
        } catch (e: Exception) {
            return Either.Right(ExtraScrapError(ExtraScrapError.Reason.UNABLE_TO_RESIZE_ICON, e.message))
        }
    }
}