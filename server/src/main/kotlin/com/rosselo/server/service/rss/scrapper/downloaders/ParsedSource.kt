package com.rosselo.server.service.rss.scrapper.downloaders

import com.rosselo.server.model.entity.rss.SourceType
import java.time.LocalDateTime

data class ParsedSource(
        val title: String,
        val description: String?,
        val link: String,
        val source: String,
        val type: SourceType,
        val items: List<ParsedSourceItem>,
        val dateOfFetch: LocalDateTime
)
