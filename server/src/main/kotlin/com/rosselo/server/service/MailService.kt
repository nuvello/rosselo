package com.rosselo.server.service

import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.model.entity.Account
import com.rosselo.server.model.entity.PasswordResetRequest
import com.sendgrid.Method
import com.sendgrid.Request
import com.sendgrid.SendGrid
import com.sendgrid.helpers.mail.Mail
import com.sendgrid.helpers.mail.objects.Email
import com.sendgrid.helpers.mail.objects.Personalization
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.stereotype.Service
import java.io.IOException
import java.time.Year


@Service
class MailService @Autowired constructor(
        private val taskExecutor: ThreadPoolTaskExecutor,
        private val rosseloConfig: RosseloConfig
) {
    fun sendResetPasswordEmail(passwordResetRequest: PasswordResetRequest, token: String) {
        val mail = Mail()
        mail.from = Email(rosseloConfig.email)
        mail.templateId = "d-073d27cf5b9d4a4ba083261a44c2ee18"

        val personalization = Personalization()
        personalization.addTo(Email(passwordResetRequest.account.email))
        personalization.addDynamicTemplateData("linkToResetPassword", "${rosseloConfig.domain}/set-password/${passwordResetRequest.account.email}/$token")
        personalization.addDynamicTemplateData("subject", "Rosselo - password reset")
        personalization.addDynamicTemplateData("currentYear", Year.now().value)

        mail.addPersonalization(personalization)

        send(mail)
    }

    fun sendEmailChangedEmail(account: Account, originalEmail: String) {
        val mail = Mail()
        mail.from = Email(rosseloConfig.email)
        mail.templateId = "d-5399bd41cc2543318fb96a5ac972966e"

        val personalization = Personalization()
        personalization.addTo(Email(originalEmail))
        personalization.addDynamicTemplateData("linkToLogin", "${rosseloConfig.domain}/login")
        personalization.addDynamicTemplateData("newEmail", account.email)
        personalization.addDynamicTemplateData("subject", "Rosselo - e-mail changed")
        personalization.addDynamicTemplateData("currentYear", Year.now().value)

        mail.addPersonalization(personalization)

        send(mail)
    }

    private fun send(mail: Mail) {
        taskExecutor.execute {
            val sg =
                    SendGrid(rosseloConfig.sendgridApiKey)
            val request = Request()
            try {
                request.method = Method.POST
                request.endpoint = "mail/send"
                request.body = mail.build()
                sg.api(request)
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
        }
    }
}