package com.rosselo.server.service.rss.scrapper.downloaders

import java.time.LocalDateTime

data class ParsedSourceItem(
        val link: String,
        val uniqueId: String,
        val title: String,
        val date: LocalDateTime?
)
