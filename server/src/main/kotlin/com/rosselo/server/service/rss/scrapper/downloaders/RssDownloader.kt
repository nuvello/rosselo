package com.rosselo.server.service.rss.scrapper.downloaders

import com.rometools.rome.feed.synd.SyndFeed
import com.rometools.rome.io.SyndFeedInput
import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceDownloadErrorType
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.utils.network.RosseloHttpClient
import com.rosselo.server.utils.network.URIUtil
import org.apache.commons.text.StringEscapeUtils
import org.jsoup.Jsoup
import org.springframework.web.util.UriComponentsBuilder
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import java.io.IOException
import java.io.StringReader
import java.net.ProtocolException
import java.net.URI
import java.net.URISyntaxException
import java.net.http.HttpTimeoutException
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.regex.Pattern
import javax.xml.parsers.DocumentBuilderFactory

/**
 * A class used for downloading RSS urls. Handles the complex business logic
 * such as finding the correct RSS url in case the provided url is an HTML
 * document.
 *
 * @see RssDownloader.download
 * @author Juraj Mlich <juraj@mlich.eu>
 */
class RssDownloader constructor(
        private val rosseloHttpClient: RosseloHttpClient,
        private val sourceRepository: SourceRepository
) {
    private companion object {
        private val unescapedAmpersandsPattern = Pattern.compile("&([^;\\\\W]*([^;\\\\w]|\$))")
        private val documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        private val rssCommonPaths = mutableListOf("feed", "rss", "atom.xml", "rss.xml", ".rss", "rss/index.xml")
    }

    /**
     * 1. Download the given URI.
     * 2. If it's an HTML document, try to find RSS link inside it and download
     * the link.
     * 3. If the link is not found inside the HTML document, try common RSS urls.
     * 4. If the result is still not a RSS, try to find [Source] with the [Source.link]
     * property equal to the provided URI. If found, download it's [Source.source]
     * 5. Fix unescaped ampersands in the given RSS document.
     * 6. Parse the document and build the result.
     *
     * @throws DownloadException in case of an error
     */
    fun download(initialUriRaw: String): ParsedSource {
        val now = LocalDateTime.now()
        try {
            val result = getResult(initialUriRaw)

            val source = InputSource(StringReader(fixUnescapedAmpersands(result.contentAsString())))
            val build = SyndFeedInput().build(documentBuilder.parse(source))

            return mapFeedToParsedSource(build, result.finalUrl.toString(), now)
        } catch (e: URISyntaxException) {
            throw DownloadException(SourceDownloadErrorType.URI, e)
        } catch (e: ProtocolException) {
            throw DownloadException(SourceDownloadErrorType.URI, e)
        } catch (e: SecurityException) {
            throw DownloadException(SourceDownloadErrorType.SECURITY, e)
        } catch (e: HttpTimeoutException) {
            throw DownloadException(SourceDownloadErrorType.TIMEOUT, e)
        } catch (e: IOException) {
            throw DownloadException(SourceDownloadErrorType.IO, e)
        } catch (e: SAXException) {
            throw DownloadException(SourceDownloadErrorType.CANNOT_PARSE, e)
        } catch (e: DownloadException) {
            throw e
        } catch (e: Exception) {
            throw DownloadException(SourceDownloadErrorType.OTHER, e)
        }
    }

    /**
     * Find a document with RSS and download it.
     *
     * @throws DownloadException in case of an errors
     * @see RosseloHttpClient.downloadUri for more exceptions that are thrown
     */
    private fun getResult(initialUriRaw: String): RosseloHttpClient.Result {
        var initialUri = URI(initialUriRaw)

        if (initialUri.scheme == null) {
            initialUri = URI.create("https://$initialUriRaw")
        }
        if (!initialUri.isAbsolute) {
            throw IllegalStateException("ERROR") // todo
        }

        var result = rosseloHttpClient.downloadUri(initialUri, RosseloHttpClient.ResultContentType.RSS)

        if (result.contentType == RosseloHttpClient.ResultContentType.HTML_OR_XHTML) {
            val newUri = getRssUrlFromHtml(result.contentAsString(), initialUriRaw)
            if (newUri != null) {
                result = rosseloHttpClient.downloadUri(
                        URIUtil.makeRelativeTo(newUri, initialUri),
                        RosseloHttpClient.ResultContentType.RSS
                )
            } else if (result.contentType != RosseloHttpClient.ResultContentType.RSS) {
                result = tryCommonUrls(initialUri)
                        ?: throw DownloadException(
                                "Could not determine RSS URL",
                                SourceDownloadErrorType.URI
                        )
            }
        }

        if (result.contentType != RosseloHttpClient.ResultContentType.RSS) {
            val source = sourceRepository.findByTypeAndLinkStartingWith(
                    SourceType.RSS_URL,
                    initialUri.toString()
            );

            if (source == null || source.source == initialUriRaw) {
                throw DownloadException("Could not determine RSS url", SourceDownloadErrorType.URI)
            }

            return getResult(source.source)
        }

        return result
    }

    private fun mapFeedToParsedSource(build: SyndFeed, finalUrl: String, buildDate: LocalDateTime): ParsedSource {
        // todo refactor
        if (build.title == null
                || build.title.trim().isEmpty()
                || (build.links.isEmpty() && build.link == null)
                || (build.link != null && build.link.isEmpty())
                || (build.link == null && build.links.isNotEmpty() && build.links.first().href.isEmpty())) {
            throw DownloadException("Title or link empty", SourceDownloadErrorType.CANNOT_PARSE)
        }

        val title = unescape(build.title).trim().capitalize()
        val description = if (build.description == null) null else unescape(build.description).trim().capitalize();

        val items = build.entries
                .filter {
                    it.title != null && it.title.trim().isNotEmpty() && ((it.link != null && it.link.trim().isNotEmpty()) || (it.links.isNotEmpty() && it.links.first().href.isNotEmpty()))
                }
                .map {
                    ParsedSourceItem(
                            it.link ?: it.links.first().href.trim(),
                            if (it.uri == null) it.link.trim() else it.uri.trim(),
                            unescape(it.title).trim(),
                            it.publishedDate?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDateTime()
                    )
                }
                .filter {
                    it.title.isNotEmpty() && it.link.isNotEmpty() && it.uniqueId.isNotEmpty()
                }

        return ParsedSource(
                title,
                description,
                build.link?.trim() ?: build.links.first().href.trim(),
                finalUrl,
                SourceType.RSS_URL,
                items,
                buildDate
        )
    }

    private fun unescape(text: String): String {
        // to unescape even text as crazy as Du Wei: Chinese ambassador to Israel &amp;amp;apos;found dead at home&amp;amp;apos;
        return StringEscapeUtils.unescapeXml(Jsoup.parse(StringEscapeUtils.unescapeHtml4(text)).text())
    }

    private fun fixUnescapedAmpersands(rss: String): String {
        var result = rss

        repeat(2) {
            result = unescapedAmpersandsPattern.matcher(result)
                    .replaceAll("&amp;$1")
        }

        return result
    }

    private fun getRssUrlFromHtml(body: String, relativeTo: String): URI? {
        try {
            val doc = Jsoup.parse(body)

            val linkElementWithRss = doc.selectFirst(
                    "link[rel='alternate'][type='application/rss+xml']"
            ) ?: return null

            val baseElement = doc.selectFirst("base")
            val newUrl = linkElementWithRss.attr("href")

            return if (newUrl.isNotEmpty()) {
                // rewrite using the base element if found
                if (baseElement != null && baseElement.attr("href").isNotEmpty()) {
                    URIUtil.makeRelativeToBase(
                            URI.create(newUrl),
                            URI.create(baseElement.attr("href"))
                    )
                } else {
                    URIUtil.makeRelativeTo(
                            URI.create(newUrl),
                            URI.create(relativeTo)
                    )
                }
            } else {
                null
            }
        } catch (e: Exception) {
            return null
        }
    }

    private fun tryCommonUrls(uri: URI): RosseloHttpClient.Result? {
        val toTry = LinkedHashSet<URI>()
        if (uri.host.endsWith("stackexchange.com") || uri.host.endsWith("stackoverflow.com")) {
            toTry.add(UriComponentsBuilder.fromUri(uri).replacePath("feeds/hot").build().toUri())
        } else if (uri.host.endsWith("reddit.com")) {
            toTry.add(UriComponentsBuilder.fromUri(uri).replacePath(uri.path + ".rss").build().toUri())
        }
        toTry.addAll(rssCommonPaths.map {
            UriComponentsBuilder.fromUri(uri).replacePath(it).build().toUri()
        })
        toTry.addAll(rssCommonPaths.map {
            UriComponentsBuilder.fromUri(uri).pathSegment(it).build().toUri()
        })


        toTry.forEach {
            try {
                val result = rosseloHttpClient.downloadUri(it, RosseloHttpClient.ResultContentType.RSS)

                if (result.contentType == RosseloHttpClient.ResultContentType.RSS) {
                    return result
                }
            } catch (exception: Exception) {
                // ignore
            }
        }

        return null
    }
}
