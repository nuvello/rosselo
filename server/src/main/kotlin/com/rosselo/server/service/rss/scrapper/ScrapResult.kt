package com.rosselo.server.service.rss.scrapper

import com.rosselo.server.model.entity.rss.SourceType
import java.io.Serializable
import java.util.*

data class ScrapResult(
        val status: ScrapResultStatus,
        val data: ScrapResultData?
):Serializable

enum class ScrapResultStatus {
    SUCCESS,
    ERROR
}

data class ScrapResultData (
        val sourceId: UUID,
        val sourceName: String,
        val sourceData: String,
        val sourceType: SourceType,
        val items: List<ScrapResultItem>?
):Serializable {
    override fun toString(): String {
        return "ScrapResultData(sourceId=$sourceId, sourceName='$sourceName', sourceData='$sourceData', sourceType=$sourceType)"
    }
}