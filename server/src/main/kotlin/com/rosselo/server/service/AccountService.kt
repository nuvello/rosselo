package com.rosselo.server.service

import com.rosselo.server.exceptions.ValidationException
import com.rosselo.server.model.entity.*
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.repository.AccountPlanRepository
import com.rosselo.server.repository.AccountPreferenceRepository
import com.rosselo.server.repository.AccountRepository
import com.rosselo.server.repository.PasswordResetRepository
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.service.rss.CategoryService
import com.rosselo.server.service.rss.FeedService
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.security.SecureRandom
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

/**
 * Domain service used for common operations on [Account].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class AccountService @Autowired constructor(
        private val accountRepository: AccountRepository,
        private val passwordResetRepository: PasswordResetRepository,
        private val passwordEncoder: PasswordEncoder,
        private val accountPreferenceRepository: AccountPreferenceRepository,
        private val accountPlanRepository: AccountPlanRepository,
        private val feedService: FeedService,
        private val feedRepository: FeedRepository,
        private val mailService: MailService,
        private val categoryService: CategoryService,
        private val sourceRepository: SourceRepository,
        private val configService: ConfigService
) : UserDetailsService {
    /**
     * Create a new account, send registration email, create new TRIAL
     * [AccountPlan] and set the account the default settings.
     *
     * @throws ValidationException if e-mail is already used
     */
    @Transactional
    fun create(email: String, password: String) {
        val account = Account(
                createdAt = LocalDateTime.now(),
                email = email,
                passwordHash = passwordEncoder.encode(password),
                lastSeenAt = LocalDateTime.now()
        )

        val plan = AccountPlan(
                account = account,
                type = AccountPlanType.TRIAL,
                validFrom = LocalDateTime.now(),
                validTo = LocalDateTime.now()
                        .plusDays(configService.getLong(ConfigType.TRIAL_PERIOD_IN_DAYS))
        )

        if (accountRepository.findByEmail(account.email) !== null) {
            throw ValidationException("E-mail already used.")
        }
        accountRepository.saveAndFlush(account)
        accountPlanRepository.save(plan)

        // create preferences for the newly created account
        accountPreferenceRepository.saveAll(
                AccountPreferenceType
                        .values()
                        .map { AccountPreference(AccountPreferenceId(it, account), it.defaultValue) }
        )

        // create default category
        categoryService.create(account, "General")
        val exampleCategory = categoryService.create(account, "Example")

        class Quadruple (
                val name: String,
                val sourceType: SourceType,
                val source: String,
                val maxAmountOfItems: Int
        )

        listOf(
                Quadruple("CNN World", SourceType.RSS_URL, "http://rss.cnn.com/rss/edition_world.rss", 15),
                Quadruple("Reddit World News", SourceType.RSS_URL, "https://www.reddit.com/r/worldnews/top.rss?t=day", 10),
                Quadruple("The Independent", SourceType.RSS_URL, "http://www.independent.co.uk/news/world/rss", 10),
                Quadruple("BBC World News", SourceType.RSS_URL, "http://feeds.bbci.co.uk/news/world/rss.xml", 12),
                Quadruple("@nytimes", SourceType.TWITTER_ACCOUNT_NAME, "nytimes", 5),
                Quadruple("Vox videos", SourceType.YOUTUBE_CHANNEL_ID, "UCLXo7UDZvByw2ixzpQCufnA", 10),
                Quadruple("The Guardian", SourceType.RSS_URL, "https://www.theguardian.com/world/rss", 10),
                Quadruple("@BillGates", SourceType.TWITTER_ACCOUNT_NAME, "BillGates", 5),
                Quadruple("The Guardian", SourceType.YOUTUBE_CHANNEL_ID, "UCHpw8xwDNhU9gdohEcJu4aA", 10)
        ).forEach {
            val findByTypeAndSource = sourceRepository.findByTypeAndSource(it.sourceType, it.source)
            feedService.create(
                    account,
                    exampleCategory,
                    it.name,
                    findByTypeAndSource!!.id,
                    true,
                    it.maxAmountOfItems,
                    30
            )
        }
    }

    /**
     * Create and save new [PasswordResetRequest] and send it to user through
     * [MailService.sendResetPasswordEmail].
     */
    @Transactional
    fun requestResetPassword(account: Account) {
        val now = LocalDateTime.now()
        val tokenRaw = ByteArray(35)
        SecureRandom().nextBytes(tokenRaw)
        val token =
                Base64.getUrlEncoder().withoutPadding().encodeToString(tokenRaw)
        val reset = PasswordResetRequest(
                account = account,
                createdAt = now,
                validUntil = now.plusHours(1),
                confirmTokenHash = DigestUtils.sha256Hex(token)
        )

        passwordResetRepository.save(reset)
        mailService.sendResetPasswordEmail(reset, token)
    }

    /**
     * Complete password reset request.
     */
    @Transactional
    fun resetPassword(account: Account, confirmToken: String, newPassword: String) {
        val now = LocalDateTime.now()

        val reset = passwordResetRepository.findByAccountAndConfirmTokenHash(
                account,
                DigestUtils.sha256Hex(confirmToken)
        ) ?: throw IllegalStateException("Invalid token.")

        if (reset.validUntil.isBefore(now) || reset.usedAt != null) {
            throw IllegalStateException("Invalid token.")
        }

        reset.usedAt = now
        account.passwordHash = passwordEncoder.encode(newPassword)

        accountRepository.save(account)
        passwordResetRepository.save(reset)
    }

    @Transactional
    fun delete(account: Account) {
        feedRepository.findByAccount(account)
                .forEach {
                    feedService.delete(it)
                }

        accountRepository.delete(account)
    }

    @Transactional
    fun updateEmail(account: Account, newAccountEmail: String) {
        val originalEmail = account.email

        if (accountRepository.findByEmail(newAccountEmail) !== null) {
            throw ValidationException("E-mail already used.")
        }

        account.email = newAccountEmail
        accountRepository.save(account)
        mailService.sendEmailChangedEmail(account, originalEmail)
    }

    @Transactional
    fun updatePassword(account: Account, oldPassword: String,
                       newPassword: String) {
        if (!passwordEncoder.matches(oldPassword, account.passwordHash)) {
            throw IllegalStateException("Incorrect password")
        }

        account.passwordHash = passwordEncoder.encode(newPassword)
        accountRepository.save(account)
    }

    @Transactional
    fun updatePreference(
            account: Account,
            preferenceType: AccountPreferenceType,
            newValue: String
    ) {
        val preference =
                accountPreferenceRepository.findById(AccountPreferenceId(preferenceType, account))
                        .orElseThrow { IllegalStateException("AccountPreference not found") }

        if (preferenceType == AccountPreferenceType.GLOBAL_COLUMN_COUNT
                && newValue.toInt() < preference.value.toInt()) {

            val feeds = feedRepository.findByAccountAndColumnGreaterThanEqual(
                    account,
                    newValue.toInt()
            )

            feeds.forEach {
                feedService.moveToColumn(
                        it,
                        newValue.toInt() - 1
                )
            }
        }

        preference.value = newValue
        accountPreferenceRepository.save(preference)
    }

    override fun loadUserByUsername(email: String): UserDetails {
        return accountRepository.findByEmail(email)
                ?: throw UsernameNotFoundException("Account not found")
    }
}