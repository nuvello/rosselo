package com.rosselo.server.service.rss

import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.repository.rss.SourceRepository
import org.springframework.stereotype.Service
import kotlin.math.max

/**
 * Domain service used for common operations on [Service].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class SourceService(private val sourceRepository: SourceRepository) {
    fun searchByUrl(
            url: String,
            page: Int,
            tlds: List<String>?,
            languages: List<String>?
    ): List<Source> {
        return sourceRepository.findByUrlSearch(
                url,
                -1,
                4,
                false,
                10,
                (max(page, 0) - 1) * 10,
                tlds,
                languages
        )
        // todo config | constants
    }

    fun search(
            query: String,
            page: Int,
            tlds: List<String>?,
            languages: List<String>?,
            channelTypes: List<SourceType>?
    ): List<Source> {
        return sourceRepository.findByFulltextSearchExact(
                query,
                -1,
                4,
                false,
                10,
                (max(page, 0) - 1) * 10,
                tlds,
                languages,
                channelTypes
        )
        // todo config | constants
        // todo top up with non exact
    }
}

