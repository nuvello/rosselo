package com.rosselo.server.service.rss.scrapper

import com.google.common.net.InternetDomainName
import com.rosselo.server.config.QueueConfig
import com.rosselo.server.model.entity.ConfigType
import com.rosselo.server.model.entity.rss.Entry
import com.rosselo.server.model.entity.rss.Source
import com.rosselo.server.model.entity.rss.SourceDownloadError
import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.repository.rss.EntryRepository
import com.rosselo.server.repository.rss.FeedRepository
import com.rosselo.server.repository.rss.SourceRepository
import com.rosselo.server.service.ConfigService
import com.rosselo.server.service.SourceIconStorage
import com.rosselo.server.service.YoutubeService
import com.rosselo.server.service.rss.scrapper.downloaders.*
import com.rosselo.server.service.utility.LanguageDetector
import com.rosselo.server.utils.network.RosseloHttpClient
import org.apache.commons.codec.digest.DigestUtils
import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.stereotype.Service
import org.springframework.transaction.support.TransactionTemplate
import java.io.PrintWriter
import java.io.StringWriter
import java.net.URI
import java.time.LocalDateTime
import java.util.*
import kotlin.math.max

@Service
class ScrapperService(
        private val feedRepository: FeedRepository,
        private val sourceRepository: SourceRepository,
        private val entryRepository: EntryRepository,
        private val youtubeService: YoutubeService,
        private val configService: ConfigService,
        private val transactionTemplate: TransactionTemplate,
        private val rabbitTemplate: RabbitTemplate,
        private val sourceIconStorage: SourceIconStorage,
        private val extraScrapExecutor: ExtraScrapExecutor,
        rosseloHttpClient: RosseloHttpClient
) {
    private companion object {
        private const val ENTRY_IDENTIFIER_MAX_SIZE = 500
        private const val ENTRY_LINK_MAX_SIZE = 500
        private const val ENTRY_TITLE_MAX_SIZE = 500
        private const val SOURCE_TITLE_MAX_SIZE = 500
        private const val SOURCE_LINK_MAX_SIZE = 500
        private const val SOURCE_DESCRIPTION_MAX_SIZE = 500
        private val logger = LoggerFactory.getLogger(this::class.java)
    }

    private val rssDownloader: RssDownloader = RssDownloader(rosseloHttpClient, sourceRepository)
    private val youtubeDownloader: YoutubeDownloader = YoutubeDownloader(rssDownloader)
    private val twitterDownloader: TwitterDownloader = TwitterDownloader(rssDownloader)

    fun scrapByWorker(scrapTask: ScrapTask): ScrapResult {
        logger.info("Scrap by worker")
        return rabbitTemplate.convertSendAndReceive(
                QueueConfig.apiScrapTaskQueueName,
                scrapTask
        ) as ScrapResult
    }

    fun scrap(task: ScrapTask): ScrapResult {
        val sourceData: String
        val sourceType: SourceType

        var source = if (task.sourceType == ScrapTaskSourceType.SOURCE_ID) {
            val source = sourceRepository.findById(UUID.fromString(task.source))
                    .orElseThrow { IllegalArgumentException("Source not found.") }

            sourceType = source.type
            sourceData = source.source

            source
        } else {
            when (task.sourceType) {
                ScrapTaskSourceType.URL -> {
                    sourceType = SourceType.RSS_URL
                    sourceData = task.source
                }
                ScrapTaskSourceType.YOUTUBE_CHANNEL_ID -> {
                    sourceType = SourceType.YOUTUBE_CHANNEL_ID
                    sourceData = task.source
                }
                ScrapTaskSourceType.YOUTUBE_PLAYLIST_ID -> {
                    sourceType = SourceType.YOUTUBE_PLAYLIST_ID
                    sourceData = task.source
                }
                ScrapTaskSourceType.YOUTUBE_CHANNEL_NAME -> {
                    sourceType = SourceType.YOUTUBE_CHANNEL_ID
                    sourceData = youtubeService.getChannelId(task.source)
                }
                ScrapTaskSourceType.TWITTER_ACCOUNT_NAME -> {
                    sourceType = SourceType.TWITTER_ACCOUNT_NAME
                    sourceData = task.source
                }
                ScrapTaskSourceType.SOURCE_ID -> {
                    // impossible exception
                    throw IllegalStateException()
                }
            }

            sourceRepository.findByTypeAndSource(sourceType, sourceData)
        }

        if (shouldScrap(source, task)) {
            val dateOfFetch = LocalDateTime.now()
            val data: ParsedSource
            try {
                data = download(sourceType, sourceData)
            } catch (ex: DownloadException) {
                val sw = StringWriter()
                ex.printStackTrace(PrintWriter(sw))
                var exceptionAsString = sw.toString()
                if (exceptionAsString.length > 2000) {
                    exceptionAsString = exceptionAsString.substring(0, 2000)
                }

                if (source != null) {
                    val error = SourceDownloadError(
                            UUID.randomUUID(),
                            source,
                            LocalDateTime.now(),
                            exceptionAsString,
                            ex.reason
                    )

                    transactionTemplate.execute {
                        source = sourceRepository.merge(source!!)
                        if (source!!.downloadErrors.size > 5) {
                            source!!.downloadErrors.remove(source!!.downloadErrors.minBy { it.createdAt })
                        }
                        source!!.errorsSinceLastSuccessful++
                        source!!.downloadErrors.add(error)
                        source!!.lastFetchAt = dateOfFetch
                        sourceRepository.save(source!!)
                    }
                }

                return ScrapResult(ScrapResultStatus.ERROR, null)
            }

            return if (source == null) {
                newSource(task, data)
            } else {
                updateSource(task, source!!, data)
            }
        } else {
            return buildResult(task, source!!)
        }
    }

    private fun newSource(task: ScrapTask, data: ParsedSource): ScrapResult {
        // a permanent redirect happened
        if (data.source != task.source) {
            // check if there is a source with the given data
            val newSource = sourceRepository.findByTypeAndSource(
                    data.type,
                    data.source
            )
            if (newSource != null) {
                // serve this source instead
                return if (shouldScrap(newSource, task)) {
                    updateSource(task, newSource, data)
                } else {
                    buildResult(task, newSource)
                }
            }
        }

        // create new source
        val newSource = Source(
                UUID.randomUUID(),
                data.type,
                data.source,
                truncate(data.title, SOURCE_TITLE_MAX_SIZE),
                if (data.description != null) truncate(data.description, SOURCE_DESCRIPTION_MAX_SIZE) else null,
                truncate(data.link, SOURCE_LINK_MAX_SIZE).toLowerCase(),
                LocalDateTime.now(),
                null,
                null,
                null,
                if (data.type == SourceType.RSS_URL) InternetDomainName.from(URI(data.link).host).publicSuffix().toString() else null,
                null,
                false,
                emptyArray(),
                emptyArray(),
                emptyArray(),
                null,
                null,
                null,
                0,
                0,
                task.storeItems,
                data.dateOfFetch,
                data.dateOfFetch,
                null,
                0
        )

        if (shouldDoExtraScrap(newSource, task)) {
            doExtraScrap(newSource, data)
        }

        val rawItems = data.items.take(configService.getInt(ConfigType.MAX_NUMBER_OF_ENTRIES_PER_FEED))

        // todo this can throw if somebody created the source!
        transactionTemplate.execute {
            sourceRepository.save(newSource)

            if (task.storeItems) {
                rawItems.sortedBy { it.date }
                        .forEach {
                            val item = createNewEntry(it, newSource)
                            entryRepository.save(item)
                        }
            }
        }

        val resultItems = if (task.returnItems) {
            rawItems.map {
                ScrapResultItem(
                        truncate(it.title, ENTRY_TITLE_MAX_SIZE),
                        truncate(it.link, ENTRY_LINK_MAX_SIZE)
                )
            }.toList()
        } else {
            null
        }


        return ScrapResult(ScrapResultStatus.SUCCESS, ScrapResultData(newSource.id, newSource.title, newSource.source, newSource.type, resultItems))
    }

    private fun updateSource(task: ScrapTask, sourceToUpdate: Source, data: ParsedSource): ScrapResult {

        var source = sourceToUpdate
        // permanent redirect happened, the resource was moved
        if (data.source != source.source) {
            // check if such a resource exists already
            val newSource = sourceRepository.findByTypeAndSource(
                    source.type,
                    data.source
            )
            if (newSource != null) {
                transactionTemplate.execute {
                    // lock both sources
                    sourceRepository.findByIdWithSharedLock(newSource.id)
                    sourceRepository.findByIdWithExclusiveLock(source.id)

                    // replace the current source with the new one
                    feedRepository.replaceSource(source.id, newSource.id)
                    // delete this one
                    sourceRepository.deleteById(source.id)
                }

                if (source.bigIconHash != null) {
                    sourceIconStorage.removeBigIcon(source)
                }
                if (source.smallIconHash != null) {
                    sourceIconStorage.removeSmallIcon(source)
                }
                // and serve the new one
                return if (shouldScrap(newSource, task)) {
                    updateSource(task, newSource, data)
                } else {
                    buildResult(task, newSource)
                }
            }
        }

        if (shouldDoExtraScrap(source, task)) {
            doExtraScrap(source, data)
        }

        val tld = if (source.type == SourceType.RSS_URL) {
            InternetDomainName.from(URI(data.link).host).publicSuffix().toString()
        } else {
            null
        }
        // todo redo error handling

        transactionTemplate.execute {
            // todo this can throw if somebody updated / removed the source
            // two transactions?
            source = sourceRepository.merge(source)
            source.source = data.source
            source.title = truncate(data.title, SOURCE_TITLE_MAX_SIZE)
            if (source.description == null || (data.description != null && data.description.length > source.description!!.length)) {
                source.description = if (data.description != null) truncate(data.description, SOURCE_DESCRIPTION_MAX_SIZE) else null
            }
            source.link = truncate(data.link, SOURCE_LINK_MAX_SIZE).toLowerCase()
            source.tld = tld
            source.lastFetchAt = data.dateOfFetch
            source.lastSuccessfulFetchAt = data.dateOfFetch
            source.downloadErrors.clear()
            source.errorsSinceLastSuccessful = 0
            source.entriesSaved = source.entriesSaved || task.storeItems

            sourceRepository.save(source)

            if (task.storeItems) {
                val maxPerSource = configService.getInt(ConfigType.MAX_NUMBER_OF_ENTRIES_PER_FEED)
                // do all contain date?
                var containDate = true
                // matched entries
                val found = HashSet<Entry>()
                // mindate / max date from the retrieved entries
                var minDate: LocalDateTime? = null
                var maxDate: LocalDateTime? = null
                // is there a new one?
                var added = false
                val items = entryRepository.findUserIndependentView(source)
                // maps so that we can match the retrieved entries with already stored ones
                // even if at most two of title / identifier / links changed but at least one property persisted
                val titles = HashMap<String, Entry>()
                val identifiers = HashMap<String, Entry>()
                val links = HashMap<String, Entry>()
                items.forEach { titles[it.title] = it; identifiers[it.identifier] = it; links[it.link] = it }

                val sortedItems = data.items.sortedBy { it.date }

                // only the newest x and iterate from latest to newest
                for (i in max(0, sortedItems.size - maxPerSource) until data.items.size) {
                    val item = data.items[i]

                    // todo this can throw if somebody updated / removed the source
                    if (item.date == null) {
                        containDate = false
                    } else if (containDate) {
                        if (minDate == null || minDate.isAfter(item.date)) {
                            minDate = item.date
                        }
                        if (maxDate == null || maxDate.isBefore(item.date)) {
                            maxDate = item.date
                        }
                    }

                    val entry = titles[item.title] ?: identifiers[item.uniqueId]
                    ?: links[item.link]

                    if (entry == null) {
                        val newEntry = createNewEntry(item, source)
                        entryRepository.save(newEntry)
                        added = true
                    } else {
                        entry.identifier = truncate(item.uniqueId, ENTRY_IDENTIFIER_MAX_SIZE)
                        entry.link = truncate(item.link, ENTRY_LINK_MAX_SIZE)
                        entry.title = truncate(item.title, ENTRY_TITLE_MAX_SIZE)

                        entryRepository.save(entry)
                        found.add(entry)
                    }
                }

                // remove ones that disappeared (we can identify them by dates)
                if (containDate && minDate != null && maxDate != null) {
                    items.filter {
                        it.publishedAt != null
                                && it.publishedAt.isAfter(minDate) && it.publishedAt.isBefore(maxDate)
                                && !found.contains(it)
                    }.forEach {
                        entryRepository.delete(it)
                    }
                }

                if (added) {
                    entryRepository.keepOnlyNewestPerSource(source, maxPerSource)
                }
            }
        }

        return buildResult(task, source)
    }

    private fun doExtraScrap(source: Source, data: ParsedSource) {
        source.lastExtraScrapAt = LocalDateTime.now()
        val (smallIcon, bigIcon, language, description, errors) = extraScrapExecutor.execute(data)

        if (source.language == null || language != LanguageDetector.UNKNOWN_LANGUAGE) {
            source.language = language
        }

        if (smallIcon != null) {
            val hash = DigestUtils.sha256Hex(smallIcon.data)

            if (source.smallIconHash != hash) {
                sourceIconStorage.saveSmallIcon(source, smallIcon.data, smallIcon.mimeType)
                source.smallIconHash = hash;
            }
        }

        if (bigIcon != null) {
            val hash = DigestUtils.sha256Hex(bigIcon.data)

            if (source.bigIconHash != hash) {
                sourceIconStorage.saveBigIcon(source, bigIcon.data, bigIcon.mimeType)
                source.bigIconHash = hash;
            }
        }

        if (description != null &&
                (source.description == null || source.description!!.length < description.length)) {
            source.description = truncate(description, SOURCE_DESCRIPTION_MAX_SIZE)
        }

        // todo store errors
        if (errors.isNotEmpty()) {
            logger.error("Errors in extra scrap for " + source.source + " " + errors.toString())
        }
    }

    private fun truncate(data: String, maxLength: Int): String {
        if (data.length < maxLength) {
            return data
        }

        return data.substring(0, maxLength - 3) + "..."
    }

    private fun createNewEntry(item: ParsedSourceItem, source: Source): Entry {
        val title = Jsoup.parse(item.title).text()

        return Entry(
                UUID.randomUUID(),
                LocalDateTime.now(),
                item.date,
                truncate(item.uniqueId, ENTRY_IDENTIFIER_MAX_SIZE),
                truncate(title, ENTRY_TITLE_MAX_SIZE),
                truncate(item.link, ENTRY_LINK_MAX_SIZE),
                source
        )
    }

    private fun shouldDoExtraScrap(source: Source?, task: ScrapTask): Boolean {
        return if (task.quick) {
            false
        } else {
            val lastExtraScrapAt = source?.lastExtraScrapAt
            lastExtraScrapAt == null || lastExtraScrapAt.isBefore(LocalDateTime.now().minusDays(30))
        }
    }

    private fun shouldScrap(source: Source?, task: ScrapTask): Boolean {
        return if (source == null) {
            true
        } else {
            !task.quick
                    || ((task.storeItems || task.returnItems) && !source.entriesSaved)
                    || (task.returnItems && source.lastFetchAt.isBefore(LocalDateTime.now().minusDays(5)))
        }
    }

    private fun buildResult(task: ScrapTask, source: Source): ScrapResult {
        var items: List<ScrapResultItem>? = null

        if (task.returnItems) {
            items = entryRepository.findUserIndependentView(source)
                    .map {
                        ScrapResultItem(
                                it.title,
                                it.link
                        )
                    }.toList()
        }

        return ScrapResult(ScrapResultStatus.SUCCESS, ScrapResultData(source.id, source.title, source.source, source.type, items))
    }

    private fun download(sourceType: SourceType, source: String): ParsedSource {
        return when (sourceType) {
            SourceType.TWITTER_ACCOUNT_NAME -> twitterDownloader.download(source)
            SourceType.YOUTUBE_PLAYLIST_ID -> youtubeDownloader.downloadPlaylist(source)
            SourceType.YOUTUBE_CHANNEL_ID -> youtubeDownloader.downloadChannel(source)
            SourceType.RSS_URL -> rssDownloader.download(source)
        }
    }
}
