package com.rosselo.server.service.rss.scrapper.downloaders

import com.rosselo.server.model.entity.rss.SourceDownloadErrorType

class DownloadException : Exception {
    val reason: SourceDownloadErrorType

    constructor(message: String, reason: SourceDownloadErrorType, cause: Throwable) : super(message, cause) {
        this.reason = reason
    }
    constructor(message: String, reason: SourceDownloadErrorType) : super(message) {
        this.reason = reason
    }
    constructor(reason: SourceDownloadErrorType, cause: Throwable) : super(cause) {
        this.reason = reason
    }
    constructor(reason: SourceDownloadErrorType) : super() {
        this.reason = reason
    }

}
