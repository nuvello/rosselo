package com.rosselo.server.service

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.ObjectMetadata
import com.rosselo.server.config.RosseloConfig
import com.rosselo.server.model.entity.rss.Source
import org.apache.commons.codec.binary.Base64
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream

@Service
class SourceIconStorage(
        private val s3Client: AmazonS3,
        private val rosseloConfig: RosseloConfig
) {
    private companion object {
        private const val ICON_BIG_SUFFIX = "_icon_big"
        private const val ICON_SMALL_SUFFIX = "_icon_small"
    }

    fun getUrlBigIcon(source: Source): String {
        return s3Client.getUrl(rosseloConfig.s3.buckets.sourceIcons, "${source.id}$ICON_BIG_SUFFIX").toExternalForm()
    }
    fun getUrlSmallIcon(source: Source): String {
        return s3Client.getUrl(rosseloConfig.s3.buckets.sourceIcons, "${source.id}$ICON_SMALL_SUFFIX").toExternalForm()
    }

    fun removeSmallIcon(source: Source) {
        s3Client.deleteObject(rosseloConfig.s3.buckets.sourceIcons, "${source.id}$ICON_SMALL_SUFFIX")
    }
    fun removeBigIcon(source: Source) {
        s3Client.deleteObject(rosseloConfig.s3.buckets.sourceIcons, "${source.id}$ICON_BIG_SUFFIX")
    }

    fun saveBigIcon(source: Source, data: ByteArray, contentType: String) {
        save("${source.id}$ICON_BIG_SUFFIX", data, contentType)
    }
    fun saveSmallIcon(source: Source, data: ByteArray, contentType: String) {
        save("${source.id}$ICON_SMALL_SUFFIX", data, contentType)
    }

    private fun save(name: String, data: ByteArray, contentType: String) {
        val objectMetadata = ObjectMetadata()
        objectMetadata.contentType = contentType
        objectMetadata.contentLength = data.size.toLong()
        objectMetadata.contentMD5 = String(Base64.encodeBase64(DigestUtils.md5(data)))
        objectMetadata.setHeader("x-amz-acl", "public-read")

        s3Client.putObject(
                rosseloConfig.s3.buckets.sourceIcons,
                name,
                ByteArrayInputStream(data),
                objectMetadata
        )
    }
}