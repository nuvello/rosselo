package com.rosselo.server.service;

import com.rosselo.server.model.entity.Account;
import com.rosselo.server.model.entity.LimitedAction;
import com.rosselo.server.model.entity.LimitedActionLog;
import com.rosselo.server.model.entity.LimitedActionType;
import com.rosselo.server.repository.LimitedActionLogRepository;
import com.rosselo.server.repository.LimitedActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class LimitedActionService {

    private LimitedActionRepository limitedActionRepository;
    private LimitedActionLogRepository limitedActionLogRepository;

    @Autowired
    public LimitedActionService(
            LimitedActionRepository limitedActionRepository,
            LimitedActionLogRepository limitedActionLogRepository
    ) {
        this.limitedActionRepository = limitedActionRepository;
        this.limitedActionLogRepository = limitedActionLogRepository;
    }

    public void log(LimitedActionType type, String ipAddress, Account account) {
        final LimitedAction action = findAction(type);

        limitedActionLogRepository.save(
                new LimitedActionLog(
                        null,
                        action,
                        account,
                        LocalDateTime.now(),
                        ipAddress
                )
        );
    }

    public boolean canDo(LimitedActionType type, String ipAddress, Account account) {
        final LimitedAction action = findAction(type);

        // fixme: simplify

        if (action.getMaxPerHourPerIp() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    ipAddress,
                    LocalDateTime.now().minusHours(1)
            ) >= action.getMaxPerHourPerIp()) {
                return false;
            }
        }

        if (action.getMaxPerDayPerIp() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    ipAddress,
                    LocalDateTime.now().minusDays(1)
            ) >= action.getMaxPerDayPerIp()) {
                return false;
            }
        }

        if (action.getMaxPerWeekPerIp() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    ipAddress,
                    LocalDateTime.now().minusWeeks(1)
            ) >= action.getMaxPerWeekPerIp()) {
                return false;
            }
        }

        if (account == null) {
            return true;
        }

        if (action.getMaxPerHourPerUser() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    account,
                    LocalDateTime.now().minusHours(1)
            ) >= action.getMaxPerHourPerUser()) {
                return false;
            }
        }

        if (action.getMaxPerDayPerUser() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    account,
                    LocalDateTime.now().minusHours(1)
            ) >= action.getMaxPerDayPerUser()) {
                return false;
            }
        }

        if (action.getMaxPerWeekPerUser() != null) {
            if (limitedActionLogRepository.count(
                    action,
                    account,
                    LocalDateTime.now().minusHours(1)
            ) >= action.getMaxPerWeekPerUser()) {
                return false;
            }
        }

        return true;
    }

    private LimitedAction findAction(LimitedActionType type) {
        final Optional<LimitedAction> action = limitedActionRepository.findByType(type);

        if (!action.isPresent()) {
            throw new IllegalStateException("Limited action with type of " + type + " not found.");
        }

        return action.get();
    }
}
