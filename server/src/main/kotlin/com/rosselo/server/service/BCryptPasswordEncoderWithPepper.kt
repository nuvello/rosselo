package com.rosselo.server.service

import com.rosselo.server.config.RosseloConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

/**
 * Implementation of [PasswordEncoder] extending [BCryptPasswordEncoder] with the difference of adding
 * pepper to the hashed password. The pepper is stored in the environment variable rosselo.pepper.
 *
 * @author Juraj Mlich <jurajmlich></jurajmlich>@gmail.com>
 */
@Service
class BCryptPasswordEncoderWithPepper @Autowired constructor(
        private val rosseloConfig: RosseloConfig
) : BCryptPasswordEncoder(), PasswordEncoder {

    /**
     * {@inheritDoc}
     */
    override fun encode(rawPassword: CharSequence): String {
        return super.encode(rawPassword.toString() + rosseloConfig.pepper)
    }

    /**
     * {@inheritDoc}
     */
    override fun matches(rawPassword: CharSequence, encodedPassword: String?): Boolean {
        return super.matches(rawPassword.toString() + rosseloConfig.pepper, encodedPassword)
    }
}
