package com.rosselo.server.service.utility

import com.google.common.base.Optional
import com.optimaize.langdetect.LanguageDetectorBuilder
import com.optimaize.langdetect.i18n.LdLocale
import com.optimaize.langdetect.ngram.NgramExtractors
import com.optimaize.langdetect.profiles.LanguageProfileReader
import com.optimaize.langdetect.text.CommonTextObjectFactories
import org.springframework.stereotype.Component

@Component
class LanguageDetector {
    companion object {
        const val UNKNOWN_LANGUAGE = "unknown"
        private val langShortToLongMap = mapOf(
                Pair("en", "english"),
                Pair("cs", "czech"),
                Pair("fr", "french"),
                Pair("de", "german"),
                Pair("pl", "polish"),
                Pair("pt", "portuguese"),
                Pair("ru", "russian"),
                Pair("sk", "slovak"),
                Pair("es", "spanish")
        )
        private val languageProfiles = LanguageProfileReader().readBuiltIn(langShortToLongMap.keys.map {
            LdLocale.fromString(it)
        })
        private val languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build()
        private val textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText()
    }

    fun detect(text: String): String {
        val lang: Optional<LdLocale> = languageDetector.detect(textObjectFactory.forText(text))

        return if (lang.isPresent) {
            langShortToLongMap[lang.get().language] ?: UNKNOWN_LANGUAGE
        } else {
            UNKNOWN_LANGUAGE
        }
    }
}