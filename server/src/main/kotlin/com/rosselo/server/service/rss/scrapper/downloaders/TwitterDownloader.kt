package com.rosselo.server.service.rss.scrapper.downloaders

import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.utils.network.EncodingUtil

class TwitterDownloader constructor(
        private val rssDownloader: RssDownloader
) {
    fun download(nick: String): ParsedSource {
        // todo make this configurable
        return rssDownloader.download(
                "https://nitter.lacontrevoie.fr/${EncodingUtil.encodeURIComponent(nick)}/rss"
        ).copy(source = nick, type = SourceType.TWITTER_ACCOUNT_NAME, title = "@$nick")
    }
}