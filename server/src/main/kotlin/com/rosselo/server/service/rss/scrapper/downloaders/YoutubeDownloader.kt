package com.rosselo.server.service.rss.scrapper.downloaders

import com.rosselo.server.model.entity.rss.SourceType
import com.rosselo.server.utils.network.EncodingUtil

class YoutubeDownloader constructor(
        private val rssDownloader: RssDownloader
) {
    fun downloadChannel(channelId: String): ParsedSource {
        return rssDownloader.download(
                "https://www.youtube.com/feeds/videos.xml?channel_id=${EncodingUtil.encodeURIComponent(channelId)}"
        ).copy(source = channelId, type = SourceType.YOUTUBE_CHANNEL_ID)
    }

    fun downloadPlaylist(playlistId: String): ParsedSource {
        return rssDownloader.download(
                "https://www.youtube.com/feeds/videos.xml?playlist_id=${EncodingUtil.encodeURIComponent(playlistId)}"
        ).copy(source = playlistId, type = SourceType.YOUTUBE_PLAYLIST_ID)
    }
}