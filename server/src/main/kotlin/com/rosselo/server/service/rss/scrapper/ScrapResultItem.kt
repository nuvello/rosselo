package com.rosselo.server.service.rss.scrapper

import java.io.Serializable

data class ScrapResultItem(
        val title: String,
        val link: String
):Serializable