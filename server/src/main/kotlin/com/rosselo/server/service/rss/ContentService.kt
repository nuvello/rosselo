package com.rosselo.server.service.rss

import com.rosselo.server.model.entity.*
import com.rosselo.server.model.entity.rss.Category
import com.rosselo.server.repository.AccountPreferenceRepository
import com.rosselo.server.repository.rss.CategoryRepository
import com.rosselo.server.service.ConfigService
import com.rosselo.server.service.rss.scrapper.ScrapResultStatus
import com.rosselo.server.service.rss.scrapper.ScrapTask
import com.rosselo.server.service.rss.scrapper.ScrapTaskSourceType
import com.rosselo.server.service.rss.scrapper.ScrapperService
import com.rosselo.server.utils.opml.OpmlCategory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

/**
 * Domain service used for common operations on [Category].
 *
 * @author Juraj Mlich <juraj@mlich.eu>
 */
@Service
class ContentService @Autowired constructor(
        private val feedService: FeedService,
        private val categoryRepository: CategoryRepository,
        private val categoryService: CategoryService,
        private val configService: ConfigService,
        private val accountPreferenceRepository: AccountPreferenceRepository,
        private val scrapperService: ScrapperService
) {
    @Transactional
    fun importOpmlFeeds(
            account: Account,
            intoCategory: Category?,
            opmlCategories: List<OpmlCategory>
    ) {
        val categories = HashMap<String, Category>()
        val hideEntriesAfterReadAccountPreference =
                HideEntriesAfterReadPreference.valueOf((accountPreferenceRepository.findById(
                        AccountPreferenceId(AccountPreferenceType.HIDE_ENTRIES_AFTER_READ, account)
                ).orElseThrow { IllegalStateException() }).value) // todo message
        val defaultMaxAmountOfItems =
                configService.getNullableInt(ConfigType.DEFAULT_FEED_MAX_AMOUNT_OF_ITEMS_PREFERENCE)
        val defaultNoOlderThan =
                configService.getNullableInt(ConfigType.DEFAULT_FEED_NO_OLDER_THAN_PREFERENCE)

        val defaultHideEntriesAfterRead =
                when (hideEntriesAfterReadAccountPreference) {
                    HideEntriesAfterReadPreference.RESPECT_FEED_SETTINGS ->
                        configService.get(ConfigType.DEFAULT_FEED_HIDE_ENTRIES_AFTER_READ_PREFERENCE) == "true"
                    HideEntriesAfterReadPreference.FALSE ->
                        false
                    HideEntriesAfterReadPreference.TRUE ->
                        true
                }

        opmlCategories.forEach { opmlCategory ->
            opmlCategory.feeds.forEach { opmlFeed ->
                var toCategory: Category? = intoCategory

                if (toCategory == null) {
                    toCategory = categories[opmlCategory.name]
                }

                if (toCategory == null) {
                    val withTheSameName =
                            categoryRepository.findAllByNameAndAccount(opmlCategory.name, account)

                    if (withTheSameName.isNotEmpty()) {
                        toCategory = withTheSameName.first()
                    }
                }

                if (toCategory == null) {
                    toCategory =
                            categoryService.create(account, opmlCategory.name)
                    categories[toCategory.name] = toCategory
                }

                val result = scrapperService.scrapByWorker(ScrapTask(
                        ScrapTaskSourceType.URL,
                        opmlFeed.url,
                        storeItems = true,
                        returnItems = false,
                        quick = true
                ))

                if (result.status == ScrapResultStatus.SUCCESS && result.data != null) {
                    feedService.create(
                            account,
                            toCategory,
                            opmlFeed.name,
                            result.data.sourceId,
                            defaultHideEntriesAfterRead,
                            defaultMaxAmountOfItems,
                            defaultNoOlderThan
                    )
                } else {
                    // todo fixme
                }
            }
        }
    }
}