package com.rosselo.server.service.utility

import com.rosselo.server.utils.network.RosseloHttpClient
import org.apache.http.entity.ContentType
import org.imgscalr.Scalr
import org.springframework.stereotype.Service
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.URI
import javax.imageio.ImageIO

data class Image(
        val data: ByteArray,
        val mimeType: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Image

        if (!data.contentEquals(other.data)) return false
        if (mimeType != other.mimeType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = data.contentHashCode()
        result = 31 * result + mimeType.hashCode()
        return result
    }
}

@Service
class ImageService(
        private val rosseloHttpClient: RosseloHttpClient
) {
    fun download(uri: URI): Image {
        val image = rosseloHttpClient.downloadUri(uri, RosseloHttpClient.ResultContentType.IMAGE)

        if (image.contentType != RosseloHttpClient.ResultContentType.IMAGE) {
            throw IllegalArgumentException("Provided URI doesn't contain an image.")
        }

        return Image(image.content, image.mimeType)
    }

    fun ensureMaxDimensions(original: Image, maxSize: Int): Image {
        var newImage = ImageIO.read(ByteArrayInputStream(original.data))

        if (newImage.width < maxSize && newImage.height < maxSize) {
            return original
        }

        newImage = Scalr.resize(newImage, Scalr.Method.SPEED, maxSize);

        val resultOutputStream = ByteArrayOutputStream()
        ImageIO.write(newImage, "png", resultOutputStream)

        return Image(
                resultOutputStream.toByteArray(),
                ContentType.IMAGE_PNG.mimeType
        )
    }
}