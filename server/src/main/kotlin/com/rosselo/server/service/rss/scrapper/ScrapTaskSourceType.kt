package com.rosselo.server.service.rss.scrapper

enum class ScrapTaskSourceType {
    SOURCE_ID,
    URL,
    YOUTUBE_CHANNEL_ID,
    YOUTUBE_CHANNEL_NAME,
    YOUTUBE_PLAYLIST_ID,
    TWITTER_ACCOUNT_NAME
}