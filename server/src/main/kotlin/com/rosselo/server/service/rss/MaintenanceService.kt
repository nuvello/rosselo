package com.rosselo.server.service.rss

import com.rosselo.server.repository.LimitedActionLogRepository
import com.rosselo.server.repository.PasswordResetRepository
import com.rosselo.server.repository.rss.EntryRepository
import com.rosselo.server.repository.rss.SourceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import javax.transaction.Transactional


/**
 * @author Juraj Mlich <jurajmlich></jurajmlich>@gmail.com>
 */
@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class MaintenanceService @Autowired constructor(
        private val passwordResetRepository: PasswordResetRepository,
        private val limitedActionLogRepository: LimitedActionLogRepository,
        private val sourceRepository: SourceRepository
) {

    @Scheduled(fixedDelay = 1000 * 60 * 60, initialDelay = 1000 * 60)
    @Transactional
    fun maintain() {
        // todo do this only on one instance
        passwordResetRepository.deleteAllByCreatedAtBefore(
                LocalDateTime.now().minusDays(7)
        )
        limitedActionLogRepository.deleteAllByCreationDateTimeBefore(
                LocalDateTime.now().minusDays(7)
        )
    }
}
