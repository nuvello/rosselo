package com.rosselo.server.service

import com.maxmind.geoip2.DatabaseReader
import com.maxmind.geoip2.model.CountryResponse
import com.rosselo.server.config.RosseloConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Service
import java.net.InetAddress


@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
class GeoIPService @Autowired constructor(
        private val rosseloConfig: RosseloConfig
) {
    private companion object {
        private val reader: DatabaseReader

        init {
            val res =
                    GeoIPService::class.java.getResourceAsStream("/geoip.mmdb")

            reader = DatabaseReader.Builder(res).build()
        }
    }

    fun locateCountry(ip: String): CountryResponse? {
        val ipAddress = if (rosseloConfig.production) {
            InetAddress.getByName(ip)
        } else {
            InetAddress.getByName("89.238.186.228")
        }

        return try {
            reader.country(ipAddress)
        } catch (e: Exception) {
            e.printStackTrace() // todo better
            null
        }
    }
}
