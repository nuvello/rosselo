package com.rosselo.server.service.rss.scrapper

import java.io.Serializable


data class ScrapTask(
        val sourceType: ScrapTaskSourceType,
        val source: String,
        val storeItems: Boolean,
        val returnItems: Boolean,
        val quick: Boolean
) : Serializable
