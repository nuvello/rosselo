package com.rosselo.server.utils

import com.rosselo.server.utils.network.URIUtil
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import java.net.URI
import kotlin.test.assertEquals

internal class URIUtilTest {
    @ParameterizedTest
    @CsvSource(
            "http://test.com/skuska/ferko, test.com/skuska/ferko",
            "https://test.com/skuska/ferko, test.com/skuska/ferko",
            "www.test.com/skuska/ferko, test.com/skuska/ferko",
            "test.com/skuska/ferko, test.com/skuska/ferko",
            "http://www.test.com/skuska/ferko, test.com/skuska/ferko",
            "https://www.test.com/skuska/ferko, test.com/skuska/ferko"
    )
    fun removeSchemeAndWww(uri: String, expected: String) {
        assertEquals(expected, URIUtil.removeSchemeAndWww(uri))
    }

    @ParameterizedTest
    @CsvSource(
            "//test.com/skuska/ferko, http://google.com/test, http://test.com/skuska/ferko",
            "//test.com/skuska/ferko, https://google.com/test/test, https://test.com/skuska/ferko",
            "//test.com/skuska/ferko, https://google.com/, https://test.com/skuska/ferko",
            "http://test.com/skuska, https://google.com, http://test.com/skuska",
            "/skuska/fa, http://google.com, http://google.com/skuska/fa",
            "/skuska/fa, http://google.com/test/test, http://google.com/skuska/fa",
            "/skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",

            "skuska/fa, https://google.com/test/test/, https://google.com/test/test/skuska/fa",
            "skuska/fa, https://google.com/test/test, https://google.com/test/test/skuska/fa",
            "./skuska/fa, https://google.com/test/test, https://google.com/test/test/skuska/fa",
            "../skuska/fa, https://google.com/test/test, https://google.com/test/skuska/fa",
            "../../skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",
            "../../../skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",
            "../../../skuska/../fa, https://google.com/test/test, https://google.com/fa"
    )
    fun makeRelativeToBase(uri: String, base: String, expected: String) {
        assertEquals(URIUtil.makeRelativeToBase(
                URI.create(uri),
                URI.create(base)
        ).toString(), expected)
    }

    @ParameterizedTest
    @CsvSource(
            "skuska/fa, https://google.com/test/test, https://google.com/test/skuska/fa",
            "//test.com/skuska/ferko, http://google.com/test, http://test.com/skuska/ferko",
            "//test.com/skuska/ferko, https://google.com/test/test, https://test.com/skuska/ferko",
            "//test.com/skuska/ferko, https://google.com/, https://test.com/skuska/ferko",
            "http://test.com/skuska, https://google.com, http://test.com/skuska",
            "/skuska/fa, http://google.com, http://google.com/skuska/fa",
            "/skuska/fa, http://google.com/test/test, http://google.com/skuska/fa",
            "/skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",

            "skuska/fa, https://google.com/test/test/, https://google.com/test/test/skuska/fa",
            "./skuska/fa, https://google.com/test/test, https://google.com/test/skuska/fa",
            "../skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",
            "../../skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",
            "../../../skuska/fa, https://google.com/test/test, https://google.com/skuska/fa",
            "../../../skuska/../fa, https://google.com/test/test, https://google.com/fa"
    )
    fun makeRelativeTo(uri: String, base: String, expected: String) {
        assertEquals(URIUtil.makeRelativeTo(
                URI.create(uri),
                URI.create(base)
        ).toString(), expected)
    }
}