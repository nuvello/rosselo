const gulp = require('gulp');
const sass = require('gulp-sass');
const nunjucksRender = require('gulp-nunjucks-render');
const inlinesource = require('gulp-inline-source');
const inlineCss = require('gulp-inline-css');
const autoprefixer = require('gulp-autoprefixer');
const htmlmin = require('gulp-htmlmin');

sass.compiler = require('node-sass');

const config = {
    sass: {
        files: ['./src/sass/**/*.scss'],
        output: './dist/css'
    },
    html: {
        files: ['./src/emails/*.html'],
        output: './dist',
        templates: ['./src/templates']
    }
};

gulp.task('sass', function () {
    return gulp.src(config.sass.files)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer({browsers: ['last 2 versions', '> 1%']}))
        .pipe(gulp.dest(config.sass.output));
});

gulp.task('html', function () {
    return gulp.src(config.html.files)
        .pipe(nunjucksRender({
            path: config.html.templates,
            envOptions: {
                tags: {
                    variableStart: '<$',
                    variableEnd: '$>',
                }
            }
        })).on('error', function (err) {
            console.log(err);
        })
        .pipe(inlinesource())
        .pipe(inlineCss())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(config.html.output));
});

gulp.task('build', gulp.series('sass', 'html'));
gulp.task('watch', gulp.parallel('build', function() {
    gulp.watch(config.sass.files, gulp.series('build'));
    gulp.watch(config.html.files, gulp.series('build'));
    gulp.watch(config.html.templates, gulp.series('build'));
}));
